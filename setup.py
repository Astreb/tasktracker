from setuptools import setup, find_packages

setup(
    name="TaskTrackerConsole",
    version='1.1',
    description='Console interface for TaskTracker',
    author='Nikita Kulichok',
    author_email='nikita.kulicok@gmail.com',
    url='https://bitbucket.org/Astreb/tasktracker',
    packages=find_packages(),
    test_suite='library.tests.init_tests.get_tests',
    install_requires=['json_tricks', 'django-enumfields'],
    entry_points={
        'console_scripts': [
            'tasktracker=console.consoleapp:main'
        ]
    }
)
