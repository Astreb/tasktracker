# TaskTracker Home

Цель данного проекта - разработка приложения для отслеживания и работы над задачами. Вы можете добавлять различные задачи, назначать для них сроки выполнения, изменять и отслеживать статус их выполнения в процессе работы, добавлять задачи в группы, давать другим пользователям достп к вашим задачам и группам и многое-многое другое!

### Console application

Программа предоставляет удобный консольный интерфейс для работы с вышеперечисленными функциями. 

**Как установить программу:**

Для начала копируйте репозиторий:

```bash
user$ git clone https://bitbucket.org/Astreb/tasktracker tasktracker
```
Перейдите в дирикторию проекта и установите:

```bash
user$ cd tasktracker
user$[sudo] python3 setup.py install
```
Поздравляю! Вы успешно установили TaskTracker!

Вы можете использовать его по краткому имени tasktracker.

По умолчанию, при запуске, приложение само создаст для вас файл конфигурации с настройками по умолчанию.

Однако есть возможность настраивать файл конфигурации из консоли!

**Пример настройки конфигурации**

```bash
# Установка пользователя
tasktracker config --user --user_name Astreb --user_email 'astreb@gmail.com'

# Установка конфигурации по умолчанию
tasktracker config --default

# Установка пути к директории с хранилищем
tasktracker config --edit --storage_path '~/MyTaskTracker/storage'
```

**Примеры использования самого приложения**

``` bash
# Создание задачи
tasktracker tasks --create --task_name 'test task' --start_date '11:00 11.09.2018'

# Редактирование задачи
tasktracker task 1 --edit --description 'test description'

# Просмотр информации о задаче
tasktracker task 1 --show

ID: 1
Name: test task
Start date: 2018-09-11 11:00:00
End date: None
Priority: NORMAL
Status: STARTED
In archive: False
Description: test description
Parent task: None
Parent task relation type: None

[NO SUBTASKS]

[NO GROUPS]

[USERS]

ID: 2
Name: Astreb
Email: astreb@gmail.com
Access type: AUTHOR

# Создадим группу
tasktracker groups --create --group_name 'test group'

# Добавим в неё задачу
tasktracker group 1 --add_task --task_id 1

# Смотрим, что получилось
tasktracker group 1 --show

ID: 1
Name: test group
Description: None

[TASKS]

ID: 1
Name: test task
Start date: 2018-09-11 11:00:00
End date: None
Priority: NORMAL
Status: STARTED

[USERS]

ID: 2
Name: Astreb
Email: None
Access type: AUTHOR
```

И это лишь малая часть возможностей! Для большей информации смотрите STATUS.MD или документацию модулей.


### Library 

Билиблиотека расположена в директории library данного проекта и представляет собой API для работы с приложением.  

**Как установить бибилиотеку:**

Скопируйте репоизторий:

``` bash 
user$ git clone git clone https://bitbucket.org/Astreb/tasktracker tasktracker
```
Перейдите в дирикторию library и установите модуль библиотеки:

``` bash
user$ cd library
user$[sudo] python3 setup.py install
```
Вы также можете запустить тесты бибилотеки:

``` bash
user$[sudo] python3 setup.py test
```
**Как использовать библиотеку:**

1. Импортируйте модуль библиотеки
2. Импортируйте из модуля tasktracker библиотеки класс TaskTracker
3. Создайте объект данного класса, передав как параметр путь к директории, в которой вы хотите хранить ваши данные
4. Пользуйтесь! 

**Пример использования библиотеки:**

``` python
>>> import library
>>> from library.tasktracker import TaskTracker
>>> import datetime
>>> db_path = '~/TaskTracker/storage'
>>> tasktracker = TaskTracker(db_path)
>>> user = tasktracker.create_user('Astreb', 'astreb@gmail.com')
>>> task = tasktracker.create_task('Astreb', 'test task', datetime.datetime.now()) 
>>> task.name
'test_task'
>>> task.status
<TaskStatus.CREATED: 0>
>>> tasktracker.start_date
datetime.datetime(2018, 9, 11, 14, 8, 13, 958691)
>>> tasktracker.update_tasks()
>>> task = tasktracker.get_task_by_id('Astreb', 1)
>>> task.status
<TaskStatus.STARTED: 1>
```
