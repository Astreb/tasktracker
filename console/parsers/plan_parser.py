"""
    Parser for work with single plan
"""
import datetime

from console.service.show_formats import plan_show_format


def init_plan_parser(subparsers):
    plan_parser = subparsers.add_parser('plan', help='Parser to work with a one plan')
    plan_parser.add_argument('plan_id', type=int, help='Plan id')
    plan_parser.add_argument('-s', '--show', dest='show_plan',
                             action='store_true', help='Show plan')
    plan_parser.add_argument('-e', '--edit', dest='edit_plan',
                             action='store_true', help='Edit plan')
    plan_parser.add_argument('-crt', '--change_repeat_time', dest='change_repeat_time',
                             action='store_true', help='Change repeat time delta and repeat months count')

    plan_parser.add_argument('-n', '--plan_task_name', dest='plan_task_name', help='Plan task name')
    plan_parser.add_argument('-desc', '--description', dest='plan_desc',
                             type=str, help='Plan task description')
    plan_parser.add_argument('-p', '--priority', dest='priority', type=int,
                             choices=[0, 1, 2, 3], help='Plan task priority')

    plan_parser.add_argument('-ct', '-task_creation_time', dest='task_creation_time',
                             type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                             help='Task creation time(in HH:MM DD.MM.YYYY format)')
    plan_parser.add_argument('-sd', '--start_date', dest='start_date',
                             type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                             help='Start date(in HH:MM DD.MM.YYYY format)')
    plan_parser.add_argument('-ed', '--end_date', dest='end_date',
                             type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                             help='End date in HH:MM DD.MM.YYYY format')

    plan_parser.add_argument('-M', '--months', dest='delta_months_count', default=0,
                             type=int, help='The plan task will be repeated in M months.')
    plan_parser.add_argument('-w', '--weeks', dest='delta_weeks_count', default=0,
                             type=int, help='The plan task will be repeated in w weeks.')
    plan_parser.add_argument('-d', '--days', dest='delta_days_count', default=0,
                             type=int, help='The plan task will be repeated in d days.')
    plan_parser.add_argument('-H', '--hours', dest='delta_hours_count', default=0,
                             type=int, help='The plan task will be repeated in H hours.')
    plan_parser.add_argument('-m', '--minutes', dest='delta_minutes_count', default=0,
                             type=int, help='The plan task will be repeated in m minutes.')

    plan_parser.set_defaults(parser_level='plan')

    return plan_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'plan':
        return

    if args.show_plan:
        plan = tasktracker.get_plan_by_id(args.plan_id)
        print(plan_show_format(plan))

        return

    if args.edit_plan:
        plan = tasktracker.get_plan_by_id(args.plan_id)

        if args.change_repeat_time:
            if (args.delta_months_count < 0 or args.delta_weeks_count < 0 or args.delta_days_count < 0 or
                    args.delta_hours_count < 0 or args.delta_minutes_count < 0):
                parser.error('Time arguments can not be negative')

            delta_time = datetime.timedelta(weeks=args.delta_weeks_count,
                                            days=args.delta_days_count,
                                            hours=args.delta_hours_count,
                                            minutes=args.delta_minutes_count)
            delta_months_count = args.delta_months_count
        else:
            delta_time = plan.repeat_time_delta
            delta_months_count = plan.repeat_time_delta_months

        tasktracker.edit_plan(user_name,
                              args.plan_id,
                              new_task_name=args.plan_task_name,
                              new_task_creation_time=args.task_creation_time,
                              new_start_date=args.start_date,
                              new_repeat_time_delta=delta_time,
                              new_repeat_time_delta_months=delta_months_count,
                              new_end_date=args.end_date,
                              new_description=args.plan_desc,
                              new_priority=args.priority)

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
