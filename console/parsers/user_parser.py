"""
    Parser for work with current user
"""
from console.service.show_formats import user_full_format


def init_user_parser(subparsers):
    user_parser = subparsers.add_parser('user', help='Parser to work with user')
    user_parser.add_argument('-s', '--show', dest='show_user',
                             action='store_true', help='Show user info')
    user_parser.add_argument('-e', '--edit', dest='edit_user',
                             action='store_true', help='Edit user')

    user_parser.add_argument('-n', '--user_name', dest='user_name', help='User name')
    user_parser.add_argument('-E', '--email', dest='user_email', help='User email')

    user_parser.add_argument('-a', '--give_access', dest='give_access',
                             action='store_true', help='Give another user access.')
    user_parser.add_argument('-t', '--to_task', dest='to_task',
                             action='store_true', help='Change other user access type to task')
    user_parser.add_argument('-g', '--to_group', dest='to_group',
                             action='store_true', help='Change other user access type to group')
    user_parser.add_argument('-uid', '--another_user_id', dest='another_user_id',
                             type=int, help='Another user id')
    user_parser.add_argument('-tid', '--task_id', dest='task_id',
                             type=int, help='Task id')
    user_parser.add_argument('-gid', '--group_id', dest='group_id',
                             type=int, help='Group id')
    user_parser.add_argument('-at', '--access_type', dest='access_type', type=int,
                             choices=[1, 2, 3], help='Access type')

    user_parser.set_defaults(parser_level='user')

    return user_parser


def parse_args(parser, config, tasktracker, args):
    if args.parser_level != 'user':
        return

    user_id = tasktracker.get_user_id_by_user_name(config.get_user_name())

    if args.show_user:
        user_name = config.get_user_name()
        user = tasktracker.get_user_by_name(user_name)

        print(user_full_format(tasktracker, user))

        return

    if args.edit_user:
        if args.user_name is not None:
            is_existence = tasktracker.check_user_existence(args.user_name)
            if is_existence:
                parser.error('A user with that name already exists.')
            config.set_user_name(args.user_name)

        if args.user_email is not None:
            config.set_user_email(args.user_email)

        tasktracker.edit_user(user_id,
                              new_user_name=args.user_name,
                              new_user_email=args.user_email)

        return

    if args.give_access:
        user_name = config.get_user_name()

        if args.another_user_id is None:
            parser.error('Another user id must be set.')

        if args.access_type is None:
            parser.error('Access type must be set.')

        if args.to_task and args.to_group:
            parser.error('Arguments to_task and to_group can not be used at the same time.')

        if args.to_task:
            if args.task_id is None:
                parser.error('Task id must be set.')

            tasktracker.change_other_user_access_type_to_task(user_name,
                                                              args.another_user_id,
                                                              args.task_id,
                                                              args.access_type)

            return

        if args.to_group:
            if args.group_id is None:
                parser.error('Group id must be set.')

            tasktracker.change_other_user_access_type_to_group(user_name,
                                                               args.another_user_id,
                                                               args.group_id,
                                                               args.access_type)
            return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
