"""
    Parser for work with single group
"""
from console.service.show_formats import (
    group_full_format,
    group_tasks_short_format,
    group_users_short_format
)


def init_group_parser(subparsers):
    group_parser = subparsers.add_parser('group', help='Parser to work with a one group')
    group_parser.add_argument('group_id', type=int, help='Group id')
    group_parser.add_argument('-s', '--show', dest='show_group',
                              action='store_true', help='Show group info')
    group_parser.add_argument('-e', '--edit', dest='edit_group',
                              action='store_true', help='Edit group')

    group_parser.add_argument('-n', '--group_name', dest='group_name', help='Group name')
    group_parser.add_argument('-desc', '--description', dest='group_desc',
                              type=str, help='Group description')

    group_parser.add_argument('-a', '--add_task', dest='add_task',
                              action='store_true', help='Add task to group')
    group_parser.add_argument('-r', '--remove_task', dest='remove_task',
                              action='store_true', help='Remove task from group')
    group_parser.add_argument('-tid', '--task_id', dest='task_id',
                              type=int, help='Task id')

    group_parser.add_argument('-t', '--tasks', dest='show_group_tasks',
                              action='store_true', help='Show group tasks')
    group_parser.add_argument('-u', '--users', dest='show_group_users',
                              action='store_true', help='Show group users')

    group_parser.set_defaults(parser_level='group')

    return group_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'group':
        return

    if args.edit_group:
        tasktracker.edit_group(user_name,
                               args.group_id,
                               new_group_name=args.group_name,
                               new_group_description=args.group_desc)
        return

    if args.show_group:
        if args.show_group_tasks:
            tasks = tasktracker.get_group_tasks(user_name, args.group_id)

            if not tasks:
                print('\nNo tasks\n')
            else:
                print('\n[TASKS]\n')

            print(group_tasks_short_format(tasktracker, user_name, args.group_id))

            return

        if args.show_group_users:
            users = tasktracker.get_group_users(user_name, args.group_id)

            if not users:
                print('\nNo users\n')
            else:
                print('\n[USERS]\n')

            print(group_users_short_format(tasktracker, user_name, args.group_id))

            return

        group = tasktracker.get_group_by_id(user_name, args.group_id)
        print(group_full_format(tasktracker, user_name, group))

        return

    if args.add_task:
        if args.task_id is None:
            parser.error("Task id must be set.")

        tasktracker.add_task_to_group(user_name, args.group_id, args.task_id)

        return

    if args.remove_task:
        if args.task_id is None:
            parser.error("Task id must be set.")

        tasktracker.delete_task_from_group(user_name, args.group_id, args.task_id)

        return

    if not args.show_group:
        if args.show_group_tasks or args.show_group_users:
            parser.error('Args show_group_tasks and show_group_users can not be used without arg show_group')

    parser.error('Unknown command. Perhaps you used unknown arguments.')
