"""
    Parser for work with config file
"""
from console.service.logger import update_logger


def init_config_parser(subparsers):
    config_parser = subparsers.add_parser('config', help='Parser to work with configuration')
    config_parser.add_argument('-d', '--default', dest='default_config',
                               action='store_true', help='Set default configuration settings')
    config_parser.add_argument('-s', '--show', dest='show_config',
                               action='store_true', help='Show configuration settings')
    config_parser.add_argument('-c', '--check', dest='check_config',
                               action='store_true', help='Check configuration settings')
    config_parser.add_argument('-e', '--edit', dest='edit_config',
                               help='Edit basic configuration settings')
    config_parser.add_argument('-u', '--user', dest='user',
                               action='store_true', help='Set current user')
    config_parser.add_argument('-cs', '--clear_storage', dest='clear_storage',
                               action='store_true', help='Clear storage')

    config_parser.add_argument('-un', '--user_name', dest='user_name', help='Set user name')
    config_parser.add_argument('-ue', '--user_email', dest='user_email', help='Set ser email')

    config_parser.add_argument('-sp', '--storage_path', dest='storage_path', help='Set storage path')
    config_parser.add_argument('-cfg_p', '--config_path', dest='config_path', help='Set config path')
    config_parser.add_argument('-cfg_f', '--config_file_name', dest='config_file_name',
                               help='Set config file name')

    config_parser.add_argument('-log', '--logging', dest='logging',
                               action='store_true', help='Change logging settings')
    config_parser.add_argument('-l', '--level', dest='logging_level', type=int,
                               choices=[0, 1, 2, 3, 4], help='Change logging level')
    config_parser.add_argument('-m', '--mode', dest='logging_mode',
                               choices=['on', 'off'], help='Change logging mode')
    config_parser.add_argument('-p', '--path', dest='logging_path', help='Change logging path')
    config_parser.add_argument('-f', '--file_name', dest='log_file_name',
                               help='Change log file name')

    config_parser.set_defaults(parser_level='config')

    return config_parser


def parse_args(parser, config, tasktracker, args):
    if args.parser_level != 'config':
        return

    if args.clear_storage:
        tasktracker.clear_storage()
        return

    if args.default_config:
        config.set_default_config()
        return

    if args.show_config:
        config.show_config()
        return

    if args.check_config:
        config.check_config()
        print('Config file is ok.')
        return

    if args.edit_config:
        if args.storage_path is not None:
            config.set_storage_path(args.storage_path)

        if args.config_path is not None:
            config.set_config_path(args.config_path)

        if args.config_file_name is not False:
            config.set_config_file_name(args.config_file_name)

        return

    if args.logging:
        if args.logging_path is not None:
            config.set_logging_path(args.logging_path)

        if args.log_file_name is not None:
            config.set_log_file_name(args.log_file_name)

        if args.logging_level is not None:
            config.set_logging_level(args.logging_level)

        if args.logging_mode is not None:
            config.set_logging_mode(args.logging_mode)

        update_logger(config)

        return

    if args.user:
        if args.user_name is None:
            parser.error('User name must be set.')

        config.set_user(args.user_name, user_email=args.user_email)

        is_existence = tasktracker.check_user_existence(args.user_name)
        if not is_existence:
            tasktracker.create_user(args.user_name, user_email=args.user_email)

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
