"""
    Parser for work with single task
"""
import datetime

from console.service.show_formats import (
    task_subtasks_short_format,
    task_users_short_format,
    task_groups_short_format,
    task_full_format
)


def init_task_parser(subparsers):
    task_parser = subparsers.add_parser('task', help='Parser to work with a one task')
    task_parser.add_argument('task_id', type=int, help='Task id')
    task_parser.add_argument('-s', '--show', dest='show_task',
                             action='store_true', help='Show task info')
    task_parser.add_argument('-e', '--edit', dest='edit_task',
                             action='store_true', help='Edit task')
    task_parser.add_argument('-mv', '--move', dest='move_task',
                             action='store_true', help='Move task tree')

    task_parser.add_argument('-n', '--task_name', dest='task_name', help='Task name')
    task_parser.add_argument('-desc', '--description', dest='task_desc',
                             type=str, help='Task description')
    task_parser.add_argument('-p', '--priority', dest='priority', type=int,
                             choices=[0, 1, 2, 3], help='Task priority')

    task_parser.add_argument('-sd', '--start_date', dest='start_date',
                             type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                             help='Start date(in HH:MM DD.MM.YYYY format)')
    task_parser.add_argument('-ed', '--end_date', dest='end_date',
                             type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                             help='End date in HH:MM DD.MM.YYYY format')

    task_parser.add_argument('-a', '--add_subtask', dest='add_subtask',
                             action='store_true', help='Add subtask to task')
    task_parser.add_argument('-r', '--remove_subtask', dest='remove_subtask',
                             action='store_true', help='Remove subtask from task')
    task_parser.add_argument('-t', '--relation_type', dest='relation_type',
                             type=int, choices=[0, 1, 2], help='Relation type')
    task_parser.add_argument('-sid', '--subtask_id', dest='subtask_id',
                             type=int, help='Subtask id')
    task_parser.add_argument('-pid', '--parent_task_id', dest='parent_task_id',
                             type=int, help='Parent task id')

    task_parser.add_argument('-cst', '--change_status', dest='change_status',
                             action='store_true', help='Change task status')
    task_parser.add_argument('-st', '--status', dest='status', type=int,
                             choices=[0, 1, 2, 3], help='Task status')
    task_parser.add_argument('-arch', '--archive', dest='archive',
                             action='store_true', help='Add task to archive')

    task_parser.add_argument('-sub', '--subtasks', dest='show_task_subtasks',
                             action='store_true', help='Show task subtasks')
    task_parser.add_argument('-u', '--users', dest='show_task_users',
                             action='store_true', help='Show task users')
    task_parser.add_argument('-g', '--groups', dest='show_task_groups',
                             action='store_true', help='Show task groups')

    task_parser.set_defaults(parser_level='task')

    return task_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'task':
        return

    if args.show_task:
        if args.show_task_subtasks:
            subtasks = tasktracker.get_task_subtasks(user_name, args.task_id)

            if not subtasks:
                print('\nNo subtasks\n')
            else:
                print('\n[SUBTASKS]\n')

            print(task_subtasks_short_format(tasktracker, user_name, args.task_id))

            return

        if args.show_task_users:
            users = tasktracker.get_task_users(user_name, args.task_id)

            if not users:
                print('\nNo users\n')
            else:
                print('\n[USERS]\n')

            print(task_users_short_format(tasktracker, user_name, args.task_id))

            return

        if args.show_task_groups:
            groups = tasktracker.get_task_groups(user_name, args.task_id)

            if not groups:
                print('\nNo groups\n')
            else:
                print('\n[GROUPS]\n')

            print(task_groups_short_format(tasktracker, user_name, args.task_id))

            return

        task = tasktracker.get_task_by_id(user_name, args.task_id)
        print(task_full_format(tasktracker, user_name, task))

        return

    if args.edit_task:
        tasktracker.edit_task(user_name,
                              args.task_id,
                              new_task_name=args.task_name,
                              new_description=args.task_desc,
                              new_priority=args.priority,
                              new_start_date=args.start_date,
                              new_end_date=args.end_date)

        return

    if args.move_task:
        tasktracker.move_task_tree(user_name,
                                   args.task_id,
                                   new_root_task_id=args.parent_task_id,
                                   task_relation_type=args.relation_type)

        return

    if args.change_status:
        if args.status is None:
            parser.error('New task status must be set.')

        tasktracker.change_task_status(user_name,
                                       args.task_id,
                                       args.status)

        return

    if args.archive:
        tasktracker.add_task_to_archive(user_name, args.task_id)
        return

    if args.add_subtask:
        if args.subtask_id is None:
            parser.error('Subtask id must be set.')

        tasktracker.add_subtask(user_name,
                                args.task_id,
                                args.subtask_id,
                                task_relation_type=args.relation_type)

        return

    if args.remove_subtask:
        if args.subtask_id is None:
            parser.error('Subtask id must be set.')

        tasktracker.delete_subtask(user_name,
                                   args.task_id,
                                   args.subtask_id)

        return

    if not args.show_task:
        if args.show_task_subtasks or args.show_task_users or args.show_task_groups:
            parser.error('Args show_task_tasksm, show_task_users, show_task_groups '
                         'can not be used without arg show_task')

    parser.error('Unknown command. Perhaps you used unknown arguments.')
