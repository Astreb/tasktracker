"""
    Parser for working with user plans
"""
import datetime

from console.service.show_formats import user_plans_short_format


def init_plans_parser(subparsers):
    plans_parser = subparsers.add_parser('plans', help='Parser to work with a list of all plans')
    plans_parser.add_argument('-c', '--create', dest='create_plan',
                              action='store_true', help='Create new plan')
    plans_parser.add_argument('-r', '--remove', dest='remove_plan',
                              action='store_true', help='Remove plan')
    plans_parser.add_argument('-s', '--show', dest='show_plans',
                              action='store_true', help='Show user plans')

    plans_parser.add_argument('-n', '--plan_task_name', dest='plan_task_name', help='Plan task name')
    plans_parser.add_argument('-id', '--plan_id', dest='plan_id',
                              type=int, help='Plan id')
    plans_parser.add_argument('-desc', '--description', dest='plan_desc',
                              type=str, help='Plan task description')
    plans_parser.add_argument('-p', '--priority', dest='priority', type=int,
                              choices=[0, 1, 2, 3], default=1, help='Plan task priority')

    plans_parser.add_argument('-ct', '-task_creation_time', dest='task_creation_time',
                              type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                              help='Task creation time(in HH:MM DD.MM.YYYY format)')
    plans_parser.add_argument('-sd', '--start_date', dest='start_date',
                              type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                              help='Start date(in HH:MM DD.MM.YYYY format)')
    plans_parser.add_argument('-ed', '--end_date', dest='end_date',
                              type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                              help='End date in HH:MM DD.MM.YYYY format')

    plans_parser.add_argument('-M', '--months', dest='delta_months_count', default=0,
                              type=int, help='The plan task will be repeated in M months.')
    plans_parser.add_argument('-w', '--weeks', dest='delta_weeks_count', default=0,
                              type=int, help='The plan task will be repeated in w weeks.')
    plans_parser.add_argument('-d', '--days', dest='delta_days_count', default=0,
                              type=int, help='The plan task will be repeated in d days.')
    plans_parser.add_argument('-H', '--hours', dest='delta_hours_count', default=0,
                              type=int, help='The plan task will be repeated in H hours.')
    plans_parser.add_argument('-m', '--minutes', dest='delta_minutes_count', default=0,
                              type=int, help='The plan task will be repeated in m minutes.')

    plans_parser.set_defaults(parser_level='plans')

    return plans_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'plans':
        return

    if args.create_plan:
        if args.plan_task_name is None:
            parser.error('Plan task name must be set.')

        if args.task_creation_time is None:
            parser.error('Task creation time must be set.')

        if args.start_date is None:
            parser.error('Start date must be set.')

        if (args.delta_months_count < 0 or args.delta_weeks_count < 0 or args.delta_days_count < 0 or
                args.delta_hours_count < 0 or args.delta_minutes_count < 0):
            parser.error('Time arguments can not be negative')

        delta_time = datetime.timedelta(weeks=args.delta_weeks_count,
                                        days=args.delta_days_count,
                                        hours=args.delta_hours_count,
                                        minutes=args.delta_minutes_count)
        if args.delta_months_count == 0 and str(delta_time) == '0:00:00':
            parser.error('Repeat delta time must be positive.')

        tasktracker.create_plan(user_name,
                                args.plan_task_name,
                                args.task_creation_time,
                                args.start_date,
                                repeat_time_delta=delta_time,
                                repeat_time_delta_months=args.delta_months_count,
                                end_date=args.end_date,
                                description=args.plan_desc,
                                priority=args.priority)

        return

    if args.remove_plan:
        if args.plan_id is None:
            parser.error('Plan id must be set.')

        tasktracker.delete_plan(user_name, args.plan_id)

        return

    if args.show_plans:
        plans = tasktracker.get_user_plans(user_name)

        if not plans:
            print('\nNo plans')
            return
        else:
            print('\n[USER_PLANS]')

        print(user_plans_short_format(tasktracker, user_name))

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
