"""
    Parser for work with single notification
"""
import datetime

from console.service.show_formats import notification_show_format


def init_notification_parser(subparsers):
    notification_parser = subparsers.add_parser('notification', help='Parser to work with single notification')
    notification_parser.add_argument('-s', '--show', dest='show_notification',
                                     action='store_true', help='Show selected notification')
    notification_parser.add_argument('-e', '--edit', dest='edit_notification',
                                     action='store_true', help='Edit notification')

    notification_parser.add_argument('notification_id', type=int, help='Notification id')
    notification_parser.add_argument('-nm', '--notification_message', dest='notification_message',
                                     type=str, help='Notification message')
    notification_parser.add_argument('-ct', '--creation_time', dest='notification_creation_time',
                                     type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                                     help='Creation time in HH:MM DD.MM.YYYY format')

    notification_parser.set_defaults(parser_level='notification')

    return notification_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'notification':
        return

    if args.show_notification:
        notification = tasktracker.get_notification_by_id(args.notification_id)
        print(notification_show_format(notification))

        return

    if args.edit_notification:
        tasktracker.edit_notification(user_name,
                                      args.notification_id,
                                      args.notification_creation_time,
                                      args.notification_message)

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
