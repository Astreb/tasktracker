"""
    Provides all TaskTracker console application command arguments parsers

    Modules:
        group_parser - parser for work with single group
        groups_parser - parser for work with all user groups
        task_parser - parser for work with single task
        tasks_parser - parser for working with user tasks
        plan_parser - parser for work with single plan
        plans_parser - parser for working with user plans
        user_parser - parser for work with current user
        config_parser - parser for work with config file
        notification_parser - parser for work with notifications

"""