"""
    Parser for working with user tasks
"""
import datetime

from console.service.show_formats import user_tasks_short_format


def init_tasks_parser(subparsers):
    tasks_parser = subparsers.add_parser('tasks', help='Parser to work with a list of all tasks')
    tasks_parser.add_argument('-c', '--create', dest='create_task',
                              action='store_true', help='Create new task')
    tasks_parser.add_argument('-r', '--remove', dest='remove_task',
                              action='store_true', help='Remove task')
    tasks_parser.add_argument('-s', '--show', dest='show_tasks',
                              action='store_true', help='Show user tasks')

    tasks_parser.add_argument('-n', '--task_name', dest='task_name', help='Task name')
    tasks_parser.add_argument('-id', '--task_id', dest='task_id',
                              type=int, help='Task id')
    tasks_parser.add_argument('-desc', '--description', dest='task_desc',
                              type=str, help='Task description')
    tasks_parser.add_argument('-p', '--priority', dest='priority', type=int,
                              choices=[0, 1, 2, 3], default=1, help='Task priority')

    tasks_parser.add_argument('-sd', '--start_date', dest='start_date',
                              type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                              help='Start date(in HH:MM DD.MM.YYYY format)')
    tasks_parser.add_argument('-ed', '--end_date', dest='end_date',
                              type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                              help='End date in HH:MM DD.MM.YYYY format')

    tasks_parser.set_defaults(parser_level='tasks')

    return tasks_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'tasks':
        return

    if args.create_task:
        if args.task_name is None:
            parser.error('Task name must be set.')
        if args.start_date is None:
            parser.error('Start date must be set.')

        tasktracker.create_task(user_name,
                                args.task_name,
                                args.start_date,
                                end_date=args.end_date,
                                description=args.task_desc,
                                priority=args.priority)

        return

    if args.remove_task:
        if args.task_id is None:
            parser.error('Task id must be set.')

        tasktracker.delete_task(user_name, args.task_id)

        return

    if args.show_tasks:
        tasks = tasktracker.get_user_tasks(user_name)

        if not tasks:
            print('\nNo tasks')
            return
        else:
            print('\n[USER_TASKS]')

        print(user_tasks_short_format(tasktracker, user_name))

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
