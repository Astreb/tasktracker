"""
    Parser for work with notifications
"""
import datetime

from console.service.show_formats import (
    user_notifications_show_format,
    user_messages_show_format
)


def init_notifications_parser(subparsers):
    notifications_parser = subparsers.add_parser('notifications', help='Parser to work with notifications')
    notifications_parser.add_argument('-s', '--show', dest='show_notifications',
                                      action='store_true', help='Show all user notifications')
    notifications_parser.add_argument('-m', '--messages', dest='show_messages',
                                      action='store_true', help="Show all user notification messages")
    notifications_parser.add_argument('-c', '--create', dest='create_notification',
                                      action='store_true', help='Create new notification')
    notifications_parser.add_argument('-r', '--remove', dest='remove_notification',
                                      action='store_true', help='Remove notification')
    notifications_parser.add_argument('-e', '--edit', dest='edit_notification',
                                      action='store_true', help='Edit notification')

    notifications_parser.add_argument('-nid', '--notification_id', dest='notification_id',
                                      type=int, help='Notification id')
    notifications_parser.add_argument('-nm', '--notification_message', dest='notification_message',
                                      type=str, help='Notification message')
    notifications_parser.add_argument('-ct', '--creation_time', dest='notification_creation_time',
                                      type=lambda date: datetime.datetime.strptime(date, '%H:%M %d.%m.%Y'),
                                      help='Creation time in HH:MM DD.MM.YYYY format')

    notifications_parser.set_defaults(parser_level='notifications')

    return notifications_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'notifications':
        return

    if args.show_notifications:
        notifications = tasktracker.get_user_notifications(user_name)

        if not notifications:
            print('\nNo notifications')
            return
        else:
            print('\n[USER_NOTIFICATIONS]')

        print(user_notifications_show_format(tasktracker, user_name))

        return

    if args.show_messages:
        messages = tasktracker.get_user_messages(user_name)

        if not messages:
            print('\nNo messages')
            return
        else:
            print('\n[USER_MESSAGES]')

        print(user_messages_show_format(tasktracker, user_name))

        return

    if args.create_notification:
        if args.notification_message is None:
            parser.error('Notification message must be set.')

        if args.notification_creation_time is None:
            parser.error('Notification creation time must be set.')

        tasktracker.create_notification(user_name,
                                        args.notification_creation_time,
                                        args.notification_message)

        return

    if args.remove_notification:
        if args.notification_id is None:
            parser.error('Notification id must be set.')

        tasktracker.delete_notification(user_name, args.notification_id)

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
