"""
    Parser for work with all user groups
"""
from console.service.show_formats import user_groups_short_format


def init_groups_parser(subparsers):
    groups_parser = subparsers.add_parser('groups', help='Parser to work with a list of all groups')
    groups_parser.add_argument('-c', '--create', dest='create_group',
                               action='store_true', help='Create new group')
    groups_parser.add_argument('-r', '--remove', dest='remove_group',
                               action='store_true', help='Remove group')
    groups_parser.add_argument('-s', '--show', dest='show_groups',
                               action='store_true', help='Show user groups')

    groups_parser.add_argument('-n', '--group_name', dest='group_name', help='Group name')
    groups_parser.add_argument('-desc', '--description', dest='group_desc',
                               type=str, help='Group description')
    groups_parser.add_argument('-id', '--group_id', type=int,
                               dest='group_id', help='Group id')

    groups_parser.set_defaults(parser_level='groups')

    return groups_parser


def parse_args(parser, user_name, tasktracker, args):
    if args.parser_level != 'groups':
        return

    if args.create_group:
        if args.group_name is None or args.group_name == '':
            parser.error('Group name must be set.')
        tasktracker.create_group(user_name,
                                 args.group_name,
                                 group_description=args.group_desc)

        return

    if args.remove_group:
        if args.group_id is None:
            parser.error('Group id must by set.')
        tasktracker.delete_group(user_name, args.group_id)

        return

    if args.show_groups:
        groups = tasktracker.get_user_groups(user_name)

        if not groups:
            print('\nNo groups')
            return
        else:
            print('\n[USER_GROUPS]')

        print(user_groups_short_format(tasktracker, user_name))

        return

    parser.error('Unknown command. Perhaps you used unknown arguments.')
