"""
    Provides modules for console TaskTracker service

    Modules:
        config - provides classes for working with the configuration file
        logger - provides function and enum for work with application logger
        show_formats - contains functions that format data for console output

"""