"""
    Provides classes for working with the configuration file

    Classes:
        ConfigConstNames - contains basic constants for working with configuration file
        ConfigManager - contains all functions for working with configuration file

"""
import configparser
import os

from console.service.logger import LoggingLevel
from library.service.exceptions import ConfigNotFound, IncorrectConfig


class ConfigConstNames:
    """Contains basic constants for working with configuration file"""
    USER_SECTION = 'User'
    STORAGE_SECTION = 'Storage'
    LOGGING_SECTION = 'Logging'
    USER = 'user'
    EMAIL = 'email'
    DEFAULT_CONFIG_PATH = '~/TaskTracker'
    DEFAULT_TESTS_PATH = '~/TaskTracker/tests'
    DEFAULT_LOGGING_PATH = '~/TaskTracker'
    DEFAULT_STORAGE_PATH = '~/TaskTracker/storage'
    DEFAULT_LOGGING_LEVEL = 'DEBUG'
    DEFAULT_LOG_FILE_NAME = 'log'
    LOGGING_ON = 'on'
    LOGGING_OFF = 'off'
    LOGGING_MODE = 'logging_mode'
    LOGGING_LEVEL = 'logging_level'
    PATH = 'path'
    CONFIG_FILE_NAME = 'config.ini'
    LOG_FILE_NAME = 'log_file_name'


class ConfigManager:
    """
        Contains all functions for working with configuration file

        Attributes:
            config_file_name(str) - name of config file
            config_path(str) - path to config file
            config(ConfigParser object) - object that works with config file

        Functions:
                show_config - print to console content of config file
                check_config - verifies correctness of configuration file
                set_default_config - set default settings in config file
                set_user - set current user
                get_user_name - returns name of current user
                get_user_email - returns email of current user
                set_user_name - change name for current user
                set_user_email - change email for current user
                set_storage_path - set path to storage directory
                get_storage_path - returns path to storage directory
                set_config_path - set path to config directory
                get_config_path - returns path to config directory
                set_config_file_name - set new config file name
                get_config_file_name - returns name of config file
                set_logging_path - set path to log file directory
                get_logging_path - returns path to log file directory
                set_log_file_name - set new name for log file
                get_log_file_name - returns name of log file
                set_logging_level - set new logging level
                get_logging_level - returns current logging level
                set_logging_mode - set new logging  mode
                get_logging_mode - returns current logging mode

    """
    def __init__(self):
        """Set ConfigManager attributes"""
        self.config_file_name = ConfigConstNames.CONFIG_FILE_NAME
        self.config_path = os.path.expanduser(os.path.join(ConfigConstNames.DEFAULT_CONFIG_PATH,
                                                           ConfigConstNames.CONFIG_FILE_NAME))

        self.config = configparser.RawConfigParser()

        if not os.path.exists(self.config_path):
            self.set_default_config()

    def show_config(self):
        """
            Print to console content of config file

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config.read(self.config_path)
        for section in self.config.sections():
            print('[{}]'.format(section))
            for item in self.config.items(section):
                print(item)
            print()

    def check_config(self):
        """
            Verifies correctness of configuration file

            Raises:
                ConfigNotFound exception, if config file is not found
                IncorrectConfig exception, if user name is empty string
                IncorrectConfig exception, if path to storage directory is incorrect
                IncorrectConfig exception, if path to logging directory is incorrect
                IncorrectConfig exception, if name of log file is empty string
                IncorrectConfig exception, if logging level is incorrect
                IncorrectConfig exception, if logging mode is incorrect

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config.read(self.config_path)

        user = self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.USER]
        if user == '':
            raise IncorrectConfig('User name can not be empty.')

        path = self.get_storage_path()
        if not path.startswith('~/'):
            raise IncorrectConfig('Incorrect storage directory path.')

        path = self.get_logging_path()
        if not path.startswith('~/'):
            raise IncorrectConfig('Incorrect logging directory path.')

        log_file_name = self.get_log_file_name()
        if log_file_name == '':
            raise IncorrectConfig('Incorrect log file name.')

        logging_level = self.get_logging_level()
        if logging_level not in range(5):
            raise IncorrectConfig('Incorrect logging level.')

        logging_mode = self.get_logging_mode()
        if logging_mode != ConfigConstNames.LOGGING_ON and logging_mode != ConfigConstNames.LOGGING_OFF:
            raise IncorrectConfig('Incorrect logging mode.')

    def set_default_config(self):
        """Set default settings in config file"""
        if not self.config.has_section(ConfigConstNames.USER_SECTION):
            self.config.add_section(ConfigConstNames.USER_SECTION)
        self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.USER] = 'guest'
        self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.EMAIL] = 'None'

        if not self.config.has_section(ConfigConstNames.STORAGE_SECTION):
            self.config.add_section(ConfigConstNames.STORAGE_SECTION)
        self.config[ConfigConstNames.STORAGE_SECTION][ConfigConstNames.PATH] = ConfigConstNames.DEFAULT_STORAGE_PATH

        if not self.config.has_section(ConfigConstNames.LOGGING_SECTION):
            self.config.add_section(ConfigConstNames.LOGGING_SECTION)
        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.PATH] = ConfigConstNames.DEFAULT_LOGGING_PATH
        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_MODE] = ConfigConstNames.LOGGING_ON
        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_LEVEL] = ConfigConstNames.DEFAULT_LOGGING_LEVEL
        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOG_FILE_NAME] = ConfigConstNames.DEFAULT_LOG_FILE_NAME

        if not os.path.exists(os.path.expanduser(ConfigConstNames.DEFAULT_CONFIG_PATH)):
            os.mkdir(os.path.expanduser(ConfigConstNames.DEFAULT_CONFIG_PATH))

        if not os.path.exists(os.path.expanduser(ConfigConstNames.DEFAULT_LOGGING_PATH)):
            os.mkdir(os.path.expanduser(ConfigConstNames.DEFAULT_LOGGING_PATH))

        if not os.path.exists(os.path.expanduser(ConfigConstNames.DEFAULT_STORAGE_PATH)):
            os.mkdir(os.path.expanduser(ConfigConstNames.DEFAULT_STORAGE_PATH))

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def set_user(self, user_name, user_email=None):
        """
            Set current user

            Args:
                user_name(str) - name of user
                user_email(str) - email of user

            Raises:
                ConfigNotFound exception, if config file is not found
                IncorrectConfig exception, if user name is empty string

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        if user_name == '':
            raise IncorrectConfig('User name must be not empty string.')

        self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.USER] = user_name
        if user_email is not None:
            self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.EMAIL] = user_email

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_user_name(self):
        """
            Returns name of current user

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.USER]

    def get_user_email(self):
        """
            Returns email of current user

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.EMAIL]

    def set_user_name(self, user_name):
        """
            Change name for current user

            Args:
                user_name(str) - new user name

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.USER] = user_name

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def set_user_email(self, user_email):
        """
            Change email for current user

            Args:
                user_email(str) - new user email

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config[ConfigConstNames.USER_SECTION][ConfigConstNames.EMAIL] = user_email

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def set_storage_path(self, storage_path):
        """
            Set path to storage directory

            Args:
                storage_path(str) - new path to storage directory

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config[ConfigConstNames.STORAGE_SECTION][ConfigConstNames.PATH] = storage_path

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_storage_path(self):
        """
            Returns path to storage directory

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.STORAGE_SECTION][ConfigConstNames.PATH]

    def set_config_path(self, config_path):
        """
            Set path to config directory

            Args:
                config_path(str) - new path to config directory

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not config_path.startswith('~/'):
            raise ValueError('Incorrect config directory path.')

        self.config_path = os.path.expanduser(os.path.join(config_path,
                                                           self.config_file_name))

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_config_path(self):
        """Returns path to config directory"""
        return self.config_path

    def set_config_file_name(self, config_file_name):
        """
            Set new config file name

            Args:
                config_file_name(str) - new name of config file

            Raises:
                ConfigNotFound exception, if config file is not found
                ValueError exception, if new name of config file is incorrect

        """
        if config_file_name == '':
            raise ValueError('Incorrect config file name.')

        self.config_file_name = config_file_name
        self.config_path = os.path.expanduser(os.path.join(self.config_path,
                                                           self.config_file_name))

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_config_file_name(self):
        """Returns name of config file"""
        return self.config_file_name

    def set_logging_path(self, logging_path):
        """
            Set path to log file directory

            Args:
                logging_path(str) - new path to log file directory

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.PATH] = logging_path

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_logging_path(self):
        """
            Returns path to log file directory

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.PATH]

    def set_log_file_name(self, logging_file_name):
        """
            Set new name for log file

            Args:
                logging_file_name(str) - new name of log file

            Raises:
                ConfigNotFound exception, if config file is not found
                ValueError exception, if new log file name is empry strinf

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        if logging_file_name == '':
            raise ValueError('Log file name must be not empty string.')

        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOG_FILE_NAME] = logging_file_name

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_log_file_name(self):
        """
            Returns name of log file

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOG_FILE_NAME]

    def set_logging_level(self, logging_level):
        """
            Set new logging level

            Args:
                logging_level(int, LoggingLevel enum) - new logging level

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        logging_level = LoggingLevel(logging_level)
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        logging_level = LoggingLevel(logging_level).name
        self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_LEVEL] = logging_level

        with open(self.config_path, 'w') as file:
            self.config.write(file)

    def get_logging_level(self):
        """
            Returns current logging level

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        logging_level = self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_LEVEL]
        return LoggingLevel[logging_level].value

    def get_logging_mode(self):
        """
            Returns current logging mode

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        return self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_MODE]

    def set_logging_mode(self, logging_mode):
        """
            Set new logging mode

            Args:
                logging_mode(str 'on' or 'off') - new logging mode

            Raises:
                ConfigNotFound exception, if config file is not found

        """
        if not os.path.exists(self.config_path):
            raise ConfigNotFound(self.config_path)

        if logging_mode is not None:
            self.config[ConfigConstNames.LOGGING_SECTION][ConfigConstNames.LOGGING_MODE] = logging_mode

        with open(self.config_path, 'w') as file:
            self.config.write(file)
