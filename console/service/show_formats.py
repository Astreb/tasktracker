"""
    Contains functions that format data for console output

    Functions:
        task_full_format - returns string with full information about task
        task_short_format - returns string with short information about task
        group_full_format - returns string with full information about group
        group_short_format - returns string with short information about group
        user_full_format - returns string with full information about user
        user_short_format - returns string with short information about user
        plan_show_format - returns string with full information about plan
        notification_show_format - returns string with full information about notification
        user_tasks_short_format - returns string with short information about user tasks
        user_groups_short_format - returns string with short information about user groups
        user_plans_short_format - returns string with short information about user plans
        user_notifications_show_format - returns string with information about user notifications
        user_messages_show_format - returns string with information about user messages
        task_subtasks_short_format - returns string with short information about task subtasks
        task_groups_short_format - returns string with short information about task groups
        task_users_short_format - returns string with short information about task users
        group_tasks_short_format - returns string with short information about group tasks
        group_users_short_format - returns string with short information about group users

"""


def task_full_format(tasktracker, user_name, task):
    """
        Returns string with full information about task

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show task info
            task(Task object) - instance of task information about which user need to get

    """
    task_repr = task_short_format(task)

    description = 'Description: {}'.format(task.description)
    parent_task_id = 'Parent task: {}'.format(task.parent_task_id)
    if task.parent_task_relation_type is not None:
        parent_task_relation_type = 'Parent task relation type: {}'.format(task.parent_task_relation_type.name)
    else:
        parent_task_relation_type = 'Parent task relation type: None'
    in_archive = 'In archive: {}'.format(task.in_archive)

    task_repr = '\n'.join((task_repr,
                           in_archive,
                           description,
                           parent_task_id,
                           parent_task_relation_type))

    task_subtasks_repr = task_subtasks_short_format(tasktracker, user_name, task.id)
    if task_subtasks_repr:
        task_repr = '\n'.join((task_repr, '\n[SUBTASKS]'))
        task_repr = '\n'.join((task_repr, task_subtasks_repr))
    else:
        task_repr = '\n'.join((task_repr, '\n[NO SUBTASKS]'))

    task_groups_repr = task_groups_short_format(tasktracker, user_name, task.id)
    if task_groups_repr:
        task_repr = '\n'.join((task_repr, '\n[GROUPS]'))
        task_repr = '\n'.join((task_repr, task_groups_repr))
    else:
        task_repr = '\n'.join((task_repr, '\n[NO GROUPS]'))

    task_users_repr = task_users_short_format(tasktracker, user_name, task.id)
    if task_users_repr:
        task_repr = '\n'.join((task_repr, '\n[USERS]'))
        task_repr = '\n'.join((task_repr, task_users_repr))
    else:
        task_repr = '\n'.join((task_repr, '\n[USERS]'))

    return task_repr


def task_short_format(task):
    """
        Returns string with short information about task

        Args:
            task(Task object) - instance of task information about which user need to get

    """
    id = 'ID: {}'.format(task.id)
    name = 'Name: {}'.format(task.name)
    start_date = 'Start date: {}'.format(task.start_date)
    end_date = 'End date: {}'.format(task.end_date)
    priority = 'Priority: {}'.format(task.priority.name)
    status = 'Status: {}'.format(task.status.name)

    task_repr = '\n'.join((id,
                           name,
                           start_date,
                           end_date,
                           priority,
                           status))

    return task_repr


def group_full_format(tasktracker, user_name, group):
    """
        Returns string with full information about group

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show group info
            group(Group object) - instance of group information about which user need to get

    """
    group_repr = group_short_format(group)

    group_tasks_repr = group_tasks_short_format(tasktracker, user_name, group.id)
    if group_tasks_repr:
        group_repr = '\n'.join((group_repr, '\n[TASKS]'))
        group_repr = '\n'.join((group_repr, group_tasks_repr))
    else:
        group_repr = '\n'.join((group_repr, '\n[NO TASKS]'))

    group_users_repr = group_users_short_format(tasktracker, user_name, group.id)
    if group_users_repr:
        group_repr = '\n'.join((group_repr, '\n[USERS]'))
        group_repr = '\n'.join((group_repr, group_users_repr))
    else:
        group_repr = '\n'.join((group_repr, '\n[NO USERS]'))

    return group_repr


def group_short_format(group):
    """
        Returns string with short information about group

        Args:
            group(Group object) - instance of group information about which user need to get

    """
    id = 'ID: {}'.format(group.id)
    name = 'Name: {}'.format(group.name)
    description = 'Description: {}'.format(group.description)

    group_repr = '\n'.join((id,
                            name,
                            description))

    return group_repr


def user_full_format(tasktracker, user):
    """
        Returns string with full information about user

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user(User object) - instance of user information about which you need to get

    """
    user_repr = user_short_format(user)

    user_tasks_repr = user_tasks_short_format(tasktracker, user.name)
    if user_tasks_repr:
        user_repr = '\n'.join((user_repr, '\n[TASKS]'))
        user_repr = '\n'.join((user_repr, user_tasks_repr))
    else:
        user_repr = '\n'.join((user_repr, '\n[NO TASKS]'))

    user_groups_repr = user_groups_short_format(tasktracker, user.name)
    if user_groups_repr:
        user_repr = '\n'.join((user_repr, '\n[GROUPS]'))
        user_repr = '\n'.join((user_repr, user_groups_repr))
    else:
        user_repr = '\n'.join((user_repr, '\n[NO GROUPS]'))

    user_plans_repr = user_plans_short_format(tasktracker, user.name)
    if user_plans_repr:
        user_repr = '\n'.join((user_repr, '\n[PLANS]'))
        user_repr = '\n'.join((user_repr, user_plans_repr))
    else:
        user_repr = '\n'.join((user_repr, '\n[NO PLANS]'))

    return user_repr


def user_short_format(user):
    """
        Returns string with short information about user

        Args:
            user(User object) - instance of user information about which you need to get

    """
    id = 'ID: {}'.format(user.id)
    name = 'Name: {}'.format(user.name)
    email = 'Email: {}'.format(user.email)

    user_repr = '\n'.join((id,
                           name,
                           email))

    return user_repr


def plan_show_format(plan):
    """
        Returns string with full information about plan

        Args:
            plan(Plan object) - instance of plan information about which user need to get

    """
    id = 'ID: {}'.format(plan.id)

    task_name = 'Task name: {}'.format(plan.task_name)
    description = 'Description: {}'.format(plan.description)
    priority = 'Priority: {}'.format(plan.priority.name)

    task_creation_time = 'Task creation time: {}'.format(plan.task_creation_time)
    start_date = 'Start date: {}'.format(plan.start_date)
    end_date = 'End date: {}'.format(plan.end_date)
    repeat_time_delta = 'Repeat time delta: {}'.format(plan.repeat_time_delta)
    repeat_time_delta_months = 'Repeat time delta months: {}'.format(plan.repeat_time_delta_months)

    plan_repr = '\n'.join((id,
                           task_name,
                           task_creation_time,
                           description,
                           priority,
                           start_date,
                           end_date,
                           repeat_time_delta,
                           repeat_time_delta_months))

    return plan_repr


def notification_show_format(notification):
    """
        Returns string with full information about notification

        Args:
            notification(Notification object) - instance of notification information about which user need to get

    """
    id = 'ID: {}'.format(notification.id)
    message_creation_time = 'Message creation time: {}'.format(notification.creation_time)
    message = 'Message: {}'.format(notification.message)

    notification_repr = '\n'.join((id,
                                   message_creation_time,
                                   message))

    return notification_repr


def user_tasks_short_format(tasktracker, user_name):
    """
        Returns string with short information about user tasks

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants get tasks info

    """
    user_tasks_repr = ''
    tasks = tasktracker.get_user_tasks(user_name)
    for task, access_type in tasks.items():
        task_repr = task_short_format(task)
        user_access_type = 'Access type: {}'.format(access_type.name)

        user_tasks_repr = '\n\n'.join((user_tasks_repr,
                                       task_repr))
        user_tasks_repr = '\n'.join((user_tasks_repr,
                                     user_access_type))

    return user_tasks_repr


def user_groups_short_format(tasktracker, user_name):
    """
        Returns string with short information about user groups

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants get groups info

    """
    user_groups_repr = ''
    groups = tasktracker.get_user_groups(user_name)
    for group, access_type in groups.items():
        group_repr = group_short_format(group)
        user_access_type = 'Access type: {}'.format(access_type.name)

        user_groups_repr = '\n\n'.join((user_groups_repr,
                                        group_repr))
        user_groups_repr = '\n'.join((user_groups_repr,
                                      user_access_type))

    return user_groups_repr


def user_plans_short_format(tasktracker, user_name):
    """
        Returns string with short information about user plans

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants get plans info

    """
    user_plans_repr = ''
    plans = tasktracker.get_user_plans(user_name)
    for plan in plans:
        plan_repr = plan_show_format(plan)

        user_plans_repr = '\n\n'.join((user_plans_repr,
                                       plan_repr))

    return user_plans_repr


def user_notifications_show_format(tasktracker, user_name):
    """
        Returns string with information about user notifications

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants get notifications info

    """
    user_notifications_repr = ''
    notifications = tasktracker.get_user_notifications(user_name)
    for notification in notifications:
        notification_repr = notification_show_format(notification)

        user_notifications_repr = '\n\n'.join((user_notifications_repr,
                                               notification_repr))

    return user_notifications_repr


def user_messages_show_format(tasktracker, user_name):
    """
        Returns string with information about user messages

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants get messages info

    """
    user_messages_repr = ''
    messages = tasktracker.get_user_messages(user_name)
    for message in messages:
        user_messages_repr = '\n\n'.join((user_messages_repr,
                                          message))

    return user_messages_repr


def task_subtasks_short_format(tasktracker, user_name, task_id):
    """
        Returns string with short information about task subtasks

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show task subtasks info
            task_id(int) - id of task information on subtasks of which user need to get

    """
    task_subtasks_repr = ''
    subtasks = tasktracker.get_task_subtasks(user_name, task_id)
    for subtask, relation_type in subtasks.items():
        subtask_repr = task_short_format(subtask)
        subtask_relation_type = 'Relation type: {}'.format(relation_type.name)

        task_subtasks_repr = '\n\n'.join((task_subtasks_repr,
                                          subtask_repr))
        task_subtasks_repr = '\n'.join((task_subtasks_repr,
                                        subtask_relation_type))

    return task_subtasks_repr


def task_groups_short_format(tasktracker, user_name, task_id):
    """
        Returns string with short information about task groups

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show task groups info
            task_id(int) - id of task information on groups of which user need to get

    """
    task_groups_repr = ''
    groups = tasktracker.get_task_groups(user_name, task_id)
    for group in groups:
        group_repr = group_short_format(group)

        task_groups_repr = '\n\n'.join((task_groups_repr,
                                        group_repr))

    return task_groups_repr


def task_users_short_format(tasktracker, user_name, task_id):
    """
        Returns string with short information about task users

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show task users info
            task_id(int) - id of task information on users of which user need to get

    """
    task_users_repr = ''
    users = tasktracker.get_task_users(user_name, task_id)
    for user, access_type in users.items():
        user_repr = user_short_format(user)
        user_access_type = 'Access type: {}'.format(access_type.name)

        task_users_repr = '\n\n'.join((task_users_repr,
                                       user_repr))
        task_users_repr = '\n'.join((task_users_repr,
                                     user_access_type))

    return task_users_repr


def group_tasks_short_format(tasktracker, user_name, group_id):
    """
        Returns string with short information about group tasks

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show group tasks info
            group_id(int) - id of group information on tasks of which user need to get

    """
    group_tasks_repr = ''
    tasks = tasktracker.get_group_tasks(user_name, group_id)
    for task in tasks:
        task_repr = task_short_format(task)

        group_tasks_repr = '\n\n'.join((group_tasks_repr,
                                        task_repr))

    return group_tasks_repr


def group_users_short_format(tasktracker, user_name, group_id):
    """
        Returns string with short information about group users

        Args:
            tasktracker(TaskTracker) - instance of TaskTracker class
            user_name(str) - name of user who wants show group users info
            group_id(int) - id of group information on users of which user need to get

    """
    group_users_repr = ''
    users = tasktracker.get_group_users(user_name, group_id)
    for user, access_type in users.items():
        user_repr = user_short_format(user)
        user_access_type = 'Access type: {}'.format(access_type.name)

        group_users_repr = '\n\n'.join((group_users_repr,
                                        user_repr))
        group_users_repr = '\n'.join((group_users_repr,
                                      user_access_type))

    return group_users_repr
