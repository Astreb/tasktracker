"""
    Provides function and enum for work with application logger

    Classes:
        LoggingLevel(Enum) - contains logging levels

    Functions:
        update_logger - update application logger settings, using app config properties

"""
import logging
from enum import Enum

import os

from library.service.lib_logger import (
    get_lib_logger,
    enable_lib_logger,
    disable_lib_logger
)


def update_logger(config):
    """
        Update application logger settings, using app config properties

        Args:
            config(ConfigManager object) - instance of ConfigManager class

    """
    logging_levels = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }

    logger = get_lib_logger()
    logging_path = config.get_logging_path()
    log_file_name = config.get_log_file_name()
    full_file_name = os.path.expanduser(os.path.join(logging_path, log_file_name))
    file_handler = logging.FileHandler(full_file_name)
    logger.addHandler(file_handler)
    logging_mode = config.get_logging_mode()
    logging_level = config.get_logging_level()

    if logging_mode == 'on':
        enable_lib_logger()
    else:
        disable_lib_logger()

    logger.setLevel(logging_levels[LoggingLevel(logging_level).name])


class LoggingLevel(Enum):
    """Contains logging levels"""
    DEBUG = 0
    INFO = 1
    WARNING = 2
    ERROR = 3
    CRITICAL = 4
