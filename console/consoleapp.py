"""
    Contains entry point for console application
"""
from argparse import ArgumentParser

import os

import sys

from console.parsers import (
    config_parser,
    group_parser,
    plan_parser,
    groups_parser,
    plans_parser,
    tasks_parser,
    task_parser,
    user_parser,
    notifications_parser,
    notification_parser
)

from console.service.config import ConfigManager
from console.service.logger import update_logger
from library.service.exceptions import UserNotFound
from library.tasktracker import TaskTracker


def main():
    """
        Entry point for console application
        Load config, init directories, create TaskTracker object, init loggers
        Parse console input args and send to lib

    """
    try:
        config = ConfigManager()
        config.check_config()

        update_logger(config)

        user_name = config.get_user_name()
        if user_name == '':
            raise UserNotFound('Before you start, log in.')
        storage_path = config.get_storage_path()
        storage_path = os.path.expanduser(storage_path)

        tasktracker = TaskTracker(storage_path)

        is_existence = tasktracker.check_user_existence(user_name)
        if not is_existence:
            user_email = config.get_user_email()
            tasktracker.create_user(user_name, user_email)

        tasktracker.update_plans(user_name)
        tasktracker.update_tasks()
        tasktracker.update_notifications(user_name)

        parser = ArgumentParser()
        subparsers = parser.add_subparsers()
        parser.set_defaults(parser_level='tasktracker')

        config_parser_ = config_parser.init_config_parser(subparsers)
        group_parser_ = group_parser.init_group_parser(subparsers)
        groups_parser_ = groups_parser.init_groups_parser(subparsers)
        plan_parser_ = plan_parser.init_plan_parser(subparsers)
        plans_parser_ = plans_parser.init_plans_parser(subparsers)
        task_parser_ = task_parser.init_task_parser(subparsers)
        tasks_parser_ = tasks_parser.init_tasks_parser(subparsers)
        user_parser_ = user_parser.init_user_parser(subparsers)
        notification_parser_ = notification_parser.init_notification_parser(subparsers)
        notifications_parser_ = notifications_parser.init_notifications_parser(subparsers)

        input_args = parser.parse_args()

        config_parser.parse_args(config_parser_,
                                 config,
                                 tasktracker,
                                 input_args)

        group_parser.parse_args(group_parser_,
                                user_name,
                                tasktracker,
                                input_args)

        groups_parser.parse_args(groups_parser_,
                                 user_name,
                                 tasktracker,
                                 input_args)

        plan_parser.parse_args(plan_parser_,
                               user_name,
                               tasktracker,
                               input_args)

        plans_parser.parse_args(plans_parser_,
                                user_name,
                                tasktracker,
                                input_args)

        task_parser.parse_args(task_parser_,
                               user_name,
                               tasktracker,
                               input_args)

        tasks_parser.parse_args(tasks_parser_,
                                user_name,
                                tasktracker,
                                input_args)

        user_parser.parse_args(user_parser_,
                               config,
                               tasktracker,
                               input_args)

        notification_parser.parse_args(notification_parser_,
                                       user_name,
                                       tasktracker,
                                       input_args)

        notifications_parser.parse_args(notifications_parser_,
                                        user_name,
                                        tasktracker,
                                        input_args)

    except Exception as ex:
        print(ex, file=sys.stderr)


if __name__ == '__main__':
    main()
