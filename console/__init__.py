"""
    TaskTracker console application package

    Packages:
        parsers - all command parsers for console application
        service - all modules for service application

    Modules:
        consoleapp - contains entry point for console application

    Usage:
        # Create task
        tasktracker tasks --create --task_name 'test task' --start_date '11:00 11.09.2018'

        # Edit task
        tasktracker task 1 --edit --description 'test description'

        # Show task info
        tasktracker task 1 --show

        ID: 1
        Name: test task
        Start date: 2018-09-11 11:00:00
        End date: None
        Priority: NORMAL
        Status: STARTED
        In archive: False
        Description: test description
        Parent task: None
        Parent task relation type: None

        [NO SUBTASKS]

        [NO GROUPS]

        [USERS]

        ID: 2
        Name: Astreb
        Email: astreb@gmail.com
        Access type: AUTHOR

        # Create group
        tasktracker groups --create --group_name 'test group'

        # Add task to group
        tasktracker group 1 --add_task --task_id 1

        # Show group info
        tasktracker group 1 --show

        ID: 1
        Name: test group
        Description: None

        [TASKS]

        ID: 1
        Name: test task
        Start date: 2018-09-11 11:00:00
        End date: None
        Priority: NORMAL
        Status: STARTED

        [USERS]

        ID: 2
        Name: Astreb
        Email: None
        Access type: AUTHOR

"""