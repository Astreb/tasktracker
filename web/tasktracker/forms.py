from django import forms
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.forms import Form, DateTimeInput, ModelForm, TextInput
from django.http import HttpResponseRedirect
from django.views import View
from django.views.generic import FormView

from .library.models.task import TaskRelation
from .library.models.user import UserType
from .models import LibUser, Task, Group, Plan, Notification


class CustomDateTimeInput(DateTimeInput):
    input_type = 'datetime-local'


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/index/"
    template_name = "tasktracker/register.html"

    def form_valid(self, form):
        form.save()
        name = form.cleaned_data['username']
        LibUser(name=name, user_auth=User.objects.get(username=name)).save()
        super(RegisterFormView, self).form_valid(form)
        login(self.request, User.objects.get(username=name))
        return HttpResponseRedirect("/index/")


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "tasktracker/login.html"
    success_url = "/index/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        super(LoginFormView, self).form_valid(form)
        return HttpResponseRedirect("/index/")


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/login/')


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'description', 'priority', 'start_date', 'end_date']
        datetime_widget = DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'})
        widgets = {
            'start_date': datetime_widget,
            'end_date': datetime_widget
        }


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'description']


class PlanForm(ModelForm):
    class Meta:
        model = Plan
        fields = ['task_name', 'description', 'priority',
                  'task_creation_time', 'start_date', 'end_date',
                  'repeat_time_delta', 'repeat_time_delta_months']
        datetime_widget = DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'})
        widgets = {
            'task_creation_time': datetime_widget,
            'start_date': datetime_widget,
            'end_date': datetime_widget
        }


class NotificationForm(ModelForm):
    class Meta:
        model = Notification
        fields = ['creation_time', 'message']
        datetime_widget = DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'})
        widgets = {
            'creation_time': datetime_widget,
        }
