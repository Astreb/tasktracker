from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.template import loader

from library.models.task import TaskRelation, TaskStatus
from library.models.user import UserType
from .exceptions_handler import exceptions_handler
from .forms import (
    TaskForm,
    GroupForm,
    PlanForm,
    NotificationForm)
from .sqlite_storage import (
    convert_task_to_model,
    convert_plan_to_model,
    convert_group_to_model,
    convert_notification_to_model,
    get_all_users)
from .tasktracker import get_tasktracker


def index(request):
    template = loader.get_template('tasktracker/index.html')
    context = {
        'user_name': request.user.username
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def tasks(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    tasks = tasktracker.get_user_tasks(user_name)

    template = loader.get_template('tasktracker/tasks.html')
    context = {
        'tasks': tasks,
        "user_name": user_name
    }

    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def plans(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    plans = tasktracker.get_user_plans(user_name)

    template = loader.get_template('tasktracker/plans.html')
    context = {
        'plans': plans,
        "user_name": user_name
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def groups(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    groups = tasktracker.get_user_groups(user_name)

    template = loader.get_template('tasktracker/groups.html')
    context = {
        'groups': groups,
        "user_name": user_name
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def notifications(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    notifications = tasktracker.get_user_notifications(user_name)

    template = loader.get_template('tasktracker/notifications.html')
    context = {
        'notifications': notifications,
        "user_name": user_name
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def messages(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    messages = tasktracker.get_user_messages(user_name)

    template = loader.get_template('tasktracker/messages.html')
    context = {
        'messages': messages,
        "user_name": user_name
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def add_task(request):
    user_name = request.user.username

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            tasktracker = get_tasktracker(user_name)
            task = form.save(commit=False)
            task_name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')
            priority = task.priority.value

            start_date = form.cleaned_data.get('start_date')
            # start_date = datetime.datetime.strftime(start_date, '%H:%M %Y-%m-%d')
            end_date = form.cleaned_data.get('end_date')
            # end_date = datetime.datetime.strftime(end_date, '%H:%M %Y-%m-%d')
            tasktracker.create_task(
                user_name,
                task_name,
                start_date,
                end_date=end_date,
                description=description,
                priority=priority
            )

            return redirect('/tasks/')
    else:
        form = TaskForm()

    return render(request, 'tasktracker/add_task.html', {'form': form,
                                                         'user_name': user_name
                                                         })


@login_required
@exceptions_handler
def show_task(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    task = tasktracker.get_task_by_id(user_name, task_id)
    subtasks = tasktracker.get_task_subtasks(user_name, task_id)
    groups = tasktracker.get_task_groups(user_name, task_id)
    users = tasktracker.get_task_users(user_name, task_id)

    print(groups)

    template = loader.get_template('tasktracker/task.html')
    context = {
        'subtasks': subtasks,
        'groups': groups,
        'users': users,
        'user_name': user_name,
        'task': task
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def edit_task(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    task = tasktracker.get_task_by_id(user_name, task_id)
    task = convert_task_to_model(task)

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            tasktracker = get_tasktracker(user_name)
            task_name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')
            priority = form.cleaned_data.get('priority')

            start_date = form.cleaned_data.get('start_date')
            # start_date = datetime.datetime.strftime(start_date, '%H:%M %Y-%m-%d')
            end_date = form.cleaned_data.get('end_date')
            # end_date = datetime.datetime.strftime(end_date, '%H:%M %Y-%m-%d')
            tasktracker.edit_task(
                user_name,
                task_id,
                new_task_name=task_name,
                new_start_date=start_date,
                new_end_date=end_date,
                new_description=description,
                new_priority=priority.value
            )

            return HttpResponseRedirect('/tasks/')
    else:
        form = TaskForm(instance=task)

    return render(request, 'tasktracker/edit_task.html', {'form': form,
                                                          'user_name': user_name
                                                          })


@login_required
@exceptions_handler
def delete_task(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    tasktracker.delete_task(user_name, task_id)
    return HttpResponseRedirect('/tasks/')


@login_required
@exceptions_handler
def share_task(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    all_users = get_all_users()
    users = []
    current_user_id = tasktracker.get_user_id_by_user_name(user_name)

    for user in all_users:
        if user.id != current_user_id:
            users.append(user)

    if request.method == 'POST':
        user_id = int(request.POST.get('user'))
        access_type = int(request.POST.get('access_type'))
        tasktracker.change_other_user_access_type_to_task(user_name,
                                                          user_id,
                                                          task_id,
                                                          access_type)

        return HttpResponseRedirect('/tasks/{}/show/'.format(task_id))

    return render(request, 'tasktracker/change_access_type.html', {'users': users,
                                                                   'user_types': list(UserType),
                                                                   'user_name': user_name
                                                                   })


@login_required
@exceptions_handler
def send_in_archive(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    tasktracker.add_task_to_archive(user_name, task_id)
    return HttpResponseRedirect('/tasks/')


@login_required
@exceptions_handler
def add_subtask(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    user_tasks = tasktracker.get_user_tasks(user_name)
    tasks = []
    for task in user_tasks:
        if task.id != task_id:
            if task.parent_task_id != task_id:
                tasks.append(task)

    if request.method == 'POST':
        subtask_id = int(request.POST.get('subtask'))
        relation_type = int(request.POST.get('relation_type'))
        tasktracker.add_subtask(user_name,
                                task_id,
                                subtask_id,
                                task_relation_type=relation_type)

        return HttpResponseRedirect('/tasks/{}/show/'.format(task_id))

    return render(request, 'tasktracker/add_subtask.html', {'tasks': tasks,
                                                            'task_relation': list(TaskRelation),
                                                            'user_name': user_name
                                                            })


@login_required
@exceptions_handler
def delete_subtask(request, task_id, subtask_id):
    task_id = int(task_id)
    subtask_id = int(subtask_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    tasktracker.delete_subtask(user_name, task_id, subtask_id)
    return HttpResponseRedirect('/tasks/{}/show/'.format(task_id))


@login_required
@exceptions_handler
def change_task_status(request, task_id):
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    user_tasks = tasktracker.get_user_tasks(user_name)
    tasks = []
    for task in user_tasks:
        if task.id != task_id:
            tasks.append(task)

    if request.method == 'POST':
        new_status = int(request.POST.get('new_status'))
        tasktracker.change_task_status(user_name,
                                       task_id,
                                       new_status)

        return HttpResponseRedirect('/tasks/{}/show/'.format(task_id))

    return render(
        request,
        'tasktracker/change_task_status.html',
        {
            'task_status': list(TaskStatus),
            'user_name': user_name
        }
    )


@login_required
@exceptions_handler
def change_user_access_type_to_task(request, task_id, user_id):
    task_id = int(task_id)
    user_id = int(user_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    all_users = get_all_users()
    users = []
    current_user_id = tasktracker.get_user_id_by_user_name(user_name)

    another_user = tasktracker.get_user_by_id(user_id)
    current_access = tasktracker.get_user_access_type_to_task(another_user.name, task_id).name

    for user in all_users:
        if user.id != current_user_id:
            users.append(user)

    if request.method == 'POST':
        access_type = int(request.POST.get('access_type'))
        tasktracker.change_other_user_access_type_to_task(user_name,
                                                          user_id,
                                                          task_id,
                                                          access_type)

        return HttpResponseRedirect('/tasks/{}/show/'.format(task_id))

    return render(request, 'tasktracker/change_access_type.html', {'current_access': current_access,
                                                                   'user_id': user_id,
                                                                   'users': users,
                                                                   'user_types': list(UserType),
                                                                   'user_name': user_name
                                                                   })


@login_required
@exceptions_handler
def add_group(request):
    user_name = request.user.username

    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            tasktracker = get_tasktracker(user_name)
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')

            tasktracker.create_group(
                user_name,
                name,
                group_description=description
            )

            return HttpResponseRedirect('/groups/')
    else:
        form = GroupForm()

    return render(request, 'tasktracker/add_group.html', {'form': form,
                                                          'user_name': user_name
                                                          })


@login_required
@exceptions_handler
def show_group(request, group_id):
    group_id = int(group_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    group = tasktracker.get_group_by_id(user_name, group_id)
    tasks = tasktracker.get_group_tasks(user_name, group_id)
    users = tasktracker.get_group_users(user_name, group_id)

    template = loader.get_template('tasktracker/group.html')
    context = {
        'tasks': tasks,
        'group': group,
        'users': users,
        'user_name': user_name,
    }
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def edit_group(request, group_id):
    group_id = int(group_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    group = tasktracker.get_group_by_id(user_name, group_id)
    group = convert_group_to_model(group)

    if request.method == 'POST':
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')

            tasktracker.edit_group(
                user_name,
                group_id,
                new_group_name=name,
                new_group_description=description
            )

            return HttpResponseRedirect('/groups/')
    else:
        form = GroupForm(instance=group)

    return render(request, 'tasktracker/edit_group.html', {'form': form,
                                                           'user_name': user_name
                                                           })


@login_required
@exceptions_handler
def delete_group(request, group_id):
    group_id = int(group_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    tasktracker.delete_group(user_name, group_id)
    return HttpResponseRedirect('/groups/')


@login_required
@exceptions_handler
def share_group(request, group_id):
    group_id = int(group_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    all_users = get_all_users()
    users = []
    current_user_id = tasktracker.get_user_id_by_user_name(user_name)

    for user in all_users:
        if user.id != current_user_id:
            users.append(user)

    if request.method == 'POST':
        user_id = int(request.POST.get('user'))
        access_type = int(request.POST.get('access_type'))
        tasktracker.change_other_user_access_type_to_group(user_name,
                                                           user_id,
                                                           group_id,
                                                           access_type)

        return HttpResponseRedirect('/groups/{}/show/'.format(group_id))

    return render(request, 'tasktracker/change_access_type.html', {'users': users,
                                                                   'user_types': list(UserType),
                                                                   'user_name': user_name
                                                                   })


@login_required
@exceptions_handler
def add_task_to_group(request, group_id):
    group_id = int(group_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    group_tasks = [task.id for task in tasktracker.get_group_tasks(user_name, group_id)]

    user_tasks = tasktracker.get_user_tasks(user_name)
    tasks = []
    for task in user_tasks:
        if task.id not in group_tasks:
            tasks.append(task)

    if request.method == 'POST':
        task_id = int(request.POST.get('task'))
        tasktracker.add_task_to_group(user_name,
                                      group_id,
                                      task_id)

        return HttpResponseRedirect('/groups/{}/show/'.format(group_id))

    return render(request, 'tasktracker/add_task_to_group.html', {'tasks': tasks,
                                                                  'user_name': user_name
                                                                  })


@login_required
@exceptions_handler
def delete_task_from_group(request, group_id, task_id):
    group_id = int(group_id)
    task_id = int(task_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    tasktracker.delete_task_from_group(user_name, group_id, task_id)
    return HttpResponseRedirect('/groups/{}/show/'.format(group_id))


@login_required
@exceptions_handler
def change_user_access_type_to_group(request, group_id, user_id):
    group_id = int(group_id)
    user_id = int(user_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)

    all_users = get_all_users()
    users = []
    current_user_id = tasktracker.get_user_id_by_user_name(user_name)

    another_user = tasktracker.get_user_by_id(user_id)
    current_access = tasktracker.get_user_access_type_to_group(another_user.name, group_id).name

    for user in all_users:
        if user.id != current_user_id:
            users.append(user)

    if request.method == 'POST':
        access_type = int(request.POST.get('access_type'))
        tasktracker.change_other_user_access_type_to_group(user_name,
                                                           user_id,
                                                           group_id,
                                                           access_type)

        return HttpResponseRedirect('/groups/{}/show/'.format(group_id))

    return render(request, 'tasktracker/change_access_type.html', {'current_access': current_access,
                                                                   'user_id': user_id,
                                                                   'users': users,
                                                                   'user_types': list(UserType),
                                                                   'user_name': user_name
                                                                   })


@login_required
@exceptions_handler
def add_plan(request):
    user_name = request.user.username

    if request.method == 'POST':
        form = PlanForm(request.POST)
        if form.is_valid():
            tasktracker = get_tasktracker(user_name)
            task_name = form.cleaned_data.get('task_name')
            task_creation_time = form.cleaned_data.get('task_creation_time')
            start_date = form.cleaned_data.get('start_date')
            repeat_time_delta = form.cleaned_data.get('repeat_time_delta')
            repeat_time_delta_months = form.cleaned_data.get('repeat_time_delta_months')
            end_date = form.cleaned_data.get('end_date')
            description = form.cleaned_data.get('description')
            priority = form.cleaned_data.get('priority')

            tasktracker.create_plan(
                user_name,
                task_name,
                task_creation_time,
                start_date,
                repeat_time_delta,
                repeat_time_delta_months=repeat_time_delta_months,
                end_date=end_date,
                description=description,
                priority=priority.value
            )

            return HttpResponseRedirect('/plans/')
    else:
        form = PlanForm()

    return render(request, 'tasktracker/add_plan.html', {'form': form,
                                                         'user_name': user_name
                                                         })


@login_required
@exceptions_handler
def show_plan(request, plan_id):
    plan_id = int(plan_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)

    plan = tasktracker.get_plan_by_id(plan_id)

    template = loader.get_template('tasktracker/plan.html')
    context = {
        'plan': plan,
        'user_name': user_name,
    }

    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def edit_plan(request, plan_id):
    plan_id = int(plan_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    plan = tasktracker.get_plan_by_id(plan_id)
    plan = convert_plan_to_model(plan)

    if request.method == 'POST':
        form = PlanForm(request.POST, instance=plan)
        if form.is_valid():
            task_name = form.cleaned_data.get('task_name')
            task_creation_time = form.cleaned_data.get('task_creation_time')
            start_date = form.cleaned_data.get('start_date')
            repeat_time_delta = form.cleaned_data.get('repeat_time_delta')
            repeat_time_delta_months = form.cleaned_data.get('repeat_time_delta_months')
            end_date = form.cleaned_data.get('end_date')
            description = form.cleaned_data.get('description')
            priority = form.cleaned_data.get('priority')

            tasktracker.edit_plan(
                user_name,
                plan_id,
                new_task_name=task_name,
                new_task_creation_time=task_creation_time,
                new_start_date=start_date,
                new_repeat_time_delta=repeat_time_delta,
                new_repeat_time_delta_months=repeat_time_delta_months,
                new_end_date=end_date,
                new_description=description,
                new_priority=priority.value
            )

            return HttpResponseRedirect('/plans/')
    else:
        form = PlanForm(instance=plan)

    return render(request, 'tasktracker/edit_plan.html', {'form': form,
                                                          'user_name': user_name
                                                          })


@login_required
@exceptions_handler
def delete_plan(request, plan_id):
    plan_id = int(plan_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    tasktracker.delete_plan(user_name, plan_id)
    return HttpResponseRedirect('/plans/')


@login_required
@exceptions_handler
def add_notification(request):
    user_name = request.user.username

    if request.method == 'POST':
        form = NotificationForm(request.POST)
        if form.is_valid():
            tasktracker = get_tasktracker(user_name)
            creation_time = form.cleaned_data.get('creation_time')
            message = form.cleaned_data.get('message')

            tasktracker.create_notification(
                user_name,
                creation_time,
                message
            )

            return HttpResponseRedirect('/notifications/')
    else:
        form = NotificationForm()

    return render(request, 'tasktracker/add_notification.html', {'form': form,
                                                                 'user_name': user_name
                                                                 })


@login_required
@exceptions_handler
def edit_notification(request, notification_id):
    notification_id = int(notification_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name)
    notification = tasktracker.get_notification_by_id(notification_id)
    notification = convert_notification_to_model(notification)

    if request.method == 'POST':
        form = NotificationForm(request.POST, instance=notification)
        if form.is_valid():
            creation_time = form.cleaned_data.get('creation_time')
            message = form.cleaned_data.get('message')

            tasktracker.edit_notification(
                user_name,
                notification_id,
                new_notification_creation_time=creation_time,
                new_notification_message=message
            )

            return HttpResponseRedirect('/notifications/')
    else:
        form = NotificationForm(instance=notification)

    return render(request, 'tasktracker/edit_notification.html', {'form': form,
                                                                  'user_name': user_name
                                                                  })


@login_required
@exceptions_handler
def delete_notification(request, notification_id):
    notification_id = int(notification_id)
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    tasktracker.delete_notification(user_name, notification_id)
    return HttpResponseRedirect('/notifications/')


@login_required
@exceptions_handler
def clear_messages(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    tasktracker.clear_user_messages(user_name)
    return HttpResponseRedirect('/messages/')


@login_required
@exceptions_handler
def profile(request):
    user_name = request.user.username
    tasktracker = get_tasktracker(user_name=user_name)
    user = tasktracker.get_user_by_name(user_name)
    template = loader.get_template('tasktracker/user_profile.html')
    context = {
        'user_name': user.name,
        'user_email': user.email
    }
    return HttpResponse(template.render(context))

