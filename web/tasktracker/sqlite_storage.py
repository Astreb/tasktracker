from django.db.models import Max

from . import models
from .library.models.group import Group
from .library.models.notification import Notification
from .library.models.plan import Plan
from .library.models.relation import RelatedObjects, Relation
from .library.models.task import Task
from .library.models.user import User
from .library.service.exceptions import (
    TaskNotFound,
    GroupNotFound,
    UserNotFound,
    PlanNotFound,
    NotificationNotFound,
    RelationNotFound
)


class SQLiteStorage:
    """
        Contains functions for working with SQLite storage
    """

    def __init__(self):
        pass

    def clear_storage(self):
        pass

    def get_new_task_id(self):
        """Get and return a new id for task"""
        current_max_task_id = models.Task.objects.all().aggregate(Max('id'))
        if current_max_task_id['id__max'] is not None:
            return current_max_task_id['id__max'] + 1
        else:
            return 1

    def get_new_group_id(self):
        """Get and return a new id for group"""
        current_max_group_id = models.Group.objects.all().aggregate(Max('id'))
        if current_max_group_id['id__max'] is not None:
            return current_max_group_id['id__max'] + 1
        else:
            return 1

    def get_new_user_id(self):
        """Get and return a new id for user"""
        current_max_user_id = models.LibUser.objects.all().aggregate(Max('id'))
        if current_max_user_id['id__max'] is not None:
            return current_max_user_id['id__max'] + 1
        else:
            return 1

    def get_new_plan_id(self):
        """Get and return a new id for plan"""
        current_max_plan_id = models.Plan.objects.all().aggregate(Max('id'))
        if current_max_plan_id['id__max'] is not None:
            return current_max_plan_id['id__max'] + 1
        else:
            return 1

    def get_new_notification_id(self):
        """Get and return a new id for notification"""
        current_max_notification_id = models.Notification.objects.all().aggregate(Max('id'))
        if current_max_notification_id['id__max'] is not None:
            return current_max_notification_id['id__max'] + 1
        else:
            return 1

    def get_task_by_id(self, task_id):
        """
            Returns required task by its id

            Args:
                task_id(int) - id of required task

            Returns:
                instance of required task(Task object)

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            task_model = models.Task.objects.get(id=task_id)
            return convert_task_from_model(task_model)
        except models.Task.DoesNotExist:
            raise TaskNotFound(task_id)

    def add_task(self, task):
        """
            Add new task to storage

            Args:
                task(Task object) - instance of the task to be added

        """
        task_model = convert_task_to_model(task)
        task_model.save()

    def delete_task(self, task_id):
        """
            Delete task from storage

            Args:
                task_id(int) - id of the task to be deleted

            Raises:
                TaskNotFound exception, if deleted task is not found

        """
        try:
            task_model = models.Task.objects.get(id=task_id)
            task_model.delete()
        except models.Task.DoesNotExist:
            raise TaskNotFound(task_id)

    def update_task(self, task_id, task):
        """
            Replaces old instance of task with a new instance

            Args:
                task_id(int) - updated task id
                task(Task object) - new instance of updated task

            Raises:
                TaskNotFound exception, if updatable task is not found

        """
        try:
            models.Task.objects.get(id=task_id)
            task_model = convert_task_to_model(task)
            task_model.save()
        except models.Task.DoesNotExist:
            raise TaskNotFound(task_id)

    def get_task_users(self, task_id):
        """
            Returns a dict of all users who have access to task and their access types to task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all users who have access to task and their access types to task

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            models.Task.objects.get(id=task_id)
            users = {}
            user_task_models = models.UserTaskRelation.objects.all()
            for user_task_model in user_task_models:
                if user_task_model.task.id == task_id:
                    user = convert_user_from_model(user_task_model.user)
                    users[user] = user_task_model.access_type

            return users

        except models.Task.DoesNotExist:
            raise TaskNotFound

    def get_all_tasks(self):
        """Returns a list of all tasks"""
        task_models = models.Task.objects.all()
        tasks = []
        for task_model in task_models:
            task = convert_task_from_model(task_model)
            tasks.append(task)

        return tasks

    def get_task_groups(self, task_id):
        """
            Returns a list of all groups to which task is included

            Args:
                task_id(int) - required task id

            Returns:
                 list of all groups to which task is included

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            models.Task.objects.get(id=task_id)
            group_models = models.Group.objects.filter(tasks__id=task_id)

            groups = []

            for group_model in group_models:
                group = convert_group_from_model(group_model)
                groups.append(group)

            return groups

        except models.Task.DoesNotExist:
            raise TaskNotFound(task_id)

    def get_task_subtasks(self, task_id):
        """
            Returns a dict of all subtasks and type of their relation with task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all subtasks and type of their relation with task

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            models.Task.objects.get(id=task_id)
            subtasks = {}
            subtask_models = models.Task.objects.filter(parent_task__id=task_id)
            for subtask_model in subtask_models:
                subtask = convert_task_from_model(subtask_model)
                subtasks[subtask] = subtask_model.parent_task_relation_type

            return subtasks

        except models.Task.DoesNotExist:
            raise TaskNotFound

    def get_group_by_id(self, group_id):
        """
            Returns required group by its id

            Args:
                group_id(int) - required group id

            Returns:
                instance of required group(Group object)

            Raises:
                GroupNotFound exception, if required group is not found

        """
        try:
            group_model = models.Group.objects.get(id=group_id)
            return convert_group_from_model(group_model)
        except models.Group.DoesNotExist:
            raise GroupNotFound(group_id)

    def add_group(self, group):
        """
            Add new group to storage

            Args:
                group(Group object) - instance of added group

        """
        group_model = convert_group_to_model(group)
        group_model.save()

    def delete_group(self, group_id):
        """
            Delete group from storage

            Args:
                group_id(int) - id of the group to be deleted

            Raises:
                GroupNotFound exception, if deleted group is not found

        """
        try:
            group_model = models.Group.objects.get(id=group_id)
            group_model.delete()
        except models.Group.DoesNotExist:
            raise GroupNotFound(group_id)

    def update_group(self, group_id, group):
        """
            Replaces old instance of group with a new instance

            Args:
                group_id(int) - updated group id
                group(Group object) - new instance of updated group

            Raises:
                GroupNotFound exception, if updatable group is not found

        """
        try:
            models.Group.objects.get(id=group_id)
            group_model = convert_group_to_model(group)
            group_model.save()
        except models.Group.DoesNotExist:
            raise GroupNotFound(group_id)

    def get_group_users(self, group_id):
        """
            Returns a dict of all users who have access to group and their access types to group

            Args:
                group_id(int) - required group id

            Returns:
                dict of all users who have access to group and their access types to group

            Raises:
                GroupNotFound exception, if required group is not found

        """
        try:
            models.Group.objects.get(id=group_id)
            users = {}
            user_group_models = models.UserGroupRelation.objects.all()
            for user_group_model in user_group_models:
                if user_group_model.group.id == group_id:
                    user = convert_user_from_model(user_group_model.user)
                    users[user] = user_group_model.access_type

            return users

        except models.Group.DoesNotExist:
            raise GroupNotFound

    def get_group_tasks(self, group_id):
        """
            Returns a list of all group tasks

            Args:
                group_id(int) - required group id

            Returns:
                list of all group tasks

            Raises:
                GroupNotFound exception, if required group is not found

        """
        try:
            group = models.Group.objects.get(id=group_id)
            task_models = group.tasks.all()

            tasks = []

            for task_model in task_models:
                task = convert_task_from_model(task_model)
                tasks.append(task)

            return tasks

        except models.Group.DoesNotExist:
            raise GroupNotFound(group_id)

    def check_user_existence(self, user_name):
        """
            Checks if there is already a user with that name

            Args:
                user_name(str) - name of required user

            Returns:
                 True, if a user with this name is found.
                 else False

        """
        try:
            models.LibUser.objects.get(name=user_name)
            return False
        except models.LibUser.DoesNotExist:
            return True

    def get_user_by_id(self, user_id):
        """
            Returns required user by its id

            Args:
                user_id(int) - id of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            user_model = models.LibUser.objects.get(id=user_id)
            return convert_user_from_model(user_model)
        except models.LibUser.DoesNotExist:
            raise UserNotFound(user_id)

    def get_user_by_name(self, user_name):
        """
            Returns required user by its name

            Args:
                user_name(str) - name of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            user_model = models.LibUser.objects.get(name=user_name)
            return convert_user_from_model(user_model)
        except models.LibUser.DoesNotExist:
            raise UserNotFound(user_name)

    def get_user_id_by_user_name(self, user_name):
        """
            Returns user id by its name

            Args:
                user_name(str) - name of required user

            Returns:
                id of required user(int)

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            user_model = models.LibUser.objects.get(name=user_name)
            return user_model.id
        except models.LibUser.DoesNotExist:
            raise UserNotFound(user_name)

    def add_user(self, user):
        """
            Add new user to storage

            Args:
                user(User object) - instance of added user

        """
        user_model = convert_user_to_model(user)
        user_model.save()

    def update_user(self, user_id, user):
        """
            Replaces old instance of user with a new instance

            Args:
                user_id(int) - updated user id
                user(User object) - new instance of updated user

            Raises:
                UserNotFound exception, if updatable user is not found

        """
        try:
            models.LibUser.objects.get(id=user_id)
            user_model = convert_user_to_model(user)
            user_model.save()
        except models.LibUser.DoesNotExist:
            raise UserNotFound(user_id)

    def get_user_tasks(self, user_id):
        """
            Returns a dict of all tasks to which the user has access and type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all tasks to which the user has access and type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            models.LibUser.objects.get(id=user_id)
            tasks = {}
            user_task_models = models.UserTaskRelation.objects.all()
            for user_task_model in user_task_models:
                if user_task_model.user.id == user_id:
                    task = convert_task_from_model(user_task_model.task)
                    tasks[task] = user_task_model.access_type

            return tasks

        except models.LibUser.DoesNotExist:
            raise UserNotFound

    def get_user_groups(self, user_id):
        """
            Returns a dict of all groups to which the user has access and type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all groups to which the user has access and type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            models.LibUser.objects.get(id=user_id)
            groups = {}
            user_group_models = models.UserGroupRelation.objects.all()
            for user_group_model in user_group_models:
                if user_group_model.user.id == user_id:
                    group = convert_group_from_model(user_group_model.group)
                    groups[group] = user_group_model.access_type

            return groups

        except models.LibUser.DoesNotExist:
            raise UserNotFound

    def get_user_plans(self, user_id):
        """
            Returns a list of all user plans

            Args:
                user_id(int) - required user id

            Returns:
                list of all user plans

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            plans = []
            plan_models = models.Plan.objects.all()
            for plan_model in plan_models:
                if plan_model.user.id == user_id:
                    plan = convert_plan_from_model(plan_model)
                    plans.append(plan)

            return plans

        except models.LibUser.DoesNotExist:
            raise UserNotFound

    def get_user_notifications(self, user_id):
        """
            Returns a list of all user notifications

            Args:
                user_id(int) - required user id

            Returns:
                list of all user notifications

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            notifications = []
            notification_models = models.Notification.objects.all()
            for notification_model in notification_models:
                if notification_model.user.id == user_id:
                    notification = convert_notification_from_model(notification_model)
                    notifications.append(notification)

            return notifications

        except models.LibUser.DoesNotExist:
            raise UserNotFound

    def get_user_messages(self, user_id):
        """
            Returns a list of all user messages

            Args:
                user_id(int) - required user id

            Returns:
                list of all user messages

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            messages = []
            message_models = models.Message.objects.all()
            for message_model in message_models:
                if message_model.user.id == user_id:
                    message = convert_message_from_model(message_model)
                    messages.append(message)

            return messages

        except models.LibUser.DoesNotExist:
            raise UserNotFound

    def get_user_access_type_to_task(self, user_name, task_id):
        """
            Returns type of user access to task

            Args:
                user_name(str) - user name
                task_id(int) - task id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        user_task_relations = models.UserTaskRelation.objects.all()
        for user_task_relation in user_task_relations:
            if (user_task_relation.user.name == user_name and
                    user_task_relation.task.id == task_id):
                return user_task_relation.access_type

        return None

    def get_user_access_type_to_group(self, user_name, group_id):
        """
            Returns type of user access to group

            Args:
                user_name(str) - user name
                group_id(int) - group id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        user_group_relations = models.UserGroupRelation.objects.all()
        for user_group_relation in user_group_relations:
            if (user_group_relation.user.name == user_name and
                    user_group_relation.group.id == group_id):
                return user_group_relation.access_type

        return None

    def get_plan_by_id(self, plan_id):
        """
            Returns required plan by its id

            Args:
                plan_id(int) - required plan id

            Returns:
                instance of required plan(Plan object)

            Raises:
                PlanNotFound exception, if required plan is not found

        """
        try:
            plan_model = models.Plan.objects.get(id=plan_id)
            return convert_plan_from_model(plan_model)
        except models.Plan.DoesNotExist:
            raise PlanNotFound(plan_id)

    def get_all_plans(self):
        """Returns a list of all plans"""
        plan_models = models.Plan.objects.all()
        plans = []
        for plan_model in plan_models:
            plan = convert_plan_from_model(plan_model)
            plans.append(plan)

        return plans

    def add_plan(self, plan):
        """
            Add new plan to storage

            Args:
                plan(Plan object) - instance of added plan

        """
        plan_model = convert_plan_to_model(plan)
        plan_model.save()

    def delete_plan(self, plan_id):
        """
            Delete plan from storage

            Args:
                plan_id(int) - id of the plan to be deleted

            Raises:
                PlanNotFound exception, if deleted plan is not found

        """
        try:
            plan_model = models.Plan.objects.get(id=plan_id)
            plan_model.delete()
        except models.Plan.DoesNotExist:
            raise PlanNotFound(plan_id)

    def update_plan(self, plan_id, plan):
        """
            Replaces old instance of plan with a new instance

            Args:
                plan_id(int) - updated plan id
                plan(Plan object) - new instance of updated plan

            Raises:
                PlanNotFound exception, if updatable plan is not found

        """
        try:
            models.Plan.objects.get(id=plan_id)
            plan_model = convert_plan_to_model(plan)
            plan_model.save()
        except models.Plan.DoesNotExist:
            raise PlanNotFound(plan_id)

    def get_notification_by_id(self, notification_id):
        """
            Returns required notification by its id

            Args:
                notification_id(int) - required notification id

            Returns:
                instance of required notification(Notification object)

            Raises:
                NotificationNotFound exception, if required notification is not found

        """
        try:
            notification_model = models.Notification.objects.get(id=notification_id)
            return convert_notification_from_model(notification_model)
        except models.Notification.DoesNotExist:
            raise NotificationNotFound(notification_id)

    def add_notification(self, notification):
        """
            Add new notification to storage

            Args:
                notification(Notification object) - instance of added notification

        """
        notification_model = convert_notification_to_model(notification)
        notification_model.save()

    def delete_notification(self, notification_id):
        """
            Delete notification from storage

            Args:
                notification_id(int) - id of the notification to be deleted

            Raises:
                NotificationNotFound exception, if deleted notification is not found

        """
        try:
            notification_model = models.Notification.objects.get(id=notification_id)
            notification_model.delete()
        except models.Notification.DoesNotExist:
            raise NotificationNotFound(notification_id)

    def update_notification(self, notification_id, notification):
        """
            Replaces old instance of notification with a new instance

            Args:
                notification_id(int) - updated notification id
                notification(Notification object) - new instance of updated plan

            Raises:
                NotificationNotFound exception, if updatable notification is not found

        """
        try:
            models.Notification.objects.get(id=notification_id)
            notification_model = convert_notification_to_model(notification)
            notification_model.save()
        except models.Notification.DoesNotExist:
            raise NotificationNotFound(notification_id)

    def add_message(self, user_id, message):
        """
            Add new message to storage

            Args:
                user_id(int) - id of user to which message is addressed
                message(str) - message that create notification

        """
        message_model = convert_message_to_model(user_id, message)
        message_model.save()

    def clear_user_messages(self, user_id):
        """
            Delete all user messages

            Args:
                user_id(int) - id of user

        """
        message_models = models.Message.objects.filter(user_id=user_id)
        for message_model in message_models:
            message_model.delete()

    def add_relation(self, relation):
        """
            Add new relation to storage

            Args:
                relation(Relation object) - instance of added relation

        """
        if (relation.related_objects.name == RelatedObjects.USER_GROUP.name or
                relation.related_objects.name == RelatedObjects.USER_TASK.name):
            relation_model = convert_relation_to_model(relation)
            relation_model.save()

        if relation.related_objects.name == RelatedObjects.GROUP_TASK.name:
            group_model = models.Group.objects.get(id=relation.first_elem_id)
            task_model = models.Task.objects.get(id=relation.second_elem_id)
            group_model.tasks.add(task_model)
            group_model.save()

    def delete_relation(self, first_elem_id, second_elem_id,
                        related_objects):
        """
            Delete relation from storage

            Args:
                first_elem_id - first element id in deleted relation
                second_elem_id - second element id in deleted relation
                related_objects - related objects in deleted relation
                relation_type - relation type in deleted relation

            Raises:
                RelationNotFound exception, if deleted relation is not found

        """
        try:
            if related_objects.name == RelatedObjects.USER_GROUP.name:
                user = models.LibUser.objects.get(id=first_elem_id)
                group = models.Group.objects.get(id=second_elem_id)
                relation_model = models.UserGroupRelation.objects.get(user=user,
                                                                      group=group)
                relation_model.delete()

            if related_objects.name == RelatedObjects.USER_TASK.name:
                user = models.LibUser.objects.get(id=first_elem_id)
                task = models.Task.objects.get(id=second_elem_id)
                relation_model = models.UserTaskRelation.objects.get(user=user,
                                                                     task=task)
                relation_model.delete()

            if related_objects.name == RelatedObjects.GROUP_TASK.name:
                group_model = models.Group.objects.get(id=first_elem_id)
                task_model = models.Task.objects.get(id=second_elem_id)
                group_model.tasks.remove(task_model)
                group_model.save()

        except Exception:
            raise RelationNotFound()

    def update_relation(self, relation):
        """
            Replaces old instance of relation with a new instance

            Args:
                relation(Relation object) - new instance of updated relation

            Raises:
                RelationNotFound exception, if updatable relation is not found

        """
        try:
            if relation.related_objects.name == RelatedObjects.USER_GROUP.name:
                user = models.LibUser.objects.get(id=relation.first_elem_id)
                group = models.Group.objects.get(id=relation.second_elem_id)
                relation_model = models.UserGroupRelation.objects.get(user=user,
                                                                      group=group)
                relation_model.delete()
                relation_model = convert_relation_to_model(relation)
                relation_model.save()

            if relation.related_objects.name == RelatedObjects.USER_TASK.name:
                user = models.LibUser.objects.get(id=relation.first_elem_id)
                task = models.Task.objects.get(id=relation.second_elem_id)
                relation_model = models.UserTaskRelation.objects.get(user=user,
                                                                     task=task)
                relation_model.delete()
                relation_model = convert_relation_to_model(relation)
                relation_model.save()

        except Exception:
            self.add_relation(relation)

    def get_all_relations(self):
        pass


def convert_task_to_model(task):
    if task.parent_task_id is not None:
        parent_task = models.Task.objects.get(id=task.parent_task_id)
    else:
        parent_task = None
    task_model = models.Task(id=task.id,
                             name=task.name,
                             description=task.description,
                             priority=task.priority,
                             status=task.status,
                             in_archive=task.in_archive,
                             parent_task=parent_task,
                             parent_task_relation_type=task.parent_task_relation_type,
                             start_date=task.start_date,
                             end_date=task.end_date)
    return task_model


def convert_task_from_model(task_model):
    if task_model.parent_task is not None:
        parent_task_id = task_model.parent_task.id
    else:
        parent_task_id = None
    task = Task(task_model.id,
                task_model.name,
                task_model.start_date,
                end_date=task_model.end_date,
                description=task_model.description,
                priority=task_model.priority.value,
                status=task_model.status,
                parent_task_id=parent_task_id,
                parent_task_relation_type=task_model.parent_task_relation_type,
                in_archive=task_model.in_archive)
    return task


def convert_group_to_model(group):
    print('kus', group.id)
    group_model = models.Group(id=group.id,
                               name=group.name,
                               description=group.description)
    return group_model


def convert_group_from_model(group_model):
    group = Group(group_model.id,
                  group_model.name,
                  group_description=group_model.description)
    return group


def convert_user_to_model(user):
    user_model = models.LibUser(id=user.id,
                                name=user.name,
                                email=user.email)
    return user_model


def convert_user_from_model(user_model):
    user = User(user_model.id,
                user_model.name,
                user_email=user_model.email)
    return user


def convert_plan_to_model(plan):
    user = models.LibUser.objects.get(id=plan.user_id)
    plan_model = models.Plan(id=plan.id,
                             user=user,
                             task_name=plan.task_name,
                             description=plan.description,
                             priority=plan.priority,
                             task_creation_time=plan.task_creation_time,
                             start_date=plan.start_date,
                             end_date=plan.end_date,
                             repeat_time_delta=plan.repeat_time_delta,
                             repeat_time_delta_months=plan.repeat_time_delta_months)
    return plan_model


def convert_plan_from_model(plan_model):
    plan = Plan(plan_model.user.id,
                plan_model.id,
                plan_model.task_name,
                plan_model.task_creation_time,
                plan_model.start_date,
                plan_model.repeat_time_delta,
                repeat_time_delta_months=plan_model.repeat_time_delta_months,
                end_date=plan_model.end_date,
                description=plan_model.description,
                priority=plan_model.priority)
    return plan


def convert_notification_to_model(notification):
    user = models.LibUser.objects.get(id=notification.user_id)
    notification_model = models.Notification(id=notification.id,
                                             user=user,
                                             creation_time=notification.creation_time,
                                             message=notification.message)
    return notification_model


def convert_notification_from_model(notification_model):
    notification = Notification(notification_model.user.id,
                                notification_model.id,
                                notification_model.creation_time,
                                notification_model.message)
    return notification


def convert_message_to_model(user_id, message):
    user = models.LibUser.objects.get(id=user_id)
    message_model = models.Message(user=user,
                                   message=message)
    return message_model


def convert_message_from_model(message_model):
    return message_model.message


def convert_relation_to_model(relation):
    if relation.related_objects.name == RelatedObjects.USER_GROUP.name:
        user = models.LibUser.objects.get(id=relation.first_elem_id)
        group = models.Group.objects.get(id=relation.second_elem_id)
        relation_model = models.UserGroupRelation(user=user,
                                                  group=group,
                                                  access_type=relation.relation_type)
        return relation_model
    if relation.related_objects.name == RelatedObjects.USER_TASK.name:
        user = models.LibUser.objects.get(id=relation.first_elem_id)
        task = models.Task.objects.get(id=relation.second_elem_id)
        relation_model = models.UserTaskRelation(user=user,
                                                 task=task,
                                                 access_type=relation.relation_type)
        return relation_model


def convert_relation_from_model(relation_model):
    if isinstance(relation_model, models.UserGroupRelation):
        relation = Relation(relation_model.user.id,
                            relation_model.group.id,
                            RelatedObjects.USER_GROUP,
                            relation_type=relation_model.access_type)
        return relation
    if isinstance(relation_model, models.UserTaskRelation):
        relation = Relation(relation_model.user.id,
                            relation_model.task.id,
                            RelatedObjects.USER_TASK,
                            relation_type=relation_model.access_type)
        return relation


def get_all_users():
    """Returns a list of all users"""
    user_models = models.LibUser.objects.all()
    users = []
    for user_model in user_models:
        user = convert_user_from_model(user_model)
        users.append(user)

    return users
