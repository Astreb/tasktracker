from .library.tasktracker import TaskTracker
from .sqlite_storage import SQLiteStorage


def get_tasktracker(user_name):
    storage = SQLiteStorage()
    tasktracker = TaskTracker(storage)
    tasktracker.update_tasks()
    tasktracker.update_plans(user_name)
    tasktracker.update_notifications(user_name)
    return tasktracker
