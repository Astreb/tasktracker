import datetime

from django.contrib.auth.models import User
from django.db import models
from enumfields import EnumField

from library.models.task import TaskRelation, TaskPriority, TaskStatus
from library.models.user import UserType


class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=200)
    priority = EnumField(TaskPriority, default=TaskPriority.NORMAL)
    status = EnumField(TaskStatus, default=TaskStatus.CREATED)
    in_archive = models.BooleanField(default=False)

    parent_task = models.ForeignKey('self', blank=True, null=True)
    # WARNING
    parent_task_relation_type = EnumField(TaskRelation, default=None, blank=True, null=True)

    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)


class Group(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=True, null=True)

    tasks = models.ManyToManyField(Task)


class LibUser(models.Model):
    user_auth = models.ForeignKey(User, blank=True, null=True)

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=40)
    email = models.EmailField(max_length=40, blank=True, null=True)

    tasks = models.ManyToManyField(Task, through='UserTaskRelation')
    groups = models.ManyToManyField(Group, through='UserGroupRelation')


class Plan(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(LibUser)

    task_name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    # WARNING
    priority = EnumField(TaskPriority, default=TaskPriority.NORMAL)

    task_creation_time = models.DateTimeField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)
    repeat_time_delta = models.DurationField(default=datetime.timedelta())
    repeat_time_delta_months = models.IntegerField(default=0)


class Notification(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(LibUser)
    creation_time = models.DateTimeField()
    message = models.CharField(max_length=200)


class Message(models.Model):
    user = models.ForeignKey(LibUser)
    message = models.CharField(max_length=200)


class UserTaskRelation(models.Model):
    user = models.ForeignKey(LibUser)
    task = models.ForeignKey(Task)
    access_type = EnumField(UserType)


class UserGroupRelation(models.Model):
    user = models.ForeignKey(LibUser)
    group = models.ForeignKey(Group)
    access_type = EnumField(UserType)
