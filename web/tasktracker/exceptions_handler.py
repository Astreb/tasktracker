from .library.service import exceptions
from django.http import HttpResponse, Http404
from django.template import loader


def exceptions_handler(func):
    def execute(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exp:
            template = loader.get_template('tasktracker/error.html')
            context = {'exception': exp}
            return HttpResponse(template.render(context))

    return execute