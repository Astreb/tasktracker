"""
    Provides a class and enumerations that describe the task

    Classes:
        Task - describes task
        TaskStatus(Enum) - an enumeration describing the statuses of tasks
        TaskRelation(Enum) - an enumeration describing the types of links between the task and the subtask
        TaskPriority(Enum) - an enumeration describing the priorities of tasks
"""

from enum import Enum


class TaskPriority(Enum):
    """Task priority value enumeration"""
    LOW = 0
    NORMAL = 1
    HIGH = 2
    CRITICAL = 3


class TaskStatus(Enum):
    """Task status values enumeration"""
    CREATED = 0
    STARTED = 1
    WORK = 2
    DONE = 3
    FAILED = 4


class TaskRelation(Enum):
    """Task relation type values enumeration"""
    DEPENDS_ON = 0
    BLOCKING = 1
    CONNECTED = 2


class Task:
    """
        Describes the task

        Attributes:
            id - task id in system
            name - task name(may not be unique)
            description - task description
            priority - task priority
            status - task execution status
            in_archive - flag indicating whether the task is in the archive
            parent_task_id - id of parent task
            parent_task_relation_type - relation type with the parent task
            start_date - date from which you can begin to execute the task
            end_date - date to which you can execute the task

    """
    def __init__(self, task_id, task_name,
                 start_date, end_date=None,
                 description=None, priority=1, status=TaskStatus.CREATED,
                 parent_task_id=None, parent_task_relation_type=None, in_archive=False):
        """
        Args:
            task_id(int) - task id in system
            task_name(str) - task name(is not empty string)
            start_date(datetime) - date from which you can begin to execute the task
            end_date(datetime) - date to which you can execute the task
            description(str) - task description(default None)
            priority(int, TaskPriority enum) - task priority(default TaskPriority.NORMAL)

        """
        self.id = task_id
        self.name = task_name
        self.description = description
        self.priority = TaskPriority(priority)
        self.status = status
        self.in_archive = in_archive

        self.parent_task_id = parent_task_id
        self.parent_task_relation_type = parent_task_relation_type

        self.start_date = start_date
        self.end_date = end_date
