"""
    Provides class that creates tasks according to the plan

    Classes:
        Plan - describes the plan for creating tasks

"""

from library.models.task import TaskPriority


class Plan:
    """
        Describes the plan for creating tasks

        Attributes:
            id - plan id in system
            user_id - id of the author of plan
            task_name - name of task created by plan(may not be unique)
            description - description of task created by plan
            priority - priority of task created by plan
            task_creation_time - time in which plan should create a task
            start_date - date from which you can begin to execute task created by plan
            end_date - date to which you can execute task created by plan
            repeat_time_delta - number of weeks, days, hours and minutes through which plan will create a periodic task
            repeat_time_delta_months - number of months through which plan will create a periodic task
    """
    def __init__(self, user_id, plan_id, task_name,
                 task_creation_time, start_date, repeat_time_delta,
                 repeat_time_delta_months=0, end_date=None,
                 description=None, priority=1):
        """
        Args:
            user_id(int) - id of plan author
            plan_id(int) - plan id in system
            task_name(int) - plan task name
            task_creation_time(datetime) - time in which the plan should create a task
            start_date(datetime) - date from which you can begin to execute the task created by the plan
            repeat_time_delta(timedelta) - time through which the plan creates a periodic task
            repeat_time_delta_months(int) - number of months through which the plan creates a periodic task
            end_date(datetime) - date to which you can execute the task created by the plan(default None)
            description(str) - plan task description(default None)
            priority(int, Enum) - plan task priority(default TaskPriority.NORMAL)

        """
        self.id = plan_id
        self.user_id = user_id

        self.task_name = task_name
        self.description = description
        self.priority = TaskPriority(priority)

        self.task_creation_time = task_creation_time
        self.start_date = start_date
        self.end_date = end_date
        self.repeat_time_delta = repeat_time_delta
        self.repeat_time_delta_months = repeat_time_delta_months
