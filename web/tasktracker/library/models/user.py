"""
    Provides a class and an enumeration that describe the user

    Classes:
        User - describes the user
        UserType - An enumeration that describes the types of user access
"""

from enum import Enum


class User:
    """
        Describes the user

        Attributes:
            id - user id in system
            name - user name(must be unique)
            email - user email

    """
    def __init__(self, user_id, user_name, user_email=None):
        """
            Args:
                user_id - user id in system
                user_name - user name(must be unique)
                user_email - user email(default None)

        """
        self.id = user_id
        self.name = user_name
        self.email = user_email


class UserType(Enum):
    """User access type value enumeration"""
    AUTHOR = 0
    MODERATOR = 1
    PERFORMER = 2
    WATCHER = 3
