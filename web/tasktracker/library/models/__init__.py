"""
    TaskTracker models

    Modules:
        task - provides class and enumerations that describe task
        group - provides class that describes group of tasks
        plan - provides class that creates tasks according to plan
        user - provides class and enumeration that describe user
        notification - provides class that describe notification

"""