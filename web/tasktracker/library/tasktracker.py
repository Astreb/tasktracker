"""
    Provides class that contains all functions for working with library

    Classes:
        TaskTracker - contains all functions for working with library

    Functions:
        add_months_to_date - adds a specified number of months to the specified date
+
"""
import calendar
import datetime

from library.models.group import Group
from library.models.notification import Notification
from library.models.plan import Plan
from library.models.relation import Relation, RelatedObjects
from library.models.task import (
    Task,
    TaskStatus,
    TaskPriority,
    TaskRelation
)
from library.models.user import (
    UserType,
    User
)
from library.service.exceptions import (
    IncorrectDateTime,
    AccessDenied,
    IncorrectAction
)
from library.service.lib_logger import (
    get_lib_logger,
    write_exceptions_log
)


class TaskTracker:
    """
        Contains all functions for working with library

        Attributes:
            storage - storage interface object for working with storage

    """

    def __init__(self, storage):
        """
            Args:
                storage - TaskTracker storage instance

        """
        self.storage = storage

    def clear_storage(self):
        """Delete all storage data"""
        self.storage.clear_storage()

    @write_exceptions_log
    def create_task(self, user_name, task_name,
                    start_date, end_date=None,
                    description=None, priority=1):
        """
            Create task with given parameters

            Args:
                *user_name(str) - name of user who wants to create a task
                *task_name(str) - created task name
                *start_date(datetime) - date from which you can begin to
                *execute created task
                *end_date(datetime) - date to which you can execute
                created task(default None)
                *description(str) - created task description(default None)
                *priority(int, TaskPriority enum) - created task
                priority(default NORMAL )

            Returns:
                instance of created task(Task object)

            Raises:
                ValueError exception, if new_task_name is empty string
                ValueError exception, if new_priority value is not correct
                IncorrectDateTime exception, if start date more end date

        """
        logger = get_lib_logger()

        task_id = self.storage.get_new_task_id()
        user_id = self.storage.get_user_id_by_user_name(user_name)

        if task_name == '':
            raise ValueError('Task name can not be empty.')

        if end_date is not None:
            if start_date > end_date:
                raise IncorrectDateTime('Start date more End date')

        if priority not in range(4):
            raise ValueError('Incorrect priority value')

        task = Task(task_id,
                    task_name,
                    start_date,
                    end_date=end_date,
                    description=description,
                    priority=priority)

        relation = self.create_relation(user_id,
                                        task_id,
                                        RelatedObjects.USER_TASK,
                                        relation_type=UserType.AUTHOR)

        self.storage.add_task(task)
        self.storage.add_relation(relation)

        logger.info('User {} create task {}'.format(user_name, task_id))

        return task

    @write_exceptions_log
    def edit_task(self, user_name, task_id, new_task_name=None,
                  new_start_date=None, new_end_date=None,
                  new_description=None, new_priority=None):
        """
            Edit selected task parameters

            Args:
                *user_name(str) - name of user who is editing task
                *task_id(int) - id of edited task
                *new_task_name(str) - edited task name
                *new_start_date(datetime) - edited start date
                *new_end_date(datetime) - edited end date
                *new_description(str) - edited task description
                *new_priority(int, TaskPriority enum) - edited task priority

            Returns:
                 edited task instance(Task object)

            Raises:
                AccessDenied exception, if user can not edit this task
                ValueError exception, if new_task_name is empty string
                IncorrectDateTime exception, if start date more end date
                ValueError exception, if new_priority value is not correct

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not edit task {}'.format(user_name, task_id))

        task = self.storage.get_task_by_id(task_id)

        if new_task_name is not None:
            if new_task_name == '':
                raise ValueError('Task name can not be empty.')
            task.name = new_task_name

        if new_start_date is not None:
            task.start_date = new_start_date

        if new_end_date is not None:
            task.end_date = new_end_date

        if task.end_date is not None:
            if task.start_date > task.end_date:
                raise IncorrectDateTime('Start date more End date')

        if new_description is not None:
            task.description = new_description

        if new_priority is not None:
            if new_priority not in range(4):
                raise ValueError('Incorrect priority value')
            task.priority = TaskPriority(new_priority)

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(task_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} edit task {}'.format(user_name, task_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.update_task(task_id, task)

        return task

    @write_exceptions_log
    def delete_task(self, user_name, task_id):
        """
            Delete selected task

            Args:
                *user_name(str) - user name that delete task
                *task_id(int) - deleted task id

            Raises:
                AccessDenied exception, if user can not delete this task

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR:
            raise AccessDenied('User {} can not delete task {}'.format(user_name, task_id))

        subtasks = self.storage.get_task_subtasks(task_id)
        for subtask in subtasks:
            self.storage.delete_relation(task_id,
                                         subtask.id,
                                         RelatedObjects.TASK_SUBTASK)

        groups = self.storage.get_task_groups(task_id)
        for group in groups:
            self.storage.delete_relation(group.id,
                                         task_id,
                                         RelatedObjects.GROUP_TASK)

        users = self.storage.get_task_users(task_id)
        for user in users.keys():
            self.storage.delete_relation(user.id,
                                         task_id,
                                         RelatedObjects.USER_TASK)

        # Create notifications for all task-related users.
        action_author = self.get_user_by_name(user_name)
        message = 'User {} delete task {}.'.format(user_name, task_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.delete_task(task_id)

    @write_exceptions_log
    def update_task_status(self, task):
        """
            Make task DONE if there are no more blocking subtasks

            Args:
                task(Task object) - instance of updated task

        """
        is_blocked = False
        subtasks = self.storage.get_task_subtasks(task.id)
        for subtask, relation_type in subtasks.items():
            if relation_type == TaskRelation.BLOCKING and subtask.status != TaskStatus.DONE:
                is_blocked = True

        if not is_blocked:
            task.status = TaskStatus.DONE
            self.storage.update_task(task.id, task)

            # Create notifications for all task-related users.
            users = self.storage.get_task_users(task.id)
            message = 'Task {} was done.'.format(task.id)
            self.create_notifications(None,
                                      message,
                                      users.keys())

            get_lib_logger().info(message)

            self.update_task_tree(task.id)

    @write_exceptions_log
    def update_parent_task(self, task_id):
        """
            Updates status of parent tasks to STARTED, if they were executed,
            but blocking subtask is no longer executed

            Args:
                task_id - id of current parent task

        """
        while task_id is not None:
            task = self.storage.get_task_by_id(task_id)

            if task.status == TaskStatus.DONE:
                task.status = TaskStatus.STARTED

                # Create notifications for all task-related users.
                users = self.storage.get_task_users(task.id)
                self.create_notifications(None,
                                          'Task {} was started.'.format(task.id),
                                          users.keys())

                self.storage.update_task(task.id, task)
            else:
                break

            if task.parent_task_relation_type == TaskRelation.BLOCKING:
                task_id = task.parent_task_id
            else:
                break

    @write_exceptions_log
    def update_task_tree(self, task_id):
        """
            Update status of the DEPENDS_ON subtasks if task was DONE

            Args:
                task_id - id of updated task tree root

        """
        task = self.storage.get_task_by_id(task_id)

        subtasks = self.storage.get_task_subtasks(task_id)
        for subtask, relation_type in subtasks.items():
            if relation_type == TaskRelation.DEPENDS_ON:
                if task.status == TaskStatus.DONE:
                    self.update_task_status(subtask)

    @write_exceptions_log
    def update_tasks(self):
        """Checks all tasks and updates status if task execution time has expired"""
        logger = get_lib_logger()

        tasks = self.storage.get_all_tasks()

        for task in tasks:
            if task.start_date < datetime.datetime.now() and task.status == TaskStatus.CREATED:
                task.status = TaskStatus.STARTED

                # Create notifications for all task-related users.
                users = self.storage.get_task_users(task.id)
                message = 'Task {} was started.'.format(task.id)
                self.create_notifications(None,
                                          message,
                                          users.keys())

                logger.info(message)

            if task.end_date is not None:
                if datetime.datetime.now() > task.end_date:
                    if task.status != TaskStatus.FAILED and task.status != TaskStatus.DONE:
                        task.status = TaskStatus.FAILED

                        # Create notifications for all task-related users.
                        users = self.storage.get_task_users(task.id)
                        message = 'Task {} was failed.'.format(task.id)
                        self.create_notifications(None,
                                                  message,
                                                  users.keys())

                        logger.info(message)

        for task in tasks:
            self.storage.update_task(task.id, task)

    @write_exceptions_log
    def change_task_status(self, user_name, task_id, new_status):
        """
            Change task status to a given

            Args:
                *user_name(str) - user name that changes status of task
                *task_id(int) - id of changed task
                *new_status(int, TaskStatus enum) - new task status

            Raises:
                AccessDenied exception, if user can not change task status
                IncorrectAction exception, if task status CREATED change to STARTED
                IncorrectAction exception, if task status is CREATED
                AccessDenied exception, if task have blocking subtasks and
                new status is DONE

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None or access_type == UserType.WATCHER:
            raise AccessDenied('User {} can not change task {} status.'.format(user_name, task_id))

        task = self.storage.get_task_by_id(task_id)

        users = self.storage.get_task_users(task.id)
        action_author = self.get_user_by_name(user_name)

        new_status = TaskStatus(new_status)

        if new_status == TaskStatus.STARTED:
            if task.status != TaskStatus.CREATED:
                task.status = TaskStatus.STARTED

                # Create notifications for all task-related users.
                message = 'User {} start task {}.'.format(user_name, task.id)
                self.create_notifications(action_author,
                                          message,
                                          users.keys())

                logger.info(message)

                # Update status of parent if task for him is blocking.
                if task.parent_task_relation_type == TaskRelation.BLOCKING:
                    self.update_parent_task(task.parent_task_id)
                self.storage.update_task(task.id, task)

                return
            else:
                raise IncorrectAction('You can not change task status to STARTED if it is CREATED.')

        if task.status == TaskStatus.CREATED:
            raise IncorrectAction(('You can not change task status to {} if it is not started.'.
                                   format(new_status.name)))

        if new_status == TaskStatus.FAILED or new_status == TaskStatus.WORK:
            task.status = new_status

            # Update status of parent if task for him is blocking.
            if task.parent_task_relation_type == TaskRelation.BLOCKING:
                self.storage.update_task(task.id, task)
                self.update_parent_task(task.parent_task_id)

            # Create notifications for all task-related users.
            message = ('User {} set task {} status to {}.'.
                       format(user_name, task_id, new_status.name))
            self.create_notifications(action_author,
                                      message,
                                      users.keys())

            logger.info(message)
            self.storage.update_task(task.id, task)

        else:
            is_blocked = False
            subtasks = self.storage.get_task_subtasks(task.id)
            for subtask, relation_type in subtasks.items():
                if relation_type == TaskRelation.BLOCKING and subtask.status != TaskStatus.DONE:
                    is_blocked = True

            if is_blocked:
                raise AccessDenied(('Task {} can not be done while there are blocking tasks'.
                                    format(task_id)))
            else:
                task.status = new_status

                # Create notifications for all task-related users.
                message = ('User {} set task {} status to {}.'.
                           format(user_name, task_id, new_status.name))
                self.create_notifications(action_author,
                                          message,
                                          users.keys())

                logger.info(message)

                # Update task and status of DEPENDS_ON subtasks.
                self.storage.update_task(task.id, task)
                self.update_task_tree(task_id)

    @write_exceptions_log
    def move_task_tree(self, user_name, root_task_id,
                       new_root_task_id=None, task_relation_type=0):
        """
            Makes task tree a subtree of another task

            Args:
                *user_name(str) - name of user who moves task tree
                *root_task_id(int) - id of task tree root task
                *new_root_task_id(int, None) - id of new task tree root task
                *task_relation_type(int, TaskRelation enum) - relation type between
                old task tree root and new task tree root

            Raises:
                AccessDenied exception, if user cannot move task tree

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, root_task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not move task {}'.format(user_name, root_task_id))

        root_task = self.storage.get_task_by_id(root_task_id)
        if root_task.parent_task_id is not None:
            self.delete_subtask(user_name, root_task.parent_task_id, root_task_id)

        if new_root_task_id is None:
            root_task.parent_task_id = None
            root_task.parent_task_relation_type = None

            self.storage.update_task(root_task_id, root_task)
        else:
            self.add_subtask(user_name,
                             new_root_task_id,
                             root_task_id,
                             task_relation_type=task_relation_type)

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(root_task_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} move task tree with root in task {}'.format(user_name, root_task_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

    @write_exceptions_log
    def add_subtask(self, user_name, task_id, subtask_id, task_relation_type=0):
        """
            Add selected subtask to selected task with selected relation type

            Args:
                *user_name(str) - name of user that add subtask
                *task_id(int) - id of task that add subtask
                *subtask_id(int) - id of subtask
                *task_relation_type(int, TaskRelation enum) - relation type
                between task and subtask

            Raises:
                AccessDenied exception, if this user cannot add subtask to task
                AccessDenied exception, if this user cannot make task a subtask
                IncorrectAction exception, if subtask is parent task for task
                IncorrectAction exception, if this subtask already is subtask of this task

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not modify task {} subtasks list'.format(user_name, task_id))

        access_type = self.get_user_access_type_to_task(user_name, subtask_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not make the task {} a subtask'.format(user_name, subtask_id))

        # Check whether the cycle will not be created.
        is_subtask_parent_task = self.is_parent(subtask_id, task_id)
        if is_subtask_parent_task:
            raise IncorrectAction('The creation of cyclic links is unacceptable')

        subtask = self.storage.get_task_by_id(subtask_id)

        if subtask.parent_task_id == task_id:
            raise IncorrectAction('Subtask {} already added.'.format(subtask_id))

        subtask.parent_task_id = task_id
        subtask.parent_task_relation_type = TaskRelation(task_relation_type)

        relation = self.create_relation(task_id,
                                        subtask_id,
                                        RelatedObjects.TASK_SUBTASK,
                                        relation_type=TaskRelation(task_relation_type))
        self.storage.add_relation(relation)

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(task_id)
        action_author = self.get_user_by_name(user_name)
        message = ('User {} add subtask {} to task {} with relation type {}.'.
                   format(user_name, subtask_id, task_id, TaskRelation(task_relation_type).name))
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.update_task(subtask_id, subtask)

    @write_exceptions_log
    def delete_subtask(self, user_name, task_id, subtask_id):
        """
            Add selected subtask to selected task with selected relation type

            Args:
                *user_name(str) - name of user that delete subtask
                *task_id(int) - id of task from which delete subtask
                *subtask_id(int) - id of subtask

            Raises:
                AccessDenied exception, if this user cannot delete subtask
                from this task
                AccessDenied exception, if this user cannot change parent of
                this subtask
                IncorrectAction exception, if this subtask is not subtask of
                selected task

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not modify task {} subtasks list'.format(user_name, task_id))

        access_type = self.get_user_access_type_to_task(user_name, subtask_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not change the parent of task {}'.format(user_name, subtask_id))

        task = self.storage.get_task_by_id(task_id)
        subtask = self.storage.get_task_by_id(subtask_id)

        if subtask.parent_task_id != task_id:
            raise IncorrectAction('Task {} is not subtask of task {}'.format(subtask_id, task_id))

        subtask.parent_task_id = None
        subtask.parent_task_relation_type = None

        self.storage.delete_relation(task_id,
                                     subtask_id,
                                     RelatedObjects.TASK_SUBTASK)

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(task_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} delete subtask {} from task {}'.format(user_name, subtask_id, task_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.update_task(subtask_id, subtask)

    @write_exceptions_log
    def get_task_by_id(self, user_name, task_id):
        """
            Returns required task by its id

            Args:
                *user_name(str) - name of user that who wants to get task
                *task_id(int) - id of required task

            Returns:
                instance of required task(Task object)

            Raises:
                TaskNotFound exception, if required task is not found
                AccessDenied exception, if this user cannot get this task

        """
        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None:
            raise AccessDenied('User {} can not view task {}.'.format(user_name, task_id))

        return self.storage.get_task_by_id(task_id)

    @write_exceptions_log
    def get_task_subtasks(self, user_name, task_id):
        """
            Returns a dict of all subtasks and type of their relation with task

            Args:
                *user_name(str) - name of user that who wants to get task subtasks
                *task_id(int) - id of required task

            Returns:
                dict of all subtasks and type of their relation with task

            Raises:
                TaskNotFound exception, if required task is not found
                AccessDenied exception, if this user cannot get subtasks of this task

        """
        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None:
            raise AccessDenied('User {} can not view task {} subtasks.'.format(user_name, task_id))

        subtasks = self.storage.get_task_subtasks(task_id)
        return subtasks

    @write_exceptions_log
    def get_task_groups(self, user_name, task_id):
        """
            Returns a list of all groups to which task is included

            Args:
                *user_name(str) - name of user that who wants to get task groups
                *task_id(int) - id of required task

            Returns:
                list of all groups to which task is included

            Raises:
                TaskNotFound exception, if required task is not found
                AccessDenied exception, if this user cannot get groups of this task

        """
        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None:
            raise AccessDenied('User {} can not view task {} groups.'.format(user_name, task_id))

        groups = self.storage.get_task_groups(task_id)
        return groups

    @write_exceptions_log
    def get_task_users(self, user_name, task_id):
        """
            Returns a dict of all users who have access to
            task and their access types to task

            Args:
                *user_name(str) - name of user that who wants to get task users
                *task_id(int) - id of required task

            Returns:
                dict of all users who have access to task and their access types to task

            Raises:
                TaskNotFound exception, if required task is not found
                AccessDenied exception, if this user cannot get users
                of this task

        """
        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None:
            raise AccessDenied('User {} can not view task {} users.'.format(user_name, task_id))

        users = self.storage.get_task_users(task_id)
        return users

    @write_exceptions_log
    def add_task_to_archive(self, user_name, task_id):
        """
            Add selected task to archive

            Args:
                *user_name - name of user who wants to add task to archive
                *task_id - id of selected task

            Raises:
                AccessDenied exception, if this user cannot add selected task to archive

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type is None or access_type == UserType.WATCHER:
            raise AccessDenied('User {} can not send task {} to the archive.'.format(user_name, task_id))

        task = self.storage.get_task_by_id(task_id)
        task.in_archive = True

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(task_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} add task {} to archive.'.format(user_name, task_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.update_task(task_id, task)

    @write_exceptions_log
    def create_group(self, user_name, group_name, group_description=None):
        """
            Create group with given parameters

            Args:
                *user_name(str) - name of user who wants to create a group
                *group_name(str) - created group name
                *group_description(str) - created group description(default None)

            Returns:
                instance of created group(Group object)

            Raises:
                ValueError exception, if group name is empty string

        """
        logger = get_lib_logger()

        group_id = self.storage.get_new_group_id()
        user_id = self.storage.get_user_id_by_user_name(user_name)

        if group_name == '':
            raise ValueError('Group name must be not empty string')

        group = Group(group_id,
                      group_name,
                      group_description=group_description)

        relation = self.create_relation(user_id,
                                        group_id,
                                        RelatedObjects.USER_GROUP,
                                        relation_type=UserType.AUTHOR)

        self.storage.add_group(group)
        self.storage.add_relation(relation)

        logger.info('User {} create group {}.'.format(user_name, group_id))

        return group

    @write_exceptions_log
    def edit_group(self, user_name, group_id,
                   new_group_name=None, new_group_description=None):
        """
            Edit selected group parameters

            Args:
                *user_name(str) - name of user who wants to edit a group
                *group_id(int) - id of group which need to be edited
                *new_group_name(str) - edited group name(default None)
                *new_group_description(str) - edited group description(default None)

            Returns:
                instance of edited group(Group object)

            Raises:
                ValueError exception, if edited group name is empty string
                AccessDenied exception, if this user cannot edit this group

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not edit group {}'.format(user_name, group_id))

        group = self.storage.get_group_by_id(group_id)

        if new_group_name is not None:
            if new_group_name == '':
                raise ValueError('Group name must be not empty string.')
            group.name = new_group_name

        if new_group_description is not None:
            group.description = new_group_description

        # Create notifications for all group-related users.
        users = self.storage.get_group_users(group_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} edit group {}.'.format(user_name, group_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.update_group(group_id, group)

        return group

    @write_exceptions_log
    def delete_group(self, user_name, group_id):
        """
            Delete selected group

            Args:
                *user_name(str) - name of user who wants to delete group
                *group_id(int) - id of deleted group

            Raises:
                 AccessDenied exception, if this user cannot delete this group

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type != UserType.AUTHOR:
            raise AccessDenied('User {} can not delete task {}'.format(user_name, group_id))

        tasks = self.storage.get_group_tasks(group_id)
        for task in tasks:
            self.storage.delete_relation(group_id,
                                         task.id,
                                         RelatedObjects.GROUP_TASK)

        users = self.storage.get_group_users(group_id)
        for user in users.keys():
            self.storage.delete_relation(user.id,
                                         group_id,
                                         RelatedObjects.USER_GROUP)

        # Create notifications for all group-related users.
        users = self.storage.get_group_users(group_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} delete group {}.'.format(user_name, group_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

        self.storage.delete_group(group_id)

    @write_exceptions_log
    def add_task_to_group(self, user_name, group_id, task_id):
        """
            Add selected task to selected group

            Args:
                *user_name(str) - name of user who wants to add task to group
                *group_id(int) - id of group in which user want to add a task
                *task_id(int) - id of task want to add to group

            Raises:
                AccessDenied exception, if this user cannot add task to this group
                AccessDenied exception, if this user cannot add this task to group
                IncorrectAction exception, if this task already added to this group

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not add a task to group {}.'.format(user_name, group_id))

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not add a task {} to group.'.format(user_name, task_id))

        groups = self.storage.get_task_groups(task_id)
        groups_id = [group.id for group in groups]
        if group_id in groups_id:
            raise IncorrectAction('Task {} is already added to group {}.'.format(task_id, group_id))

        relation = self.create_relation(group_id,
                                        task_id,
                                        RelatedObjects.GROUP_TASK)
        self.storage.add_relation(relation)

        # Create notifications for all group-related users.
        users = self.storage.get_group_users(group_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} add task {} to group {}.'.format(user_name, task_id, group_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

    @write_exceptions_log
    def delete_task_from_group(self, user_name, group_id, task_id):
        """
            Delete selected task from selected group

            Args:
                *user_name(str) - name of user who wants to delete task
                from group
                *group_id(int) - id of group from which user want to
                delete a task
                *task_id(int) - id of task want to delete from group

            Raises:
                AccessDenied exception, if this user cannot delete task
                from this group
                AccessDenied exception, if this user cannot delete this
                task from group
                IncorrectAction exception, if this task is not added
                to this group

        """
        logger = get_lib_logger()

        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not delete a task from group {}.'.format(user_name, group_id))

        access_type = self.get_user_access_type_to_task(user_name, task_id)
        if access_type != UserType.AUTHOR and access_type != UserType.MODERATOR:
            raise AccessDenied('User {} can not delete a task {} from group.'.format(user_name, task_id))

        tasks = self.storage.get_group_tasks(group_id)
        tasks_id = [task.id for task in tasks]
        if task_id not in tasks_id:
            raise IncorrectAction('Task {} is not in group {}.'.format(task_id, group_id))

        self.storage.delete_relation(group_id,
                                     task_id,
                                     RelatedObjects.GROUP_TASK)

        # Create notifications for all group-related users.
        users = self.storage.get_group_users(group_id)
        action_author = self.get_user_by_name(user_name)
        message = 'User {} delete task {} from group {}.'.format(user_name, task_id, group_id)
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

    @write_exceptions_log
    def get_group_by_id(self, user_name, group_id):
        """
            Returns required group by its id

            Args:
                *user_name(str) - name of user that who wants to get group
                *group_id(int) - id of required group

            Returns:
                instance of required group(Group object)

            Raises:
                GroupNotFound exception, if required group is not found
                AccessDenied exception, if this user cannot get this group

        """
        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type is None:
            raise AccessDenied('User {} can not view group {}.'.format(user_name, group_id))

        return self.storage.get_group_by_id(group_id)

    @write_exceptions_log
    def get_group_tasks(self, user_name, group_id):
        """
            Returns a list of all group tasks

            Args:
                *user_name(str) - name of user that who wants to
                get group tasks
                *group_id(int) - id of required group

            Returns:
                list of all group tasks

            Raises:
                GroupNotFound exception, if required group is not found
                AccessDenied exception, if this user cannot get tasks
                of this group

        """
        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type is None:
            raise AccessDenied('User {} can not view group {} tasks.'.format(user_name, group_id))

        return self.storage.get_group_tasks(group_id)

    @write_exceptions_log
    def get_group_users(self, user_name, group_id):
        """
            Returns a dict of all users who have access to group and
            their access types to group

            Args:
                *user_name(str) - name of user that who wants to get group users
                *group_id(int) - id of required group

            Returns:
                dict of all users who have access to group and their access
                types to group

            Raises:
                GroupNotFound exception, if required group is not found
                AccessDenied exception, if this user cannot get users of
                this group

        """
        access_type = self.get_user_access_type_to_group(user_name, group_id)
        if access_type is None:
            raise AccessDenied('User {} can not view group {} users.'.format(user_name, group_id))

        return self.storage.get_group_users(group_id)

    @write_exceptions_log
    def check_user_existence(self, user_name):
        """
            Checks if there is already a user with that name

            Args:
                user_name - name of required user

            Returns:
                 True, if a user with this name is found.
                 else False

        """
        return self.storage.check_user_existence(user_name)

    @write_exceptions_log
    def create_user(self, user_name, user_email=None):
        """
            Create user with given parameters

            Args:
                user_name(str) - name of created user
                user_email(str) - email of created user

            Returns:
                 instance of created user(User object)

            Raises:
                ValueError exception, if created user name is empty string

        """
        logger = get_lib_logger()

        user_id = self.storage.get_new_user_id()

        if user_name == '':
            raise ValueError('User name must be not empty string.')
        if self.check_user_existence(user_name):
            raise ValueError('There is already a user with that name')

        user = User(user_id,
                    user_name,
                    user_email=user_email)

        logger.info('Created user {}'.format(user_name))

        self.storage.add_user(user)

        return user

    @write_exceptions_log
    def edit_user(self, user_id, new_user_name=None, new_user_email=None):
        """
            Create user with given parameters

            Args:
                user_id(int) - id of edited user
                new_user_name(str) - name of created user
                new_user_email(str) - email of created user

            Returns:
                 instance of created user(User object)

            Raises:
                ValueError exception, if created user name is empty string
                ValueError exception, if there is already a user with that name

        """
        logger = get_lib_logger()

        user = self.storage.get_user_by_id(user_id)

        if new_user_name is not None:
            if new_user_name == '':
                raise ValueError('User name must be not empty string.')
            if self.check_user_existence(new_user_name):
                raise ValueError('There is already a user with that name')
            user.name = new_user_name

        if new_user_email is not None:
            user.email = new_user_email

        logger.info('Edited user {}.'.format(user_id))

        self.storage.update_user(user_id, user)

        return user

    @write_exceptions_log
    def get_user_by_id(self, user_id):
        """
            Returns required user by its id

            Args:
                user_id(int) - id of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_by_id(user_id)

    @write_exceptions_log
    def get_user_by_name(self, user_name):
        """
            Returns required user by its name

            Args:
                user_name(str) - name of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found
        """
        return self.storage.get_user_by_name(user_name)

    @write_exceptions_log
    def get_user_id_by_user_name(self, user_name):
        """
            Returns user id by its name

            Args:
                user_name(str) - name of required user

            Returns:
                id of required user(int)

        """
        return self.storage.get_user_id_by_user_name(user_name)

    @write_exceptions_log
    def get_user_tasks(self, user_name):
        """
            Returns a dict of all tasks to which the user has access and
            type of user access to them

            Args:
                user_name(str) - name of required user

            Returns:
                dict of all tasks to which the user has access and type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        return self.storage.get_user_tasks(user_id)

    @write_exceptions_log
    def get_user_groups(self, user_name):
        """
            Returns a dict of all groups to which the user has access and
            type of user access to them

            Args:
                user_name(str) - name of required user

            Returns:
                dict of all groups to which the user has access and type
                of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        return self.storage.get_user_groups(user_id)

    @write_exceptions_log
    def get_user_plans(self, user_name):
        """
            Returns a list of all user plans

            Args:
                user_name(str) - name of required user

            Returns:
                list of all user plans

            Raises:
                UserNotFound exception, if required user is not found

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        return self.storage.get_user_plans(user_id)

    @write_exceptions_log
    def get_user_notifications(self, user_name):
        """
            Returns a list of all user notifications

            Args:
                user_name(str) - name of required user

            Returns:
                list of all user notifications

            Raises:
                UserNotFound exception, if required user is not found

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        return self.storage.get_user_notifications(user_id)

    @write_exceptions_log
    def get_user_messages(self, user_name):
        """
            Returns a list of all user messages

            Args:
                user_name(str) - name of required user

            Returns:
                list of all user messages

            Raises:
                UserNotFound exception, if required user is not found

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        return self.storage.get_user_messages(user_id)

    @write_exceptions_log
    def change_other_user_access_type_to_task(self, user_name, other_user_id, task_id, access_type):
        """
            Change access type of another user to task for selected access type

            Args:
                user_name - name of user that changes access type of another user
                other_user_id - id of user for which type of access to task is changed
                task_id - id of task access type to which is changed
                access_type - new user access type to task

            Raises:
                AccessDenied exception, if user cannot change access type of another user to this task

        """
        logger = get_lib_logger()

        user_access_type = self.get_user_access_type_to_task(user_name, task_id)
        if user_access_type != UserType.AUTHOR:
            raise AccessDenied(('User {} can not change the access type of other users to task {}.'.
                                format(user_name, task_id)))

        relation = self.create_relation(other_user_id,
                                        task_id,
                                        RelatedObjects.USER_TASK,
                                        relation_type=UserType(access_type))
        self.storage.update_relation(relation)

        other_user = self.get_user_by_id(other_user_id)

        # Create notifications for all task-related users.
        users = self.storage.get_task_users(task_id)
        action_author = self.get_user_by_name(user_name)
        message = ('User {} set access type of user {} to task {} on {}.'.
                   format(user_name, other_user.name, task_id, UserType(access_type).name))
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

    @write_exceptions_log
    def change_other_user_access_type_to_group(self, user_name, other_user_id, group_id, access_type):
        """
            Change access type of another user to group for selected access type

            Args:
                user_name - name of user that changes access type of another user
                other_user_id - id of user for which type of access to group is changed
                group_id - id of group access type to which is changed
                access_type - new user access type to group

            Raises:
                AccessDenied exception, if user cannot change access type of another user to this group
        """
        logger = get_lib_logger()

        user_access_type = self.get_user_access_type_to_group(user_name, group_id)
        if user_access_type != UserType.AUTHOR:
            raise AccessDenied(('User {} can not change the access type of other users to group {}.'
                                .format(user_name, group_id)))

        relation = self.create_relation(other_user_id,
                                        group_id,
                                        RelatedObjects.USER_GROUP,
                                        relation_type=UserType(access_type))
        self.storage.update_relation(relation)

        other_user = self.storage.get_user_by_id(other_user_id)

        # Create notifications for all group-related users.
        users = self.storage.get_group_users(group_id)
        action_author = self.get_user_by_name(user_name)
        message = ('User {} set access type of user {} to task {} on {}.'.
                   format(user_name, other_user.name, group_id, UserType(access_type).name))
        self.create_notifications(action_author,
                                  message,
                                  users.keys())

        logger.info(message)

    @write_exceptions_log
    def get_user_access_type_to_task(self, user_name, task_id):
        """
            Returns type of user access to task

            Args:
                user_name(str) - user name
                task_id(int) - task id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        return self.storage.get_user_access_type_to_task(user_name, task_id)

    @write_exceptions_log
    def get_user_access_type_to_group(self, user_name, group_id):
        """
            Returns type of user access to group

            Args:
                user_name(str) - user name

                group_id(int) - group id, the type of access to which
                should be know

            Returns:
                Value of enumerator UserType or None, if user does
                not have access

        """
        return self.storage.get_user_access_type_to_group(user_name, group_id)

    @write_exceptions_log
    def create_plan(self, user_name, task_name, task_creation_time,
                    start_date, repeat_time_delta,
                    repeat_time_delta_months=0, end_date=None,
                    description=None, priority=1):
        """
            Create plan with given parameters

            Args:
                *user_name(str) - name of user who wants to create a plan
                *task_name(str) - name of the task created by the plan
                (may not be unique)
                *task_creation_time(datetime) - time in which plan should
                create a task
                *start_date(datetime) - date from which user can begin to
                execute task created by the plan
                *repeat_time_delta(timedelta) - number of weeks, days,
                hours and minutes through which plan will create a periodic task
                *repeat_time_delta_months(int) - number of months through
                which plan will create a periodic task(default 0)
                *end_date(datetime) - date to which you can execute task
                created by plan(default None)
                *description(str) - description of task created by plan(default None)
                *priority(int, TaskPriority enum) - priority of task created
                by plan(default 1)

            Returns:
                 instance of created plan(Plan object)

            Raises:
                IncorrectDateTime exception, if start date more end date
                IncorrectDateTime exception, if creation time more start date
                ValueError exception, if priority not in range(0, 4)
                ValueError exception, if task name is empty string

        """
        logger = get_lib_logger()

        plan_id = self.storage.get_new_plan_id()
        user_id = self.storage.get_user_id_by_user_name(user_name)

        if end_date is not None:
            if start_date > end_date:
                raise IncorrectDateTime('Start date more End date')

            if task_creation_time > start_date:
                raise IncorrectDateTime('Plan task must be created before it starts.')

        if priority not in range(0, 4):
            raise ValueError('Incorrect priority value')

        if task_name == '':
            raise ValueError('Task name must be not empty string.')

        plan = Plan(user_id,
                    plan_id,
                    task_name,
                    task_creation_time,
                    start_date,
                    repeat_time_delta,
                    repeat_time_delta_months=repeat_time_delta_months,
                    end_date=end_date,
                    description=description,
                    priority=priority)

        logger.info('User {} create plan {}'.format(user_name, plan_id))

        self.storage.add_plan(plan)

        return plan

    @write_exceptions_log
    def edit_plan(self, user_name, plan_id, new_task_name=None,
                  new_task_creation_time=None, new_start_date=None, new_repeat_time_delta=None,
                  new_repeat_time_delta_months=None, new_end_date=False,
                  new_description=None, new_priority=None):
        """
            Create plan with given parameters

            Args:
                *user_name(str) - name of user who wants to edit a plan
                *plan_id(int) - id of edited plan
                *new_task_name(str) - edited plan task name(default None)
                *new_task_creation_time(datetime) - edited plan task
                creation time(default None)
                *new_start_date(datetime) - edited plan task start date(default None)
                *new_repeat_time_delta(timedelta) - edited plan task
                repeat time delta(default None)
                *new_repeat_time_delta_months(int) - edited plan task repeat time
                delta months count(default None)
                *new_end_date(datetime) - edited task plan end date(default None)
                *new_description(str) - edited plan task description(default None)
                *new_priority(int, TaskPriority enum) - edited plan task priority(default None)

            Returns:
                 instance of edited plan(Plan object)

            Raises:
                AccessDenied exception, if this user cannot edit this plan
                IncorrectDateTime exception, if start date more end date
                IncorrectDateTime exception, if creation time more start date
                IncorrectDateTime exception, if repeat time delta months count is negative
                IncorrectDateTime exception, if repeat time delta is non-positive
                ValueError exception, if priority not in range(0, 4)
                ValueError exception, if task name is empty string
        """
        logger = get_lib_logger()

        user_id = self.storage.get_user_id_by_user_name(user_name)

        plan = self.storage.get_plan_by_id(plan_id)

        if plan.user_id != user_id:
            raise AccessDenied('User {} can not edit plan {}.'.format(user_id, plan_id))

        if new_task_name is not None:
            if new_task_name == '':
                raise ValueError('Name can not be empty.')
            plan.task_name = new_task_name

        if new_description is not None:
            plan.description = new_description

        if new_priority is not None:
            if new_priority not in range(0, 4):
                raise ValueError('Incorrect priority value')
            plan.priority = TaskPriority(new_priority)

        if new_repeat_time_delta is not None:
            plan.repeat_time_delta = new_repeat_time_delta

        if new_repeat_time_delta_months is not None:
            if new_repeat_time_delta_months < 0:
                raise IncorrectDateTime('Months сount must be non-negative.')
            plan.repeat_time_delta_months = new_repeat_time_delta_months

        if plan.repeat_time_delta_months == 0 and str(plan.repeat_time_delta) == '0:00:00':
            raise IncorrectDateTime('Delta time must be positive.')

        if new_task_creation_time is not None:
            plan.task_creation_time = new_task_creation_time

        if new_start_date is not None:
            plan.start_date = new_start_date

        if new_end_date is not None:
            if new_end_date:
                plan.end_date = new_end_date

        if plan.end_date is not None:
            if plan.start_date > plan.end_date:
                raise IncorrectDateTime('Start date more End date.')

        if plan.task_creation_time > plan.start_date:
            raise IncorrectDateTime('Plan task must be created before it starts.')

        logger.info('User {} edit plan {}.'.format(user_name, plan_id))

        self.storage.update_plan(plan_id, plan)

        return plan

    @write_exceptions_log
    def delete_plan(self, user_name, plan_id):
        """
            Delete selected plan

            Args:
                *user_name(str) - name of user who wants to delete plan
                *plan_id(int) - id of deleted plan

            Raises:
                AccessDenied exception, if this user cannot delete this plan

        """
        logger = get_lib_logger()

        user_id = self.storage.get_user_id_by_user_name(user_name)

        plan = self.storage.get_plan_by_id(plan_id)
        if plan.user_id != user_id:
            raise AccessDenied('User {} can not delete plan {}.'.format(user_id, plan_id))

        logger.info('User {} delete plan {}.'.format(user_name, plan_id))

        self.storage.delete_plan(plan_id)

    @write_exceptions_log
    def get_plan_by_id(self, plan_id):
        """
            Returns required plan by its id

            Args:
                plan_id(int) - required plan id

            Returns:
                instance of required plan(Plan object)

            Raises:
                PlanNotFound exception, if required plan is not found

        """
        return self.storage.get_plan_by_id(plan_id)

    @write_exceptions_log
    def update_plans(self, user_name):
        """
            Checks all plans and creates tasks, if time has come

            Args:
                user_name(str) - name of current user

        """
        logger = get_lib_logger()

        plans = self.storage.get_all_plans()

        for plan in plans:
            while plan.task_creation_time <= datetime.datetime.now():
                self.create_task(user_name,
                                 plan.task_name,
                                 plan.start_date,
                                 end_date=plan.end_date,
                                 description=plan.description,
                                 priority=plan.priority.value)

                plan.task_creation_time += plan.repeat_time_delta
                plan.task_creation_time = add_months_to_date(plan.task_creation_time,
                                                             plan.repeat_time_delta_months)

                plan.start_date += plan.repeat_time_delta
                plan.start_date = add_months_to_date(plan.start_date,
                                                     plan.repeat_time_delta_months)

                if plan.end_date is not None:
                    plan.end_date += plan.repeat_time_delta
                    plan.end_date = add_months_to_date(plan.end_date,
                                                       plan.repeat_time_delta_months)

        for plan in plans:
            self.storage.update_plan(plan.id, plan)

        logger.info('All plans have been updated.')

    @write_exceptions_log
    def create_notification(self, user_name, notification_creation_time, notification_message):
        """
            Create notification with given parameters

            Args:
                *user_name(str) - name of user who wants to create a notification
                *notification_creation_time(datetime) - time in which notification
                should create a message
                *notification_message(str) - notification message

            Returns:
                 instance of created notification(Notification object)

            Raises:
                ValueError exception, if notification message is empty string

        """
        logger = get_lib_logger()

        notification_id = self.storage.get_new_notification_id()
        user_id = self.storage.get_user_id_by_user_name(user_name)

        if notification_message == '':
            raise ValueError('Notification message must be not empty string.')

        notification = Notification(user_id,
                                    notification_id,
                                    notification_creation_time,
                                    notification_message)

        logger.info('User {} create notification {}'.format(user_name, notification_id))

        self.storage.add_notification(notification)

        return notification

    @write_exceptions_log
    def edit_notification(self, user_name, notification_id,
                          new_notification_creation_time=None, new_notification_message=None):
        """
            Create notification with given parameters

            Args:
                *user_name(str) - name of user who wants to edit a notification
                *notification_id(int) - id of edited notification
                *new_notification_creation_time(datetime) - edited notification
                message creation time(default None)
                *new_notification_message(str) - edited notification message(default None)

            Returns:
                 instance of edited notification(Notification object)

            Raises:
                AccessDenied exception, if this user cannot edit this notification
                ValueError exception, if new notification message is empty string
        """
        logger = get_lib_logger()

        user_id = self.storage.get_user_id_by_user_name(user_name)

        notification = self.storage.get_notification_by_id(notification_id)

        if notification.user_id != user_id:
            raise AccessDenied('User {} can not edit notification {}.'.format(user_id, notification_id))

        if new_notification_message is not None:
            if new_notification_message == '':
                raise ValueError('Notification message can not be empty string.')
            notification.message = new_notification_message

        if new_notification_creation_time is not None:
            notification.creation_time = new_notification_creation_time

        logger.info('User {} edit notification {}.'.format(user_name, notification_id))

        self.storage.update_notification(notification_id, notification)

        return notification

    @write_exceptions_log
    def delete_notification(self, user_name, notification_id):
        """
            Delete selected notification

            Args:
                *user_name(str) - name of user who wants to delete notification
                *notification_id(int) - id of deleted notification

            Raises:
                AccessDenied exception, if this user cannot delete this notification

        """
        logger = get_lib_logger()

        user_id = self.storage.get_user_id_by_user_name(user_name)

        notification = self.storage.get_notification_by_id(notification_id)
        if notification.user_id != user_id:
            raise AccessDenied('User {} can not delete notification {}.'.format(user_id, notification_id))

        logger.info('User {} delete notification {}.'.format(user_name, notification_id))

        self.storage.delete_notification(notification_id)

    @write_exceptions_log
    def get_notification_by_id(self, notification_id):
        """
            Returns required notification by its id

            Args:
                notification_id(int) - required notification id

            Returns:
                instance of required notification(Notification object)

            Raises:
                NotificationNotFound exception, if required notification is not found

        """
        return self.storage.get_notification_by_id(notification_id)

    @write_exceptions_log
    def update_notifications(self, user_name):
        """
            Checks all user notifications and create messages,
            if time has come

            Args:
                user_name - name of current user

        """
        logger = get_lib_logger()

        user_id = self.get_user_id_by_user_name(user_name)
        user_notifications = self.storage.get_user_notifications(user_id)

        deleted_notification = []
        for notification in user_notifications:
            if datetime.datetime.now() > notification.creation_time:
                message = '[{}] {}'.format(notification.creation_time, notification.message)
                self.storage.add_message(user_id, message)
                deleted_notification.append(notification.id)

        for notification_id in deleted_notification:
            self.delete_notification(user_name, notification_id)

        logger.info('All notifications have been updated.')

    @write_exceptions_log
    def create_notifications(self, action_author, notification_message, users):
        """
            Create notifications for users

            Args:
                *action_author(User object) - author of action that caused notification
                *notification_message(str) - notification message
                *users(Users list) - list of users who received notification

        """
        for user in users:
            if action_author is not None:
                if user.id == action_author.id:
                    continue

            self.create_notification(user.name,
                                     datetime.datetime.now(),
                                     notification_message)

    @write_exceptions_log
    def clear_user_messages(self, user_name):
        """
            Delete all user messages

            Args:
                user_name - name of current user

        """
        user_id = self.storage.get_user_id_by_user_name(user_name)
        self.storage.clear_user_messages(user_id)

        get_lib_logger().info('All user {} messages have been deleted.'.format(user_name))

    @write_exceptions_log
    def create_relation(self, first_elem_id, second_elem_id,
                        related_objects, relation_type=None):

        relation = Relation(first_elem_id,
                            second_elem_id,
                            related_objects,
                            relation_type=relation_type)

        return relation

    def get_all_relations(self):
        """Returns all relations"""
        return self.storage.get_all_relations()

    @write_exceptions_log
    def is_parent(self, expected_parent_id, expected_child_id):
        """
            Checks if one task is an ancestor of another task in task tree

            Args:
                expected_parent_id - id of expected parent task
                expected_child_id - id of expected child task

            Returns:
                True, if task with id expected_parent_id is parent of task with id expected_child_id
                else False

        """
        while expected_child_id is not None:
            if expected_child_id == expected_parent_id:
                return True
            task = self.storage.get_task_by_id(expected_child_id)
            expected_child_id = task.parent_task_id

        return False


@write_exceptions_log
def add_months_to_date(date, months_count):
    """
        Add a specified number of months to the specified date

        Args:
            date - current date
            months_count - number of months to add

        Returns:
            new datetime

    """
    new_month = date.month + months_count
    new_year = date.year + new_month // 12
    new_month %= 12
    if new_month == 0:
        new_month = 12
        new_year -= 1

    new_day = date.day
    new_month_days_count = calendar.mdays[new_month]

    if new_day > new_month_days_count:
        new_day %= calendar.mdays[new_month]
        new_month += 1

    new_year += new_month // 12
    new_month %= 12
    if new_month == 0:
        new_month = 12
        new_year -= 1

    new_date = datetime.date(year=new_year,
                             month=new_month,
                             day=new_day)

    new_time = datetime.time(hour=date.hour,
                             minute=date.minute)

    return datetime.datetime.combine(date=new_date, time=new_time)
