import datetime
import os
import unittest

from library.models.task import (
    TaskPriority,
    TaskStatus
)
from library.service.exceptions import (
    TaskNotFound,
    AccessDenied,
    IncorrectDateTime,
    IncorrectAction
)
from library.service.lib_logger import (
    enable_lib_logger,
    disable_lib_logger,
    get_lib_logger
)
from library.tasktracker import TaskTracker


class TaskTests(unittest.TestCase):
    def setUp(self):
        self.is_logger_disabled = get_lib_logger().disabled
        disable_lib_logger()

        storage_path = os.path.expanduser('~/TaskTracker/tests')
        self.tasktracker = TaskTracker(storage_path)

        self.tasktracker.create_user(user_name='test_user')
        self.test_user2 = self.tasktracker.create_user(user_name='test_user2')
        self.test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test task',
            start_date=datetime.datetime(year=2018, month=9, day=3, hour=12),
            end_date=datetime.datetime(year=2018, month=9, day=3, hour=18),
            description='test desc',
            priority=2
        )

    def test_create_task(self):
        task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test task',
            start_date=datetime.datetime(year=2018, month=9, day=3, hour=12),
            end_date=datetime.datetime(year=2018, month=9, day=3, hour=18),
            description='test desc',
            priority=2
        )

        self.assertEqual(task.id, 2)
        self.assertEqual(task.name, 'test task')
        self.assertEqual(task.start_date, datetime.datetime(year=2018, month=9, day=3, hour=12))
        self.assertEqual(task.end_date, datetime.datetime(year=2018, month=9, day=3, hour=18))
        self.assertEqual(task.description, 'test desc')
        self.assertEqual(task.priority, TaskPriority(2))

    def test_delete_task_user_author(self):
        task_id = self.test_task.id

        self.tasktracker.delete_task(user_name='test_user', task_id=task_id)
        with self.assertRaises(TaskNotFound):
            self.tasktracker.storage.get_task_by_id(task_id)

    def test_delete_task_user_not_author(self):
        for i in range(1, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=self.test_user2.id,
                        task_id=self.test_task.id,
                        access_type=i
                    )

                    self.tasktracker.delete_task(user_name='test_user2', task_id=self.test_task.id)

    def test_edit_task(self):
        task = self.tasktracker.edit_task(
            user_name='test_user',
            task_id=self.test_task.id,
            new_task_name='edit test task',
            new_start_date=datetime.datetime(year=2019, month=10, day=4, hour=13),
            new_end_date=datetime.datetime(year=2019, month=10, day=4, hour=19),
            new_description='edit test desc',
            new_priority=3
        )

        self.assertEqual(task.id, 1)
        self.assertEqual(task.name, 'edit test task')
        self.assertEqual(task.start_date, datetime.datetime(year=2019, month=10, day=4, hour=13))
        self.assertEqual(task.end_date, datetime.datetime(year=2019, month=10, day=4, hour=19))
        self.assertEqual(task.description, 'edit test desc')
        self.assertEqual(task.priority, TaskPriority(3))

    def test_edit_task_user_not_author_or_moderator(self):
        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=self.test_user2.id,
                        task_id=self.test_task.id,
                        access_type=i
                    )

                    self.tasktracker.edit_task(
                        user_name='test_user2',
                        task_id=self.test_task.id,
                        new_task_name='edit test task',
                        new_start_date=datetime.datetime(year=2019, month=10, day=4, hour=13),
                        new_end_date=datetime.datetime(year=2019, month=10, day=4, hour=19),
                        new_description='edit test desc',
                        new_priority=3
                    )

    def test_edit_task_incorrect_name(self):
        with self.assertRaises(ValueError):
            self.tasktracker.edit_task(user_name='test_user', task_id=1, new_task_name='')

    def test_edit_task_start_date_more_end_date(self):
        with self.assertRaises(IncorrectDateTime):
            self.tasktracker.edit_task(
                user_name='test_user',
                task_id=1,
                new_start_date=datetime.datetime(year=2019, month=10, day=4, hour=19),
                new_end_date=datetime.datetime(year=2019, month=10, day=4, hour=13)
            )

    def test_edit_task_incorrect_priority(self):
        with self.assertRaises(ValueError):
            self.tasktracker.edit_task(user_name='test_user', task_id=1, new_priority=5)

    def test_add_task_to_archive(self):
        self.tasktracker.add_task_to_archive(user_name='test_user', task_id=self.test_task.id)

        self.test_task = self.tasktracker.get_task_by_id('test_user', self.test_task.id)
        self.assertTrue(self.test_task.in_archive)

    def test_add_task_to_archive_user_access_type_none(self):
        with self.assertRaises(AccessDenied):
            self.tasktracker.add_task_to_archive(user_name='test_user2', task_id=self.test_task.id)

    def test_add_task_to_archive_user_access_type_watcher(self):
        self.tasktracker.change_other_user_access_type_to_task(
            user_name='test_user',
            other_user_id=self.test_user2.id,
            task_id=self.test_task.id,
            access_type=3
        )

        with self.assertRaises(AccessDenied):
            self.tasktracker.add_task_to_archive(user_name='test_user2', task_id=self.test_task.id)

    def test_move_task_tree_new_parent_task_none(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        self.tasktracker.move_task_tree(
            user_name='test_user',
            root_task_id=test_subtask.id,
            new_root_task_id=None,
            task_relation_type=1
        )

        subtask = self.tasktracker.storage.get_task_by_id(test_subtask.id)

        self.assertEqual(subtask.parent_task_id, None)
        self.assertEqual(subtask.parent_task_relation_type, None)

    def test_move_task_tree_new_parent_task_not_none(self):
        test_task2 = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task2',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.move_task_tree(
            user_name='test_user',
            root_task_id=self.test_task.id,
            new_root_task_id=test_task2.id,
            task_relation_type=1
        )

        subtasks = self.tasktracker.get_task_subtasks('test_user', test_task2.id)
        self.assertEqual(len(subtasks), 1)

    def test_move_task_tree_user_not_author_or_moderator(self):
        test_task2 = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task2',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=self.test_user2.id,
                        task_id=self.test_task.id,
                        access_type=i
                    )

                    self.tasktracker.move_task_tree(
                        user_name='test_user2',
                        root_task_id=self.test_task.id,
                        new_root_task_id=test_task2.id,
                        task_relation_type=1
                    )

    def test_add_subtask_user_not_task_author_or_moderator(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=2,
                        task_id=self.test_task.id,
                        access_type=i)

                    self.tasktracker.add_subtask(
                        user_name='test_user2',
                        task_id=self.test_task.id,
                        subtask_id=test_subtask.id,
                        task_relation_type=1
                    )

    def test_add_subtask_user_not_subtask_author_or_moderator(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user2',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user2',
                        other_user_id=1,
                        task_id=test_subtask.id,
                        access_type=i
                    )

                    self.tasktracker.add_subtask(
                        user_name='test_user',
                        task_id=self.test_task.id,
                        subtask_id=test_subtask.id,
                        task_relation_type=1
                    )

    def test_add_subtask_cyclic_links(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        with self.assertRaises(IncorrectAction):
            self.tasktracker.add_subtask(
                user_name='test_user',
                task_id=test_subtask.id,
                subtask_id=self.test_task.id,
                task_relation_type=1
            )

    def test_add_subtask_already_added(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        with self.assertRaises(IncorrectAction):
            self.tasktracker.add_subtask(
                user_name='test_user',
                task_id=self.test_task.id,
                subtask_id=test_subtask.id,
                task_relation_type=1
            )

    def test_add_subtask(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        subtasks = self.tasktracker.get_task_subtasks('test_user', self.test_task.id)
        self.assertEqual(len(subtasks), 1)

    def test_delete_subtask_user_not_task_author_or_moderator(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=2,
                        task_id=self.test_task.id,
                        access_type=i
                    )

                    self.tasktracker.delete_subtask(
                        user_name='test_user2',
                        task_id=self.test_task.id,
                        subtask_id=test_subtask.id
                    )

    def test_delete_subtask_user_not_subtask_author_or_moderator(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        self.tasktracker.change_other_user_access_type_to_task(
            user_name='test_user',
            other_user_id=1,
            task_id=self.test_user2.id,
            access_type=1
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=1,
                        task_id=test_subtask.id,
                        access_type=i
                    )

                    self.tasktracker.delete_subtask(
                        user_name='test_user2',
                        task_id=self.test_task.id,
                        subtask_id=test_subtask.id
                    )

    def test_delete_subtask_is_not_subtask(self):
        test_subtask1 = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task1',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        test_subtask2 = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task2',
            start_date=datetime.datetime(
                year=2013,
                month=12,
                day=13,
                hour=13,
                minute=13
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask1.id,
            task_relation_type=1
        )

        with self.assertRaises(IncorrectAction):
            self.tasktracker.delete_subtask(
                user_name='test_user',
                task_id=self.test_task.id,
                subtask_id=test_subtask2.id
            )

    def test_delete_subtask(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        self.tasktracker.delete_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id
        )

        subtasks = self.tasktracker.get_task_subtasks('test_user', self.test_task.id)
        self.assertEqual(len(subtasks), 0)

    def test_blocking_task_relation(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        self.tasktracker.update_tasks()

        with self.assertRaises(AccessDenied):
            self.tasktracker.change_task_status(
                user_name='test_user',
                task_id=self.test_task.id,
                new_status=3
            )

    def test_depends_on_task_relation(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=0
        )

        self.tasktracker.update_tasks()
        self.test_task = self.tasktracker.get_task_by_id('test_user', 1)

        self.tasktracker.change_task_status(
                user_name='test_user',
                task_id=self.test_task.id,
                new_status=3
            )

        test_subtask = self.tasktracker.get_task_by_id('test_user', 2)
        self.assertEqual(test_subtask.status, TaskStatus.DONE)

    def test_update_parent_task_if_blocking_subtask_failed(self):
        test_subtask = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_subtask',
            start_date=datetime.datetime(
                year=2012,
                month=12,
                day=12,
                hour=12,
                minute=12
            )
        )

        self.tasktracker.add_subtask(
            user_name='test_user',
            task_id=self.test_task.id,
            subtask_id=test_subtask.id,
            task_relation_type=1
        )

        self.tasktracker.update_tasks()
        self.test_task = self.tasktracker.get_task_by_id('test_user', 1)
        test_subtask = self.tasktracker.get_task_by_id('test_user', 2)

        self.tasktracker.change_task_status(
            user_name='test_user',
            task_id=test_subtask.id,
            new_status=3
        )

        self.tasktracker.change_task_status(
            user_name='test_user',
            task_id=self.test_task.id,
            new_status=3
        )

        self.test_task = self.tasktracker.get_task_by_id('test_user', 1)
        self.assertEqual(self.test_task.status, TaskStatus.DONE)

        self.tasktracker.change_task_status(
            user_name='test_user',
            task_id=test_subtask.id,
            new_status=4
        )

        self.test_task = self.tasktracker.get_task_by_id('test_user', 1)
        self.assertEqual(self.test_task.status, TaskStatus.STARTED)

    def test_change_task_status_not_started(self):
        with self.assertRaises(IncorrectAction):
            self.tasktracker.change_task_status(
                user_name='test_user',
                task_id=self.test_task.id,
                new_status=3
            )

    def test_change_task_status_access_denied(self):
        with self.assertRaises(AccessDenied):
            self.tasktracker.change_task_status(
                user_name='test_user2',
                task_id=self.test_task.id,
                new_status=3
            )

    def test_change_task_status_to_started_if_created(self):
        with self.assertRaises(IncorrectAction):
            self.tasktracker.change_task_status(
                user_name='test_user',
                task_id=self.test_task.id,
                new_status=1
            )

    def tearDown(self):
        if not self.is_logger_disabled:
            enable_lib_logger()
        self.tasktracker.clear_storage()


if __name__ == '__main__':
    unittest.main()
