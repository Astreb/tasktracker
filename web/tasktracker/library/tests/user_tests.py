import unittest

import os

from library.models.user import UserType
from library.service.exceptions import AccessDenied
from library.service.lib_logger import (
    disable_lib_logger,
    enable_lib_logger,
    get_lib_logger
)
from library.tasktracker import TaskTracker


class UserTests(unittest.TestCase):
    def setUp(self):
        self.is_logger_disabled = get_lib_logger().disabled
        disable_lib_logger()

        storage_path = os.path.expanduser('~/TaskTracker/tests')
        self.tasktracker = TaskTracker(storage_path)

        self.test_user = self.tasktracker.create_user(user_name='test_user')
        self.test_user2 = self.tasktracker.create_user(user_name='test_user2')

    def test_create_user(self):
        user = self.tasktracker.create_user(user_name='user', user_email='email')

        self.assertEqual(user.id, 3)
        self.assertEqual(user.name, 'user')
        self.assertEqual(user.email, 'email')

    def test_edit_user(self):
        edited_test_user = self.tasktracker.edit_user(
            user_id=self.test_user.id,
            new_user_name='test_user_name',
            new_user_email='test_user_email'
        )

        self.assertEqual(edited_test_user.id, 1)
        self.assertEqual(edited_test_user.name, 'test_user_name')
        self.assertEqual(edited_test_user.email, 'test_user_email')

    def test_user_author_change_other_user_access_type_to_task(self):
        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        for i in range(1, 4):
            with self.subTest(i=i):
                self.tasktracker.change_other_user_access_type_to_task(
                    user_name='test_user',
                    other_user_id=self.test_user2.id,
                    task_id=test_task.id,
                    access_type=i
                )

                access_type = self.tasktracker.get_user_access_type_to_task(
                    user_name='test_user2',
                    task_id=test_task.id
                )

                self.assertIs(access_type, UserType(i))

    def test_user_not_author_change_other_user_access_type_to_task(self):
        test_task = self.tasktracker.create_task(
            user_name='test_user2',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        with self.assertRaises(AccessDenied):
            self.tasktracker.change_other_user_access_type_to_task(
                user_name='test_user',
                other_user_id=self.test_user2.id,
                task_id=test_task.id,
                access_type=1
            )

    def test_user_author_change_other_user_access_type_to_group(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        for i in range(1, 4):
            with self.subTest(i=i):
                self.tasktracker.change_other_user_access_type_to_group(
                    user_name='test_user',
                    other_user_id=self.test_user2.id,
                    group_id=test_group.id,
                    access_type=i
                )

                access_type = self.tasktracker.get_user_access_type_to_group(
                    user_name='test_user2',
                    group_id=test_group.id
                )

                self.assertIs(access_type, UserType(i))

    def test_user_not_author_change_other_user_access_type_to_group(self):
        test_group = self.tasktracker.create_group(user_name='test_user2', group_name='test_group')

        with self.assertRaises(AccessDenied):
            self.tasktracker.change_other_user_access_type_to_group(
                user_name='test_user',
                other_user_id=self.test_user2.id,
                group_id=test_group.id,
                access_type=1
            )

    def tearDown(self):
        if not self.is_logger_disabled:
            enable_lib_logger()
        self.tasktracker.clear_storage()


if __name__ == '__main__':
    unittest.main()
