"""
    Provides modules for library service

    Modules:
        exceptions - provides all TaskTracker exceptions
        lib_logger - provides functions for working with the library's logger
"""