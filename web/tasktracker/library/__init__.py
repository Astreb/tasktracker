"""
    TaskTracker library

    Packages:
        models - TaskTracker models
        service - provides modules for library service
        storage - library storage logic

    Modules:
        tasktracker - provides class that contains all functions for working with library

    Usage:
        In Python

        $ import library
        $ from library.tasktracker import TaskTracker
        $ import datetime
        $ db_path = '~/TaskTracker/storage'

        # Create TaskTracker
        $ tasktracker = TaskTracker(db_path)

        # Create new user
        $ user = tasktracker.create_user('Astreb', 'astreb@gmail.com')

        # Create new task
        $ task = tasktracker.create_task('Astreb', 'test task', datetime.datetime.now())

        # Show task properties
        $ task.name
        'test_task'
        $ task.status
        <TaskStatus.CREATED: 0>
        $ tasktracker.start_date
        datetime.datetime(2018, 9, 11, 14, 8, 13, 958691)

        # Update task status
        $ tasktracker.update_tasks()

        # Get new task instance
        $ task = tasktracker.get_task_by_id('Astreb', 1)

        # Show updated status
        $ task.status
        <TaskStatus.STARTED: 1>
"""