"""
    Provides classes for working with JSON storage

    Classes:
        JSONPaths - contains the const names of the files that contain the data
        JSONStorage - contains functions for working with JSON storage

"""
import os
import shutil

from json_tricks import (
    load,
    dump
)

from library.models.relation import RelatedObjects
from library.service.exceptions import (
    TaskNotFound,
    GroupNotFound,
    UserNotFound,
    PlanNotFound,
    NotificationNotFound,
    RelationNotFound)


class JSONPaths:
    """Contains the const names of the files that contain the data"""
    TASKS = 'tasks.json'
    GROUPS = 'groups.json'
    USERS = 'users.json'
    PLANS = 'plans.json'
    NOTIFICATIONS = 'notifications.json'
    MESSAGES = 'messages.json'
    RELATIONS = 'relations.json'
    CURRENT_MAX_ID = 'current_max_id.json'


class JSONStorage:
    """
        Contains functions for working with JSON storage

        Attributes:
            db_path - path to storage directory
            current_max_id - dict of current max IDs

    """

    def __init__(self, db_path):
        """
            Args:
                db_path - path to storage directory

        """
        self.db_path = db_path
        self.current_max_id = {}

        self.prepare_database()
        self.load_current_max_id()

    def create_json_empty_file(self, file_path):
        """
            Create empty JSON file

            Args:
                file_path - path to file which you need to create

        """
        with open(file_path, 'w') as file:
            file.write('[]')

    def prepare_database(self):
        """Create a directory and files for data storage, if not exists"""
        if not os.path.exists(self.db_path):
            os.mkdir(self.db_path)

        if not os.path.exists(os.path.join(self.db_path, JSONPaths.TASKS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.TASKS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.GROUPS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.GROUPS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.USERS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.USERS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.PLANS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.PLANS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.NOTIFICATIONS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.NOTIFICATIONS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.MESSAGES)):
            with open(os.path.join(self.db_path, JSONPaths.MESSAGES), 'w') as file:
                file.write('{}')
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.RELATIONS)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.RELATIONS))
        if not os.path.exists(os.path.join(self.db_path, JSONPaths.CURRENT_MAX_ID)):
            self.create_json_empty_file(os.path.join(self.db_path, JSONPaths.CURRENT_MAX_ID))
            self.current_max_id = {'task': 0, 'group': 0, 'user': 0, 'plan': 0, 'notification': 0}
            self.save_current_max_id()

    def load_data(self, file_path):
        """
            Load data from file

            Args:
                file_path(str) - path to JSON file

        """
        with open(file_path, 'r') as file:
            return load(fp=file)

    def save_data(self, file_path, data):
        """
            Save data to file

            Args:
                file_path(str) - path to JSON file
                data - data to be saved

        """
        with open(file_path, 'w') as file:
            dump(data, fp=file, indent=4)

    def load_current_max_id(self):
        """Load current max IDs from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.CURRENT_MAX_ID)
        self.current_max_id = self.load_data(full_file_path)

    def save_current_max_id(self):
        """Save current max IDs to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.CURRENT_MAX_ID)
        self.save_data(full_file_path, self.current_max_id)

    def load_tasks(self):
        """Load tasks from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.TASKS)
        return self.load_data(full_file_path)

    def save_tasks(self, tasks):
        """Save tasks to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.TASKS)
        self.save_data(full_file_path, tasks)

    def load_groups(self):
        """Load groups from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.GROUPS)
        return self.load_data(full_file_path)

    def save_groups(self, groups):
        """Save groups to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.GROUPS)
        self.save_data(full_file_path, groups)

    def load_users(self):
        """Load users from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.USERS)
        return self.load_data(full_file_path)

    def save_users(self, users):
        """Save users to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.USERS)
        self.save_data(full_file_path, users)

    def load_plans(self):
        """Load plans from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.PLANS)
        return self.load_data(full_file_path)

    def save_plans(self, plans):
        """Save plans to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.PLANS)
        self.save_data(full_file_path, plans)

    def load_notifications(self):
        """Load notifications from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.NOTIFICATIONS)
        return self.load_data(full_file_path)

    def save_notifications(self, notifications):
        """Save notifications to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.NOTIFICATIONS)
        self.save_data(full_file_path, notifications)

    def load_messages(self):
        """Load messages from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.MESSAGES)
        return self.load_data(full_file_path)

    def save_messages(self, messages):
        """Save messages to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.MESSAGES)
        self.save_data(full_file_path, messages)

    def load_relations(self):
        """Load relations from file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.RELATIONS)
        return self.load_data(full_file_path)

    def save_relations(self, relations):
        """Save relations to file"""
        full_file_path = os.path.join(self.db_path, JSONPaths.RELATIONS)
        self.save_data(full_file_path, relations)

    def clear_storage(self):
        """Delete all storage data"""
        shutil.rmtree(self.db_path)

    def get_new_task_id(self):
        """Get and return a new id for task"""
        self.current_max_id['task'] += 1
        self.save_current_max_id()
        return self.current_max_id['task']

    def get_new_group_id(self):
        """Get and return a new id for group"""
        self.current_max_id['group'] += 1
        self.save_current_max_id()
        return self.current_max_id['group']

    def get_new_user_id(self):
        """Get and return a new id for user"""
        self.current_max_id['user'] += 1
        self.save_current_max_id()
        return self.current_max_id['user']

    def get_new_plan_id(self):
        """Get and return a new id for plan"""
        self.current_max_id['plan'] += 1
        self.save_current_max_id()
        return self.current_max_id['plan']

    def get_new_notification_id(self):
        """Get and return a new id for notification"""
        self.current_max_id['notification'] += 1
        self.save_current_max_id()
        return self.current_max_id['notification']

    def get_task_by_id(self, task_id):
        """
            Returns required task by its id

            Args:
                task_id(int) - id of required task

            Returns:
                instance of required task(Task object)

            Raises:
                TaskNotFound exception, if required task is not found

        """
        tasks = self.load_tasks()
        for task in tasks:
            if task.id == task_id:
                return task
        raise TaskNotFound(task_id)

    def add_task(self, task):
        """
            Add new task to storage

            Args:
                task(Task object) - instance of the task to be added

        """
        tasks = self.load_tasks()
        tasks.append(task)
        self.save_tasks(tasks)

    def delete_task(self, task_id):
        """
            Delete task from storage

            Args:
                task_id(int) - id of the task to be deleted

            Raises:
                TaskNotFound exception, if deleted task is not found
        """
        tasks = self.load_tasks()
        for task in tasks:
            if task.id == task_id:
                tasks.remove(task)
                self.save_tasks(tasks)
                return
        raise TaskNotFound(task_id)

    def update_task(self, task_id, task):
        """
            Replaces old instance of task with a new instance

            Args:
                task_id(int) - updated task id
                task(Task object) - new instance of updated task

            Raises:
                TaskNotFound exception, if updatable task is not found

        """
        tasks = self.load_tasks()
        for index in range(len(tasks)):
            if tasks[index].id == task_id:
                tasks[index] = task
                self.save_tasks(tasks)
                return
        raise TaskNotFound(task_id)

    def get_all_tasks(self):
        """Returns a list of all tasks"""
        return self.load_tasks()

    def get_task_users(self, task_id):
        """
            Returns a dict of all users who have access to task and their access types to task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all users who have access to task and their access types to task

            Raises:
                TaskNotFound exception, if required task is not found
        """
        try:
            relations = self.load_relations()
            users = {}
            for relation in relations:
                if relation.related_objects == RelatedObjects.USER_TASK:
                    if relation.second_elem_id == task_id:
                        user = self.get_user_by_id(relation.first_elem_id)
                        users[user] = relation.relation_type
            return users
        except TaskNotFound:
            raise

    def get_task_groups(self, task_id):
        """
            Returns a list of all groups to which task is included

            Args:
                task_id(int) - required task id

            Returns:
                 list of all groups to which task is included

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            relations = self.load_relations()
            groups = []
            for relation in relations:
                if relation.related_objects == RelatedObjects.GROUP_TASK:
                    if relation.second_elem_id == task_id:
                        groups.append(self.get_group_by_id(relation.first_elem_id))
            return groups
        except TaskNotFound:
            raise

    def get_task_subtasks(self, task_id):
        """
            Returns a dict of all subtasks and type of their relation with task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all subtasks and type of their relation with task

            Raises:
                TaskNotFound exception, if required task is not found

        """
        try:
            relations = self.load_relations()
            subtasks = {}
            for relation in relations:
                if relation.related_objects == RelatedObjects.TASK_SUBTASK:
                    if relation.first_elem_id == task_id:
                        subtask = self.get_task_by_id(relation.second_elem_id)
                        subtasks[subtask] = relation.relation_type
            return subtasks
        except TaskNotFound:
            raise

    def get_group_by_id(self, group_id):
        """
            Returns required group by its id

            Args:
                group_id(int) - required group id

            Returns:
                instance of required group(Group object)

            Raises:
                GroupNotFound exception, if required group is not found

        """
        groups = self.load_groups()
        for group in groups:
            if group.id == group_id:
                return group
        raise GroupNotFound(group_id)

    def add_group(self, group):
        """
            Add new group to storage

            Args:
                group(Group object) - instance of added group

        """
        groups = self.load_groups()
        groups.append(group)
        self.save_groups(groups)

    def delete_group(self, group_id):
        """
            Delete group from storage

            Args:
                group_id(int) - id of the group to be deleted

            Raises:
                GroupNotFound exception, if deleted group is not found

        """
        groups = self.load_groups()
        for group in groups:
            if group.id == group_id:
                groups.remove(group)
                self.save_groups(groups)
                return
        raise GroupNotFound(group_id)

    def update_group(self, group_id, group):
        """
            Replaces old instance of group with a new instance

            Args:
                group_id(int) - updated group id
                group(Group object) - new instance of updated group

            Raises:
                GroupNotFound exception, if updatable group is not found

        """
        groups = self.load_groups()
        for index in range(len(groups)):
            if groups[index].id == group_id:
                groups[index] = group
                self.save_groups(groups)
                return
        raise GroupNotFound(group_id)

    def get_group_users(self, group_id):
        """
            Returns a dict of all users who have access to group and
            their access types to group

            Args:
                group_id(int) - required group id

            Returns:
                dict of all users who have access to group and their access types to group

            Raises:
                GroupNotFound exception, if required group is not found

        """
        try:
            relations = self.load_relations()
            users = {}
            for relation in relations:
                if relation.related_objects == RelatedObjects.USER_GROUP:
                    if relation.second_elem_id == group_id:
                        user = self.get_user_by_id(relation.first_elem_id)
                        users[user] = relation.relation_type
            return users
        except GroupNotFound:
            raise

    def get_group_tasks(self, group_id):
        """
            Returns a list of all group tasks

            Args:
                group_id(int) - required group id

            Returns:
                list of all group tasks

            Raises:
                GroupNotFound exception, if required group is not found

        """
        try:
            relations = self.load_relations()
            tasks = []
            for relation in relations:
                if relation.related_objects == RelatedObjects.GROUP_TASK:
                    if relation.first_elem_id == group_id:
                        tasks.append(self.get_task_by_id(relation.second_elem_id))
            return tasks
        except GroupNotFound:
            raise

    def check_user_existence(self, user_name):
        """
            Checks if there is already a user with that name

            Args:
                user_name(str) - name of required user

            Returns:
                 True, if a user with this name is found.
                 else False

        """
        users = self.load_users()
        for user in users:
            if user.name == user_name:
                return True

        return False

    def get_user_by_id(self, user_id):
        """
            Returns required user by its id

            Args:
                user_id(int) - id of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        users = self.load_users()
        for user in users:
            if user.id == user_id:
                return user
        raise UserNotFound(user_id)

    def get_user_by_name(self, user_name):
        """
            Returns required user by its name

            Args:
                user_name(str) - name of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        users = self.load_users()
        for user in users:
            if user.name == user_name:
                return user
        raise UserNotFound(user_name)

    def get_user_id_by_user_name(self, user_name):
        """
            Returns user id by its name

            Args:
                user_name(str) - name of required user

            Returns:
                id of required user(int)

        """
        user = self.get_user_by_name(user_name)
        return user.id

    def add_user(self, user):
        """
            Add new user to storage

            Args:
                user(User object) - instance of added user

        """
        users = self.load_users()
        users.append(user)
        self.save_users(users)

    def update_user(self, user_id, user):
        """
            Replaces old instance of user with a new instance

            Args:
                user_id(int) - updated user id
                user(User object) - new instance of updated user

            Raises:
                UserNotFound exception, if updatable user is not found

        """
        users = self.load_users()
        for index in range(len(users)):
            if users[index].id == user_id:
                users[index] = user
                self.save_users(users)
                return
        raise UserNotFound(user_id)

    def get_user_tasks(self, user_id):
        """
            Returns a dict of all tasks to which the user has access and
            type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all tasks to which the user has access and
                type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            relations = self.load_relations()
            tasks = {}
            for relation in relations:
                if relation.related_objects == RelatedObjects.USER_TASK:
                    if relation.first_elem_id == user_id:
                        task = self.get_task_by_id(relation.second_elem_id)
                        tasks[task] = relation.relation_type
            return tasks
        except UserNotFound:
            raise

    def get_user_groups(self, user_id):
        """
            Returns a dict of all groups to which the user has access and
            type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all groups to which the user has access and
                type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            relations = self.load_relations()
            groups = {}
            for relation in relations:
                if relation.related_objects == RelatedObjects.USER_GROUP:
                    if relation.first_elem_id == user_id:
                        group = self.get_group_by_id(relation.second_elem_id)
                        groups[group] = relation.relation_type
            return groups
        except UserNotFound:
            raise

    def get_user_plans(self, user_id):
        """
            Returns a list of all user plans

            Args:
                user_id(int) - required user id

            Returns:
                list of all user plans

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            all_plans = self.load_plans()
            user_plans = []
            for plan in all_plans:
                if plan.user_id == user_id:
                    user_plans.append(plan)
            return user_plans
        except UserNotFound:
            raise

    def get_user_notifications(self, user_id):
        """
            Returns a list of all user notifications

            Args:
                user_id(int) - required user id

            Returns:
                list of all user notifications

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            all_notifications = self.load_notifications()
            user_notifications = []
            for notification in all_notifications:
                if notification.user_id == user_id:
                    user_notifications.append(notification)
            return user_notifications
        except UserNotFound:
            raise

    def get_user_messages(self, user_id):
        """
            Returns a list of all user messages

            Args:
                user_id(int) - required user id

            Returns:
                list of all user messages

            Raises:
                UserNotFound exception, if required user is not found

        """
        try:
            user = self.get_user_by_id(user_id)
            all_messages = self.load_messages()
            user_messages = all_messages.get(str(user.id), [])
            return user_messages
        except UserNotFound:
            raise

    def get_user_access_type_to_task(self, user_name, task_id):
        """
            Returns type of user access to task

            Args:
                user_name(str) - user name
                task_id(int) - task id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        user = self.get_user_by_name(user_name)
        relations = self.load_relations()
        for relation in relations:
            if (relation.first_elem_id == user.id and relation.second_elem_id == task_id and
                    relation.related_objects == RelatedObjects.USER_TASK):
                return relation.relation_type

        return None

    def get_user_access_type_to_group(self, user_name, group_id):
        """
            Returns type of user access to group

            Args:
                user_name(str) - user name
                group_id(int) - group id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        user = self.get_user_by_name(user_name)
        relations = self.load_relations()
        for relation in relations:
            if (relation.first_elem_id == user.id and relation.second_elem_id == group_id and
                    relation.related_objects == RelatedObjects.USER_GROUP):
                return relation.relation_type

        return None

    def get_plan_by_id(self, plan_id):
        """
            Returns required plan by its id

            Args:
                plan_id(int) - required plan id

            Returns:
                instance of required plan(Plan object)

            Raises:
                PlanNotFound exception, if required plan is not found

        """
        plans = self.load_plans()
        for plan in plans:
            if plan.id == plan_id:
                return plan
        raise PlanNotFound(plan_id)

    def get_all_plans(self):
        """Returns a list of all plans"""
        return self.load_plans()

    def add_plan(self, plan):
        """
            Add new plan to storage

            Args:
                plan(Plan object) - instance of added plan

        """
        plans = self.load_plans()
        plans.append(plan)
        self.save_plans(plans)

    def delete_plan(self, plan_id):
        """
            Delete plan from storage

            Args:
                plan_id(int) - id of the plan to be deleted

            Raises:
                PlanNotFound exception, if deleted plan is not found

        """
        plans = self.load_plans()
        for plan in plans:
            if plan.id == plan_id:
                plans.remove(plan)
                self.save_plans(plans)
                return
        raise PlanNotFound(plan_id)

    def update_plan(self, plan_id, plan):
        """
            Replaces old instance of plan with a new instance

            Args:
                plan_id(int) - updated plan id
                plan(Plan object) - new instance of updated plan

            Raises:
                PlanNotFound exception, if updatable plan is not found

        """
        plans = self.load_plans()
        for index in range(len(plans)):
            if plans[index].id == plan_id:
                plans[index] = plan
                self.save_plans(plans)
                return
        raise PlanNotFound(plan_id)

    def get_notification_by_id(self, notification_id):
        """
            Returns required notification by its id

            Args:
                notification_id(int) - required notification id

            Returns:
                instance of required notification(Notification object)

            Raises:
                NotificationNotFound exception, if required notification is not found

        """
        notifications = self.load_notifications()
        for notification in notifications:
            if notification.id == notification_id:
                return notification
        raise NotificationNotFound(notification_id)

    def add_notification(self, notification):
        """
            Add new notification to storage

            Args:
                notification(Notification object) - instance of added notification

        """
        notifications = self.load_notifications()
        notifications.append(notification)
        self.save_notifications(notifications)

    def delete_notification(self, notification_id):
        """
            Delete notification from storage

            Args:
                notification_id(int) - id of the notification to be deleted

            Raises:
                NotificationNotFound exception, if deleted notification is not found

        """
        notifications = self.load_notifications()
        for notification in notifications:
            if notification.id == notification_id:
                notifications.remove(notification)
                self.save_notifications(notifications)
                return
        raise NotificationNotFound(notification_id)

    def update_notification(self, notification_id, notification):
        """
            Replaces old instance of notification with a new instance

            Args:
                notification_id(int) - updated notification id
                notification(Notification object) - new instance of updated plan

            Raises:
                NotificationNotFound exception, if updatable notification is not found

        """
        notifications = self.load_notifications()
        for index in range(len(notifications)):
            if notifications[index].id == notification_id:
                notifications[index] = notification
                self.save_notifications(notifications)
                return
        raise NotificationNotFound(notification_id)

    def add_message(self, user_id, message):
        """
            Add new message to storage

            Args:
                user_id(int) - id of user to which message is addressed
                message(str) - message that create notification

        """
        all_messages = self.load_messages()
        user_messages = all_messages.get(str(user_id), [])
        user_messages.append(message)
        all_messages[str(user_id)] = user_messages
        self.save_messages(all_messages)

    def clear_user_messages(self, user_id):
        """
            Delete all user messages

            Args:
                user_id(int) - id of user

        """
        all_messages = self.load_messages()
        del all_messages[str(user_id)]
        self.save_messages(all_messages)

    def add_relation(self, relation):
        """
            Add new relation to storage

            Args:
                relation(Relation object) - instance of added relation

        """
        relations = self.load_relations()
        relations.append(relation)
        self.save_relations(relations)

    def delete_relation(self, first_elem_id, second_elem_id,
                        related_objects):
        """
            Delete relation from storage

            Args:
                first_elem_id - first element id in deleted relation
                second_elem_id - second element id in deleted relation
                related_objects - related objects in deleted relation
                relation_type - relation type in deleted relation

            Raises:
                RelationNotFound exception, if deleted relation is not found

        """
        relations = self.load_relations()
        for relation in relations:
            if (relation.first_elem_id == first_elem_id and relation.second_elem_id == second_elem_id and
                    relation.related_objects == related_objects):
                relations.remove(relation)
                self.save_relations(relations)
                return
        raise RelationNotFound()

    def update_relation(self, relation):
        """
            Replaces old instance of relation with a new instance

            Args:
                relation(Relation object) - new instance of updated relation

            Raises:
                RelationNotFound exception, if updatable relation is not found

        """
        relations = self.load_relations()
        for index in range(len(relations)):
            if relations[index].related_objects != relation.related_objects:
                continue
            if (relations[index].first_elem_id == relation.first_elem_id and
                    relations[index].second_elem_id == relation.second_elem_id):
                relations[index] = relation
                self.save_relations(relations)
                return

        self.add_relation(relation)

    def get_all_relations(self):
        """Returns all relations"""
        return self.load_relations()
