"""
    Interface for working with storage

    Classes:
        Storage - class with storage interface functions

"""
from library.storage.json_storage import JSONStorage


class Storage:
    """
        Contains storage interface functions

    """
    def __init__(self, db_path):
        """
            Args:
                db_path(str) - path to storage directory

        """
        self.storage = JSONStorage(db_path)

    def clear_storage(self):
        """Delete all storage data"""
        self.storage.clear_storage()

    def get_new_task_id(self):
        """Get and return a new id for task"""
        return self.storage.get_new_task_id()

    def get_new_group_id(self):
        """Get and return a new id for group"""
        return self.storage.get_new_group_id()

    def get_new_user_id(self):
        """Get and return a new id for user"""
        return self.storage.get_new_user_id()

    def get_new_plan_id(self):
        """Get and return a new id for plan"""
        return self.storage.get_new_plan_id()

    def get_new_notification_id(self):
        """Get and return a new id for notification"""
        return self.storage.get_new_notification_id()

    def get_task_by_id(self, task_id):
        """
            Returns required task by its id

            Args:
                task_id(int) - id of required task

            Returns:
                instance of required task(Task object)

            Raises:
                TaskNotFound exception, if required task is not found

        """
        return self.storage.get_task_by_id(task_id)

    def add_task(self, task):
        """
            Add new task to storage

            Args:
                task(Task object) - instance of the task to be added

        """
        self.storage.add_task(task)

    def delete_task(self, task_id):
        """
            Delete task from storage

            Args:
                task_id(int) - id of the task to be deleted

            Raises:
                TaskNotFound exception, if deleted task is not found

        """
        self.storage.delete_task(task_id)

    def update_task(self, task_id, task):
        """
            Replaces old instance of task with a new instance

            Args:
                task_id(int) - updated task id
                task(Task object) - new instance of updated task

            Raises:
                TaskNotFound exception, if updatable task is not found

        """
        self.storage.update_task(task_id, task)

    def get_task_users(self, task_id):
        """
            Returns a dict of all users who have access to task and their access types to task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all users who have access to task and their access types to task

            Raises:
                TaskNotFound exception, if required task is not found

        """
        return self.storage.get_task_users(task_id)

    def get_all_tasks(self):
        """Returns a list of all tasks"""
        return self.storage.get_all_tasks()

    def get_task_groups(self, task_id):
        """
            Returns a list of all groups to which task is included

            Args:
                task_id(int) - required task id

            Returns:
                 list of all groups to which task is included

            Raises:
                TaskNotFound exception, if required task is not found

        """
        return self.storage.get_task_groups(task_id)

    def get_task_subtasks(self, task_id):
        """
            Returns a dict of all subtasks and type of their relation with task

            Args:
                task_id(int) - required task id

            Returns:
                 dict of all subtasks and type of their relation with task

            Raises:
                TaskNotFound exception, if required task is not found

        """
        return self.storage.get_task_subtasks(task_id)

    def get_group_by_id(self, group_id):
        """
            Returns required group by its id

            Args:
                group_id(int) - required group id

            Returns:
                instance of required group(Group object)

            Raises:
                GroupNotFound exception, if required group is not found

        """
        return self.storage.get_group_by_id(group_id)

    def add_group(self, group):
        """
            Add new group to storage

            Args:
                group(Group object) - instance of added group

        """
        self.storage.add_group(group)

    def delete_group(self, group_id):
        """
            Delete group from storage

            Args:
                group_id(int) - id of the group to be deleted

            Raises:
                GroupNotFound exception, if deleted group is not found

        """
        self.storage.delete_group(group_id)

    def update_group(self, group_id, group):
        """
            Replaces old instance of group with a new instance

            Args:
                group_id(int) - updated group id
                group(Group object) - new instance of updated group

            Raises:
                GroupNotFound exception, if updatable group is not found

        """
        self.storage.update_group(group_id, group)

    def get_group_users(self, group_id):
        """
            Returns a dict of all users who have access to group and
            their access types to group

            Args:
                group_id(int) - required group id

            Returns:
                dict of all users who have access to group and their access types to group

            Raises:
                GroupNotFound exception, if required group is not found

        """
        return self.storage.get_group_users(group_id)

    def get_group_tasks(self, group_id):
        """
            Returns a list of all group tasks

            Args:
                group_id(int) - required group id

            Returns:
                list of all group tasks

            Raises:
                GroupNotFound exception, if required group is not found

        """
        return self.storage.get_group_tasks(group_id)

    def check_user_existence(self, user_name):
        """
            Checks if there is already a user with that name

            Args:
                user_name(str) - name of required user

            Returns:
                 True, if a user with this name is found.
                 else False

        """
        return self.storage.check_user_existence(user_name)

    def get_user_by_id(self, user_id):
        """
            Returns required user by its id

            Args:
                user_id(int) - id of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_by_id(user_id)

    def get_user_by_name(self, user_name):
        """
            Returns required user by its name

            Args:
                user_name(str) - name of required user

            Returns:
                instance of required user(User object)

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_by_name(user_name)

    def get_user_id_by_user_name(self, user_name):
        """
            Returns user id by its name

            Args:
                user_name(str) - name of required user

            Returns:
                id of required user(int)

        """
        return self.storage.get_user_id_by_user_name(user_name)

    def add_user(self, user):
        """
            Add new user to storage

            Args:
                user(User object) - instance of added user

        """
        self.storage.add_user(user)

    def update_user(self, user_id, user):
        """
            Replaces old instance of user with a new instance

            Args:
                user_id(int) - updated user id
                user(User object) - new instance of updated user

            Raises:
                UserNotFound exception, if updatable user is not found

        """
        self.storage.update_user(user_id, user)

    def get_user_tasks(self, user_id):
        """
            Returns a dict of all tasks to which the user has access and
            type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all tasks to which the user has access and
                type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_tasks(user_id)

    def get_user_groups(self, user_id):
        """
            Returns a dict of all groups to which the user has access and
            type of user access to them

            Args:
                user_id(int) - required user id

            Returns:
                dict of all groups to which the user has access and
                type of user access to them

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_groups(user_id)

    def get_user_plans(self, user_id):
        """
            Returns a list of all user plans

            Args:
                user_id(int) - required user id

            Returns:
                list of all user plans

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_plans(user_id)

    def get_user_notifications(self, user_id):
        """
            Returns a list of all user notifications

            Args:
                user_id(int) - required user id

            Returns:
                list of all user notifications

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_notifications(user_id)

    def get_user_messages(self, user_id):
        """
            Returns a list of all user messages

            Args:
                user_id(int) - required user id

            Returns:
                list of all user messages

            Raises:
                UserNotFound exception, if required user is not found

        """
        return self.storage.get_user_messages(user_id)

    def get_user_access_type_to_task(self, user_name, task_id):
        """
            Returns type of user access to task

            Args:
                user_name(str) - user name
                task_id(int) - task id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        return self.storage.get_user_access_type_to_task(user_name, task_id)

    def get_user_access_type_to_group(self, user_name, group_id):
        """
            Returns type of user access to group

            Args:
                user_name(str) - user name
                group_id(int) - group id, the type of access to which should be know

            Returns:
                Value of enumerator UserType or None, if user does not have access

        """
        return self.storage.get_user_access_type_to_group(user_name, group_id)

    def get_plan_by_id(self, plan_id):
        """
            Returns required plan by its id

            Args:
                plan_id(int) - required plan id

            Returns:
                instance of required plan(Plan object)

            Raises:
                PlanNotFound exception, if required plan is not found

        """
        return self.storage.get_plan_by_id(plan_id)

    def get_all_plans(self):
        """Returns a list of all plans"""
        return self.storage.get_all_plans()

    def add_plan(self, plan):
        """
            Add new plan to storage

            Args:
                plan(Plan object) - instance of added plan

        """
        self.storage.add_plan(plan)

    def delete_plan(self, plan_id):
        """
            Delete plan from storage

            Args:
                plan_id(int) - id of the plan to be deleted

            Raises:
                PlanNotFound exception, if deleted plan is not found

        """
        self.storage.delete_plan(plan_id)

    def update_plan(self, plan_id, plan):
        """
            Replaces old instance of plan with a new instance

            Args:
                plan_id(int) - updated plan id
                plan(Plan object) - new instance of updated plan

            Raises:
                PlanNotFound exception, if updatable plan is not found

        """
        self.storage.update_plan(plan_id, plan)

    def get_notification_by_id(self, notification_id):
        """
            Returns required notification by its id

            Args:
                notification_id(int) - required notification id

            Returns:
                instance of required notification(Notification object)

            Raises:
                NotificationNotFound exception, if required notification is not found

        """
        return self.storage.get_notification_by_id(notification_id)

    def add_notification(self, notification):
        """
            Add new notification to storage

            Args:
                notification(Notification object) - instance of added notification

        """
        return self.storage.add_notification(notification)

    def delete_notification(self, notification_id):
        """
            Delete notification from storage

            Args:
                notification_id(int) - id of the notification to be deleted

            Raises:
                NotificationNotFound exception, if deleted notification is not found

        """
        return self.storage.delete_notification(notification_id)

    def update_notification(self, notification_id, notification):
        """
            Replaces old instance of notification with a new instance

            Args:
                notification_id(int) - updated notification id
                notification(Notification object) - new instance of updated plan

            Raises:
                NotificationNotFound exception, if updatable notification is not found

        """
        return self.storage.update_notification(notification_id, notification)

    def add_message(self, user_id, message):
        """
            Add new message to storage

            Args:
                user_id(int) - id of user to which message is addressed
                message(str) - message that create notification

        """
        return self.storage.add_message(user_id, message)

    def clear_user_messages(self, user_id):
        """
            Delete all user messages

            Args:
                user_id(int) - id of user

        """
        return self.storage.clear_user_messages(user_id)

    def add_relation(self, relation):
        """
            Add new relation to storage

            Args:
                relation(Relation object) - instance of added relation

        """
        self.storage.add_relation(relation)

    def delete_relation(self, first_elem_id, second_elem_id,
                        related_objects):
        """
            Delete relation from storage

            Args:
                first_elem_id - first element id in deleted relation
                second_elem_id - second element id in deleted relation
                related_objects - related objects in deleted relation
                relation_type - relation type in deleted relation

            Raises:
                RelationNotFound exception, if deleted relation is not found

        """
        self.storage.delete_relation(first_elem_id,
                                     second_elem_id,
                                     related_objects)

    def update_relation(self, relation):
        """
            Replaces old instance of relation with a new instance

            Args:
                relation(Relation object) - new instance of updated relation

            Raises:
                RelationNotFound exception, if updatable relation is not found

        """
        self.storage.update_relation(relation)

    def get_all_relations(self):
        """Returns all relations"""
        return self.storage.get_all_relations()
