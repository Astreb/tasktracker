"""
    Provides class that describes a group of tasks

    Classes:
        Group - describing a group of tasks

"""

from library.models.user import UserType


class Group:
    """
        Group of tasks description

        Attributes:
            id - group id in system
            name - group name in system(may not be unique)
            description - group description

    """
    def __init__(self, group_id, group_name, group_description=None):
        """
        Args:
            group_id(int) - group id in system
            group_name(str) - group name(is not empty string)
            group_description(str) - group description(default None)

        """
        self.id = group_id
        self.name = group_name
        self.description = group_description
