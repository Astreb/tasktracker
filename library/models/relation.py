from enum import Enum


class Relation:
    """
        Attributes:
            *first_elem_id(int) - id of user or group
            *second_elem_id(int) - id of group, task, subtask, plan or notification
            *relation_type(Enum or None) - relation type between user and group,
            user and task or task and subtask
    """
    def __init__(self, first_elem_id, second_elem_id,
                 related_objects, relation_type=None):
        """
            Args:
                *first_elem_id(int) - id of user or group
                *second_elem_id(int) - id of group, task, subtask, plan or notification
                *related_objects(Enum) - types of objects that are related
                *relation_type(Enum) - relation type between user and group,
                user and task or task and subtask(default None)

        """
        self.first_elem_id = first_elem_id
        self.second_elem_id = second_elem_id
        self.related_objects = related_objects
        self.relation_type = relation_type


class RelatedObjects(Enum):
    USER_GROUP = 0
    USER_TASK = 1
    GROUP_TASK = 2
    TASK_SUBTASK = 3
