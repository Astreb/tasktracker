"""
    Provides class for notification

    Classes:
        Notification - describe notification object

"""


class Notification:
    """
        Describe notification object

        Attributes:
            id - notification id in system
            user_id - id of notification user author
            creation_time - time at which to send a notification message
            message - text of notification message

    """
    def __init__(self, user_id, notification_id, notification_creation_time, notification_message):
        """
            Args:
                id(int) - notification id in system
                user_id(int) - id of notification user author
                notification_creation_time(datetime) - time at which to send a notification message
                notification_message(str) - text of notification message
        """
        self.id = notification_id
        self.user_id = user_id
        self.creation_time = notification_creation_time
        self.message = notification_message
