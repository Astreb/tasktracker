import unittest

from library.tests.group_tests import GroupTests
from library.tests.notification_tests import NotificationTests
from library.tests.plan_tests import PlanTests
from library.tests.task_tests import TaskTests
from library.tests.user_tests import UserTests


def get_tests():
    suite = unittest.TestSuite()

    suite.addTests(unittest.makeSuite(GroupTests))
    suite.addTests(unittest.makeSuite(UserTests))
    suite.addTests(unittest.makeSuite(PlanTests))
    suite.addTests(unittest.makeSuite(TaskTests))
    suite.addTests(unittest.makeSuite(NotificationTests))

    return suite
