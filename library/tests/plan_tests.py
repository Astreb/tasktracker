import datetime
import unittest

import os

from library.models.task import TaskPriority

from library.service.exceptions import (
    AccessDenied,
    IncorrectDateTime
)

from library.service.lib_logger import (
    get_lib_logger,
    disable_lib_logger,
    enable_lib_logger
)

from library.tasktracker import TaskTracker


class PlanTests(unittest.TestCase):
    def setUp(self):
        self.is_logger_disabled = get_lib_logger().disabled
        disable_lib_logger()

        storage_path = os.path.expanduser('~/TaskTracker/tests')
        self.tasktracker = TaskTracker(storage_path)

        self.tasktracker.create_user(user_name='test_user')
        self.plan = self.tasktracker.create_plan(
            user_name='test_user',
            task_name='test plan',
            task_creation_time=datetime.datetime(year=2018, month=9, day=3, hour=11),
            start_date=datetime.datetime(year=2018, month=9, day=3, hour=12),
            end_date=datetime.datetime(year=2018, month=9, day=3, hour=18),
            repeat_time_delta=datetime.timedelta(weeks=1, days=5, hours=4, minutes=10),
            repeat_time_delta_months=1,
            description='test desc',
            priority=2
        )

    def test_create_plan(self):
        plan = self.tasktracker.create_plan(
            user_name='test_user',
            task_name='test plan',
            task_creation_time=datetime.datetime(year=2018, month=9, day=3, hour=11),
            start_date=datetime.datetime(year=2018, month=9, day=3, hour=12),
            end_date=datetime.datetime(year=2018, month=9, day=3, hour=18),
            repeat_time_delta=datetime.timedelta(weeks=1, days=5, hours=4, minutes=10),
            repeat_time_delta_months=1,
            description='test desc',
            priority=2
        )

        self.assertEqual(plan.id, 2)
        self.assertEqual(plan.user_id, 1)
        self.assertEqual(plan.task_name, 'test plan')
        self.assertEqual(plan.task_creation_time, datetime.datetime(year=2018, month=9, day=3, hour=11))
        self.assertEqual(plan.start_date, datetime.datetime(year=2018, month=9, day=3, hour=12))
        self.assertEqual(plan.end_date, datetime.datetime(year=2018, month=9, day=3, hour=18))
        self.assertEqual(plan.repeat_time_delta, datetime.timedelta(weeks=1, days=5, hours=4, minutes=10))
        self.assertEqual(plan.repeat_time_delta_months, 1)
        self.assertEqual(plan.description, 'test desc')
        self.assertEqual(plan.priority, TaskPriority(2))

    def test_delete_plan(self):
        self.tasktracker.delete_plan(user_name='test_user', plan_id=1)

        plans = self.tasktracker.get_user_plans(user_name='test_user')
        self.assertEqual(len(plans), 0)

    def test_delete_another_plan(self):
        self.tasktracker.create_user(user_name='test_user2')
        with self.assertRaises(AccessDenied):
            self.tasktracker.delete_plan(user_name='test_user2', plan_id=1)

    def test_edit_plan(self):
        plan = self.tasktracker.edit_plan(
            user_name='test_user',
            plan_id=1,
            new_task_name='edit test plan',
            new_task_creation_time=datetime.datetime(year=2019, month=10, day=4, hour=12),
            new_start_date=datetime.datetime(year=2019, month=10, day=4, hour=13),
            new_end_date=datetime.datetime(year=2019, month=10, day=4, hour=19),
            new_repeat_time_delta=datetime.timedelta(weeks=2, days=6, hours=4, minutes=20),
            new_repeat_time_delta_months=2,
            new_description='edit test desc',
            new_priority=3
        )

        self.assertEqual(plan.id, 1)
        self.assertEqual(plan.user_id, 1)
        self.assertEqual(plan.task_name, 'edit test plan')
        self.assertEqual(plan.task_creation_time, datetime.datetime(year=2019, month=10, day=4, hour=12))
        self.assertEqual(plan.start_date, datetime.datetime(year=2019, month=10, day=4, hour=13))
        self.assertEqual(plan.end_date, datetime.datetime(year=2019, month=10, day=4, hour=19))
        self.assertEqual(plan.repeat_time_delta, datetime.timedelta(weeks=2, days=6, hours=4, minutes=20))
        self.assertEqual(plan.repeat_time_delta_months, 2)
        self.assertEqual(plan.description, 'edit test desc')
        self.assertEqual(plan.priority, TaskPriority(3))

    def test_edit_plan_another_user(self):
        self.tasktracker.create_user(user_name='test_user2')
        with self.assertRaises(AccessDenied):
            self.tasktracker.edit_plan(user_name='test_user2', plan_id=1, new_task_name='')

    def test_edit_plan_empty_task_name(self):
        with self.assertRaises(ValueError):
            self.tasktracker.edit_plan(user_name='test_user', plan_id=1, new_task_name='')

    def test_edit_plan_incorrect_priority(self):
        with self.assertRaises(ValueError):
            self.tasktracker.edit_plan(user_name='test_user', plan_id=1, new_priority=5)

    def test_edit_plan_incorrect_repeat_months_count(self):
        with self.assertRaises(IncorrectDateTime):
            self.tasktracker.edit_plan(user_name='test_user', plan_id=1, new_repeat_time_delta_months=-1)

    def test_edit_plan_incorrect_repeat_delta_time(self):
        with self.assertRaises(IncorrectDateTime):
            self.tasktracker.edit_plan(
                user_name='test_user',
                plan_id=1,
                new_repeat_time_delta_months=0,
                new_repeat_time_delta=datetime.timedelta()
            )

    def test_edit_plan_creation_time_more_start_date(self):
        with self.assertRaises(IncorrectDateTime):
            self.tasktracker.edit_plan(
                user_name='test_user',
                plan_id=1,
                new_task_creation_time=datetime.datetime(year=2020, month=11, day=5, hour=14),
                new_start_date=datetime.datetime(year=2019, month=10, day=4, hour=13)
            )

    def test_edit_plan_start_date_more_end_date(self):
        with self.assertRaises(IncorrectDateTime):
            self.tasktracker.edit_plan(
                user_name='test_user',
                plan_id=1,
                new_start_date=datetime.datetime(year=2020, month=11, day=5, hour=14),
                new_end_date=datetime.datetime(year=2019, month=10, day=4, hour=13)
            )

    def tearDown(self):
        if not self.is_logger_disabled:
            enable_lib_logger()
        self.tasktracker.clear_storage()


if __name__ == '__main__':
    unittest.main()
