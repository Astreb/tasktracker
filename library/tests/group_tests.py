import unittest

import os

from library.service.exceptions import (
    AccessDenied,
    GroupNotFound
)

from library.service.lib_logger import (
    get_lib_logger,
    disable_lib_logger,
    enable_lib_logger
)

from library.tasktracker import TaskTracker


class GroupTests(unittest.TestCase):
    def setUp(self):
        self.is_logger_disabled = get_lib_logger().disabled
        disable_lib_logger()

        storage_path = os.path.expanduser('~/TaskTracker/tests')
        self.tasktracker = TaskTracker(storage_path)

        self.tasktracker.create_user(user_name='test_user')
        self.tasktracker.create_user(user_name='test_user2')

    def test_create_group(self):
        group = self.tasktracker.create_group(
            user_name='test_user',
            group_name='test_group',
            group_description='test_description'
        )

        self.assertEqual(group.id, 1)
        self.assertEqual(group.name, 'test_group')
        self.assertEqual(group.description, 'test_description')

    def test_delete_group_user_author(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')
        group_id = test_group.id

        self.tasktracker.delete_group(user_name='test_user', group_id=group_id)
        with self.assertRaises(GroupNotFound):
            self.tasktracker.storage.get_group_by_id(group_id)

    def test_delete_group_user_not_author(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')
        group_id = test_group.id

        for i in range(1, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_group(
                        user_name='test_user',
                        other_user_id=2,
                        group_id=group_id,
                        access_type=i
                    )

                    self.tasktracker.delete_group(user_name='test_user2', group_id=group_id)

    def test_edit_group_user_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        edited_test_group = self.tasktracker.edit_group(
            user_name='test_user',
            group_id=test_group.id,
            new_group_name='edited_test_group',
            new_group_description='test group desc'
        )

        self.assertEqual(edited_test_group.id, 1)
        self.assertEqual(edited_test_group.name, 'edited_test_group')
        self.assertEqual(edited_test_group.description, 'test group desc')

    def test_edit_group_user_not_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')
        group_id = test_group.id

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_group(
                        user_name='test_user',
                        other_user_id=2,
                        group_id=group_id,
                        access_type=i
                    )

                    self.tasktracker.edit_group(user_name='test_user2', group_id=test_group.id)

    def test_add_task_user_not_group_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_group(
                        user_name='test_user',
                        other_user_id=2,
                        group_id=test_group.id,
                        access_type=i)
                    self.tasktracker.add_task_to_group('test_user2', test_group.id, test_task.id)

    def test_add_task_user_not_task_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        test_task = self.tasktracker.create_task(
            user_name='test_user2',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user2',
                        other_user_id=1,
                        task_id=test_task.id,
                        access_type=i
                    )

                    self.tasktracker.add_task_to_group('test_user', test_group.id, test_task.id)

    def test_add_task_to_group(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')
        group_id = test_group.id

        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        self.tasktracker.add_task_to_group('test_user', group_id, test_task.id)

        tasks = self.tasktracker.get_group_tasks('test_user', group_id)
        self.assertEqual(len(tasks), 1)

    def test_delete_task_user_not_group_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        self.tasktracker.add_task_to_group('test_user', test_group.id, test_task.id)

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_group(
                        user_name='test_user',
                        other_user_id=2,
                        group_id=test_group.id,
                        access_type=i
                    )

                    self.tasktracker.delete_task_from_group('test_user2', test_group.id, test_task.id)

    def test_delete_task_user_not_task_author_or_moderator(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')

        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        self.tasktracker.add_task_to_group('test_user', test_group.id, test_task.id)

        self.tasktracker.change_other_user_access_type_to_group(
            user_name='test_user',
            other_user_id=2,
            group_id=test_group.id,
            access_type=1
        )

        for i in range(2, 4):
            with self.subTest(i=i):
                with self.assertRaises(AccessDenied):
                    self.tasktracker.change_other_user_access_type_to_task(
                        user_name='test_user',
                        other_user_id=1,
                        task_id=test_task.id,
                        access_type=i
                    )

                    self.tasktracker.delete_task_from_group('test_user2', test_group.id, test_task.id)

    def test_delete_task_from_group(self):
        test_group = self.tasktracker.create_group(user_name='test_user', group_name='test_group')
        group_id = test_group.id

        test_task = self.tasktracker.create_task(
            user_name='test_user',
            task_name='test_task',
            start_date='12:12 12.12.2012'
        )

        self.tasktracker.add_task_to_group('test_user', group_id, test_task.id)

        self.tasktracker.delete_task_from_group('test_user', group_id, test_task.id)

        tasks = self.tasktracker.get_group_tasks('test_user', group_id)
        self.assertEqual(len(tasks), 0)

    def tearDown(self):
        if not self.is_logger_disabled:
            enable_lib_logger()
        self.tasktracker.clear_storage()


if __name__ == '__main__':
    unittest.main()
