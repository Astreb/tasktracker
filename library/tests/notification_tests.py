import datetime
import os
import unittest

from library.service.exceptions import (
    AccessDenied,
    NotificationNotFound
)
from library.service.lib_logger import (
    get_lib_logger,
    disable_lib_logger,
    enable_lib_logger
)
from library.tasktracker import TaskTracker


class NotificationTests(unittest.TestCase):
    def setUp(self):
        self.is_logger_disabled = get_lib_logger().disabled
        disable_lib_logger()

        storage_path = os.path.expanduser('~/TaskTracker/tests')
        self.tasktracker = TaskTracker(storage_path)

        self.test_user = self.tasktracker.create_user(user_name='test_user')
        self.test_user2 = self.tasktracker.create_user(user_name='test_user2')
        self.notification = self.tasktracker.create_notification(
            user_name='test_user',
            notification_creation_time=datetime.datetime.now() + datetime.timedelta(weeks=1),
            notification_message='test notification message'
        )

    def test_create_notification(self):
        notification = self.tasktracker.create_notification(
            user_name='test_user',
            notification_creation_time=datetime.datetime(
                year=2018,
                month=10,
                day=11,
                hour=14
            ),
            notification_message='test notification message'
        )

        self.assertEqual(notification.user_id, self.test_user.id)
        self.assertEqual(notification.id, 2)
        self.assertEqual(notification.message, 'test notification message')
        self.assertEqual(
            notification.creation_time,
            datetime.datetime(
                year=2018,
                month=10,
                day=11,
                hour=14
            )
        )

    def test_create_notification_with_empty_message(self):
        with self.assertRaises(ValueError):
            self.tasktracker.create_notification(
                user_name='test_user',
                notification_creation_time=datetime.datetime(
                    year=2018,
                    month=10,
                    day=11,
                    hour=14
                ),
                notification_message=''
            )

    def test_delete_notification(self):
        self.tasktracker.delete_notification(user_name='test_user', notification_id=1)

        notifications = self.tasktracker.get_user_notifications(user_name='test_user')
        self.assertEqual(len(notifications), 0)

    def test_delete_another_notification(self):
        with self.assertRaises(AccessDenied):
            self.tasktracker.delete_notification(user_name='test_user2', notification_id=1)

    def test_edit_notification(self):
        notification = self.tasktracker.edit_notification(
            user_name='test_user',
            notification_id=1,
            new_notification_creation_time=datetime.datetime(
                year=2019,
                month=11,
                day=12,
                hour=15
            ),
            new_notification_message='edited test notification message'
        )

        self.assertEqual(notification.user_id, self.test_user.id)
        self.assertEqual(notification.id, 1)
        self.assertEqual(notification.message, 'edited test notification message')
        self.assertEqual(
            notification.creation_time,
            datetime.datetime(
                year=2019,
                month=11,
                day=12,
                hour=15
            )
        )

    def test_edit_notification_another_user(self):
        with self.assertRaises(AccessDenied):
            self.tasktracker.edit_notification(user_name='test_user2', notification_id=1,
                                               new_notification_message='message')

    def test_edit_notification_empty_notification_message(self):
        with self.assertRaises(ValueError):
            self.tasktracker.edit_notification(user_name='test_user', notification_id=1,
                                               new_notification_message='')

    def test_update_notification(self):
        creation_time = datetime.datetime.now() - datetime.timedelta(days=1)
        notification = self.tasktracker.create_notification(
            user_name='test_user',
            notification_creation_time=creation_time,
            notification_message='test notification message'
        )

        message = '[{}] {}'.format(creation_time, notification.message)

        self.tasktracker.update_notifications(user_name='test_user')

        with self.assertRaises(NotificationNotFound):
            self.tasktracker.get_notification_by_id(2)

        messages = self.tasktracker.get_user_messages(user_name='test_user')

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0], message)

    def test_clear_user_messages(self):
        creation_time = datetime.datetime.now() - datetime.timedelta(days=1)
        notification = self.tasktracker.create_notification(
            user_name='test_user',
            notification_creation_time=creation_time,
            notification_message='test notification message'
        )

        message = '[{}] {}'.format(creation_time, notification.message)

        self.tasktracker.update_notifications(user_name='test_user')

        messages = self.tasktracker.get_user_messages(user_name='test_user')

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0], message)

        self.tasktracker.clear_user_messages(user_name='test_user')

        messages = self.tasktracker.get_user_messages(user_name='test_user')

        self.assertEqual(len(messages), 0)

    def tearDown(self):
        if not self.is_logger_disabled:
            enable_lib_logger()
        self.tasktracker.clear_storage()


if __name__ == '__main__':
    unittest.main()
