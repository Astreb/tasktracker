"""
    Provides all TaskTracker exceptions

    Classes:
        TaskNotFound(Exception) - exception for the case, if not found the required task
        GroupNotFound(Exception) - exception for the case, if not found the required group
        UserNotFound(Exception) - exception for the case, if not found the required user
        PlanNotFound(Exception) - exception for the case, if not found the required plan
        NotificationNotFound(Exception) - exception for the case, if not found the required notification
        ConfigNotFound(Exception) - exception for the case, if not found config file
        AccessDenied(Exception) - exception for the case when the user has insufficient rights to perform the action
        IncorrectDateTime(Exception) - exception for the case when an incorrect time is specified
        IncorrectAction(Exception) - exception for the case when an incorrect action is committed
        IncorrectConfig(Exception) - exception for the case when incorrect configuration settings are specified

"""


class TaskNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Task {} not found.'.format(self.value)


class GroupNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Group {} not found.'.format(self.value)


class UserNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'User {} not found.'.format(self.value)


class PlanNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Plan {} not found.'.format(self.value)


class NotificationNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Notification {} not found.'.format(self.value)


class RelationNotFound(Exception):
    def __str__(self):
        return 'Relation with given parameters not found.'


class AccessDenied(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Access denied : {}.'.format(self.value)


class IncorrectDateTime(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Incorrect datetime : {}.'.format(self.value)


class IncorrectAction(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Incorrect action : {}.'.format(self.value)


class ConfigNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Config file {} not found.'.format(self.value)


class IncorrectConfig(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Incorrect config: {}'.format(self.value)
