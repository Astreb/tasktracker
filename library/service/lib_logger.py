"""
    Provides functions for working with the library's logger

    Functions:
        get_lib_logger() - returns the library's logger
        enable_lib_logger() - enable the library's logger
        disable_lib_logger() - desable the library's logger
        write_exceptions_log(func) - decorator, which allows to display all error messages in the log file
"""

import logging
from functools import wraps


def get_lib_logger():
    """Returns the library's logger"""
    return logging.getLogger('TaskTrackerLibrary')


def enable_lib_logger():
    """Enable the library's logger"""
    get_lib_logger().disabled = False


def disable_lib_logger():
    """Desable the library's logger"""
    get_lib_logger().disabled = True


def write_exceptions_log(func):
    """Decorator, which allows to display all error messages in the log file"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = get_lib_logger()

        try:
            return func(*args, **kwargs)
        except Exception as exp:
            logger.error(exp)
            raise

    return wrapper
