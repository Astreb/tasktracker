from setuptools import setup, find_packages

setup(
    name="TaskTrackerLibrary",
    version='1.1',
    description='TaskTracker Library',
    author='Nikita Kulichok',
    author_email='nikita.kulicok@gmail.com',
    url='https://bitbucket.org/Astreb/tasktracker',
    packages=find_packages(),
    test_suite='tests.init_tests.get_tests',
    install_requires=['json_tricks']
)
